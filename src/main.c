/*
 * main.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <config.h>

#include <gtk/gtk.h>

#include <clutter/clutter.h>
#include <clutter-gtk/clutter-gtk.h>

#include <glib/gi18n.h>

#include "../libglide/glide-window.h"
#include "../libglide/glide.h"

int
main (int argc, char *argv[])
{
  GlideWindow *window;

  gtk_set_locale ();
  gtk_init (&argc, &argv);
  gtk_clutter_init (&argc, &argv);
  
  glide_init (&argc, &argv);
  
  g_set_application_name ("Glide");
  
  window = glide_window_new ();
  if (argc >= 2)
    glide_window_open_document (GLIDE_WINDOW (window), argv[1]);

  gtk_main ();
  return 0;
}
