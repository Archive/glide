/*
 * glide-theme-chooser.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-theme-chooser
 * @short_description: A toplevel window for selecting
 * themes.
 *
 */

#include "glide-theme-chooser.h"
#include "glide-theme-chooser-priv.h"

#include "glide-theme-manager.h"
#include "glide-theme-preview-actor.h"

#include "glide-debug.h"

#include <girepository.h>

#include <clutter-gtk/clutter-gtk.h>

#define GLIDE_THEME_CHOOSER_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_THEME_CHOOSER, GlideThemeChooserPrivate))

G_DEFINE_TYPE(GlideThemeChooser, glide_theme_chooser, GTK_TYPE_WINDOW);

enum {
  PROP_0,
  PROP_THEME
};

enum {
  RESPONSE,
  LAST_SIGNAL
};

static guint theme_chooser_signals[LAST_SIGNAL] = { 0, };

static GlideTheme *
glide_theme_chooser_get_selection (GlideThemeChooser *chooser)
{
  GlideThemePreviewActor *prev;

  if (!chooser->priv->selection)
    return NULL;
  
  prev = GLIDE_THEME_PREVIEW_ACTOR (chooser->priv->selection);
  return glide_theme_preview_actor_get_theme (prev);
}

static void
glide_theme_chooser_finalize (GObject *object)
{
  GlideThemeChooser *chooser = (GlideThemeChooser *)object;
  GLIDE_NOTE(THEME_CHOOSER, "Finalizing theme chooser: %p", object);
  
  g_list_free (chooser->priv->themes);
}

static void
glide_theme_chooser_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideThemeChooser *chooser = (GlideThemeChooser *)object;
  switch (prop_id)
    {
    case PROP_THEME:
      g_value_set_object (value, chooser->priv->theme);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_theme_chooser_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static gboolean
glide_theme_chooser_stage_button_press (ClutterActor *actor,
					ClutterButtonEvent *bev,
					gpointer user_data)
{
  GlideThemeChooser *chooser = (GlideThemeChooser *)user_data;
  ClutterActor *clicked;

  if (chooser->priv->selection)
    {
      glide_theme_preview_actor_set_selected (GLIDE_THEME_PREVIEW_ACTOR (chooser->priv->selection),
					      FALSE);
      chooser->priv->selection = NULL;
    }
  
  clicked = clutter_stage_get_actor_at_pos (CLUTTER_STAGE (actor), CLUTTER_PICK_REACTIVE,
				  bev->x, bev->y);
  if (!clicked || !GLIDE_IS_THEME_PREVIEW_ACTOR (clicked))
    {
      gtk_widget_set_sensitive (chooser->priv->new_button, FALSE);
      return;
    }

  gtk_widget_set_sensitive (chooser->priv->new_button, TRUE);
  chooser->priv->selection = clicked;
  
  glide_theme_preview_actor_set_selected (GLIDE_THEME_PREVIEW_ACTOR (chooser->priv->selection),
					  TRUE);
}

static ClutterLayoutManager *
glide_theme_chooser_make_layout (GlideThemeChooser *chooser)
{
  // TODO: What is the deal with references on these?
  ClutterLayoutManager *layout = clutter_flow_layout_new (CLUTTER_FLOW_HORIZONTAL);
  
  clutter_flow_layout_set_homogeneous (CLUTTER_FLOW_LAYOUT (layout),
				      FALSE);
  clutter_flow_layout_set_column_spacing (CLUTTER_FLOW_LAYOUT (layout), 10);
  clutter_flow_layout_set_row_spacing (CLUTTER_FLOW_LAYOUT (layout),
				       10);
  
  chooser->priv->layout = layout;
  
  return layout;
}

static void
glide_theme_chooser_populate_theme_box (GlideThemeChooser *chooser)
{
  GList *t;
  
  for (t = chooser->priv->themes; t; t = t->next)
    {
      GlideTheme *theme = (GlideTheme *)t->data;
      ClutterActor *preview = glide_theme_preview_actor_new ();
      
      glide_theme_preview_actor_set_theme (GLIDE_THEME_PREVIEW_ACTOR (preview),
					   theme);
      
      if (!chooser->priv->selection)
	{
	  chooser->priv->selection = preview;
	  glide_theme_preview_actor_set_selected (GLIDE_THEME_PREVIEW_ACTOR (preview),
						  TRUE);
	}
      
      clutter_box_pack (CLUTTER_BOX (chooser->priv->theme_box), preview, NULL);
    }
}

static void
glide_theme_chooser_constrain_box (GlideThemeChooser *chooser)
{
  ClutterConstraint *x = clutter_align_constraint_new (chooser->priv->stage, CLUTTER_ALIGN_X_AXIS,
						       .5);
  ClutterConstraint *y = clutter_align_constraint_new (chooser->priv->stage, CLUTTER_ALIGN_Y_AXIS,
						       .5);
  
  clutter_actor_add_constraint (chooser->priv->theme_box, x);
  clutter_actor_add_constraint (chooser->priv->theme_box, y);
}				   

static ClutterActor *
glide_theme_chooser_make_theme_box (GlideThemeChooser *chooser)
{
  ClutterActor *box;
  ClutterLayoutManager *layout = glide_theme_chooser_make_layout (chooser);
  
  box = clutter_box_new (layout);
  clutter_actor_set_size (CLUTTER_ACTOR (box), 580, 380);
  
  chooser->priv->theme_box = box;
  
  glide_theme_chooser_constrain_box (chooser);
  
  glide_theme_chooser_populate_theme_box (chooser);
  
  return box;  
}

static void
glide_theme_chooser_stage_allocation_changed (ClutterActor *stage,
					      const ClutterActorBox *allocation,
					      ClutterAllocationFlags flags,
					      ClutterActor *box)
{
  gfloat width, height;
  
  clutter_actor_box_get_size (allocation, &width, &height);
  clutter_actor_set_size (box, width-20, height-20);
}

static void
glide_theme_chooser_populate_stage (GlideThemeChooser *chooser)
{
  ClutterActor *box = glide_theme_chooser_make_theme_box (chooser);
  
  clutter_container_add_actor (CLUTTER_CONTAINER (chooser->priv->stage), box);
  
  g_signal_connect (chooser->priv->stage, "allocation-changed",
		    G_CALLBACK (glide_theme_chooser_stage_allocation_changed),
		    box);
}

static void
glide_theme_chooser_setup_stage (GlideThemeChooser *chooser)
{
  ClutterActor *stage = gtk_clutter_embed_get_stage (GTK_CLUTTER_EMBED (chooser->priv->embed));
  ClutterColor black = {0x00, 0x00, 0x00, 0xff};

  clutter_actor_set_size (stage, 600, 400);
  clutter_stage_set_color (CLUTTER_STAGE (stage), &black);
  
  clutter_actor_show_all (stage);
  
  g_signal_connect (stage, "button-press-event",
		    G_CALLBACK (glide_theme_chooser_stage_button_press),
		    chooser);
  
  chooser->priv->stage = stage;
  
  glide_theme_chooser_populate_stage (chooser);
}

static GtkWidget *
glide_theme_chooser_make_clutter_embed (GlideThemeChooser *chooser)
{
  GtkWidget *embed = gtk_clutter_embed_new ();
  
  chooser->priv->embed = embed;
  glide_theme_chooser_setup_stage (chooser);
  
  gtk_widget_set_size_request (embed, 100, 400);
  
  return embed;
}

static GtkWidget *
glide_theme_chooser_make_top_hbox (GlideThemeChooser *chooser)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *embed = glide_theme_chooser_make_clutter_embed (chooser);
  
  gtk_box_pack_start (GTK_BOX (ret), embed, TRUE, TRUE, 0);
  
  return ret;  
}

static void
glide_theme_chooser_new_clicked (GtkWidget *button,
				 gpointer user_data)
{
  GlideThemeChooser *chooser = (GlideThemeChooser *)user_data;
  GlideTheme *theme = glide_theme_chooser_get_selection (chooser);

  chooser->priv->theme = theme;
  g_object_notify (G_OBJECT (chooser), "theme");

  g_signal_emit (chooser, theme_chooser_signals[RESPONSE], 0);
}

static void
glide_theme_chooser_cancel_clicked (GtkWidget *button,
				    gpointer user_data)
{
  GlideThemeChooser *chooser = (GlideThemeChooser *)user_data;

  chooser->priv->theme = NULL;
  g_object_notify (G_OBJECT (chooser), "theme");

  g_signal_emit (chooser, theme_chooser_signals[RESPONSE], 0);
}

static GtkWidget *
glide_theme_chooser_make_bottom_hbox (GlideThemeChooser *chooser)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *new, *cancel;
  
  new = gtk_button_new_with_label ("New Presentation");
  cancel = gtk_button_new_with_label ("Cancel");

  g_signal_connect (new, "clicked",
		    G_CALLBACK (glide_theme_chooser_new_clicked), chooser);
  g_signal_connect (cancel, "clicked",
		    G_CALLBACK (glide_theme_chooser_cancel_clicked), chooser);

  gtk_box_pack_start (GTK_BOX (ret), new, FALSE, 0, 0);
  gtk_box_pack_start (GTK_BOX (ret), cancel, FALSE, 0, 0);
  
  chooser->priv->new_button = new;
  
  return ret;
}

static GtkWidget *
glide_theme_chooser_make_main_vbox (GlideThemeChooser *chooser)
{
  GtkWidget *ret = gtk_vbox_new (FALSE, 0);
  GtkWidget *top, *bottom;
  
  top = glide_theme_chooser_make_top_hbox (chooser);
  bottom = glide_theme_chooser_make_bottom_hbox (chooser);
  
  gtk_box_pack_start (GTK_BOX (ret), top, TRUE, 0, 0);
  gtk_box_pack_start (GTK_BOX (ret), bottom, FALSE, 0, 0);
  
  return ret;  
}

static void
glide_theme_chooser_setup_ui (GlideThemeChooser *chooser)
{
  GtkWidget *vbox = glide_theme_chooser_make_main_vbox (chooser);
  
  gtk_container_add (GTK_CONTAINER (chooser), vbox);
}

static void
glide_theme_chooser_init (GlideThemeChooser *chooser)
{
  GLIDE_NOTE(THEME_CHOOSER, "Initializing theme chooser: %p", chooser);
  chooser->priv = GLIDE_THEME_CHOOSER_GET_PRIVATE (chooser);

  glide_theme_manager_refresh_theme_list ();
  glide_theme_manager_load_all ();
  chooser->priv->themes = glide_theme_manager_get_themes ();
  
  glide_theme_chooser_setup_ui (chooser);
  
  gtk_window_set_default_size (GTK_WINDOW (chooser), 650, 450);

}

static void
glide_theme_chooser_class_init (GlideThemeChooserClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_theme_chooser_finalize;
  object_class->get_property = glide_theme_chooser_get_property;
  object_class->set_property = glide_theme_chooser_set_property;

  g_object_class_install_property (object_class,
				   PROP_THEME,
				   g_param_spec_object ("theme",
							"Theme",
							"The selected theme",
							GLIDE_TYPE_THEME,
							G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  
  theme_chooser_signals[RESPONSE] = 
    g_signal_new ("response",
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_FIRST,
		  0,
		  NULL, NULL,
		  gi_cclosure_marshal_generic,
		  G_TYPE_NONE, 0);
  
  g_type_class_add_private (object_class, sizeof(GlideThemeChooserPrivate));
}

GtkWidget *
glide_theme_chooser_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_THEME_CHOOSER, NULL);
}

GlideTheme *
glide_theme_chooser_get_theme (GlideThemeChooser *chooser)
{
  return chooser->priv->theme;
}
