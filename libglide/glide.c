/*
 * glide.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "glide.h"
#include "glide-animation-manager.h"
#include "glide-theme-manager.h"
#include "glide-debug.h"
#include "glide-dirs.h"

guint glide_debug_flags = 0;
GlideTheme *default_theme = NULL;

#ifdef GLIDE_ENABLE_DEBUG
static const GDebugKey glide_debug_keys[] = {
  {"misc", GLIDE_DEBUG_MISC},
  {"image", GLIDE_DEBUG_IMAGE},
  {"manipulator", GLIDE_DEBUG_MANIPULATOR},
  {"stage-manager", GLIDE_DEBUG_STAGE_MANAGER},
  {"window", GLIDE_DEBUG_WINDOW},
  {"paint", GLIDE_DEBUG_PAINT},
  {"text", GLIDE_DEBUG_TEXT},
  {"document", GLIDE_DEBUG_DOCUMENT},
  {"theme-manager", GLIDE_DEBUG_THEME_MANAGER},
  {"theme-chooser", GLIDE_DEBUG_THEME_CHOOSER}
};

static gboolean
glide_arg_debug_cb (const char *key, const char *value, gpointer user_data)
{
  glide_debug_flags |=
	g_parse_debug_string (value, glide_debug_keys, G_N_ELEMENTS (glide_debug_keys));
  return TRUE;
}

static gboolean
glide_arg_no_debug_cb (const char *key, const char *value, gpointer user_data)
{
  glide_debug_flags &=
	~g_parse_debug_string (value, glide_debug_keys, G_N_ELEMENTS (glide_debug_keys));
  return TRUE;
}
#endif

static GOptionEntry glide_args[] = {
#ifdef GLIDE_ENABLE_DEBUG
  {"glide-debug", 0, 0, G_OPTION_ARG_CALLBACK, glide_arg_debug_cb,
   "Glide debugging messages to show. Comma seperated list of: all, misc, image, manipulator, stage-manager, window, text, document, or paint",
   "FLAGS"},
  {"glide-no-debug", 0, 0, G_OPTION_ARG_CALLBACK, glide_arg_no_debug_cb,
   "Disable glide debugging", "FLAGS"},
#endif
  {NULL,},
};

/**
 * glide_get_option_group:
 *
 * Returns the #GOptionGroup to be used for parsing Glide arguments
 *
 * Return value: The Glide #GOptionGroup
 */
GOptionGroup *
glide_get_option_group (void)
{
  GOptionGroup *group;
	
  group = g_option_group_new ("glide", "Glide Options",
							  "Show Glide Options", NULL, NULL);
  g_option_group_add_entries (group, glide_args);
	
  return group;
}

static gboolean
glide_parse_args (int *argc, char ***argv)
{
  GOptionContext *option_context;
  GOptionGroup *glide_group;
  GError *error = NULL;
  gboolean ret = TRUE;
  
  option_context = g_option_context_new (NULL);
  g_option_context_set_ignore_unknown_options (option_context, TRUE);
  g_option_context_set_help_enabled (option_context, TRUE);
  
  glide_group = glide_get_option_group ();
  g_option_context_add_group (option_context, glide_group);
  
  if (!g_option_context_parse (option_context, argc, argv, &error))
	{
	  if (error)
		{
		  g_warning ("%s", error->message);
		  g_error_free (error);
		}
	  
	  ret = FALSE;
	}
  g_option_context_free (option_context);
  
  return ret;
}

static void
glide_load_default_theme ()
{
  glide_theme_manager_refresh_theme_list ();
  glide_theme_manager_load_all ();
  
  default_theme = glide_theme_manager_get_theme ("Glide");
}

/**
 * glide_init:
 * @argc: A pointer to an integer containing an argument count.
 * @argv: A pointer to the argument array, or %NULL.
 *
 * Initializes Glide, and parses argv. Must be called prior to
 * other Glide functions.
 *
 * Return value: %TRUE on success, %FALSE on failure.
 */
gboolean
glide_init (int *argc, char ***argv)
{
  gboolean res = glide_parse_args (argc, argv);

  glide_animation_manager_register_animations ();
  glide_load_default_theme ();

  return res;
}

/**
 * glide_get_default_theme:
 *
 * Returns the default #GlideTheme to be used for new documents.
 *
 * Return value: The default #GlideTheme.
 */
GlideTheme *
glide_get_default_theme ()
{
  return default_theme;
}
