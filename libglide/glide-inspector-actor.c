/*
 * glide-inspector-actor.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * SECTION:glide-inspector-actor
 * @short_description: An inspector page for general actor properties.
 *
 */

#include "glide-inspector-actor.h"
#include "glide-inspector-actor-priv.h"

#include "glide-slide.h"

#include "glide-undo-manager.h"

#include "glide-gtk-util.h"

#include <string.h>
#include <math.h>

#define GLIDE_INSPECTOR_ACTOR_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_INSPECTOR_ACTOR, GlideInspectorActorPrivate))

G_DEFINE_TYPE(GlideInspectorActor, glide_inspector_actor, GTK_TYPE_ALIGNMENT);

enum {
  PROP_0,
  PROP_ACTOR
};

static void
glide_inspector_actor_get_center_pos (ClutterActor *actor, 
				      gfloat *x,
				      gfloat *y)
{
  ClutterActor *parent = clutter_actor_get_stage (actor);
  gfloat p_width, p_height, width, height;
  
  clutter_actor_get_size (parent, &p_width, &p_height);
  clutter_actor_get_size (actor, &width, &height);
  
  *x = floor(p_width/2.0 - width/2.0);
  *y = floor(p_height/2.0 - height/2.0);
}

static void
glide_inspector_actor_update_center_button_sensitive (GlideInspectorActor *ins)
{
  gfloat cx, cy, ax, ay;
  
  clutter_actor_get_position (CLUTTER_ACTOR (ins->priv->actor), &ax, &ay);
  glide_inspector_actor_get_center_pos (CLUTTER_ACTOR (ins->priv->actor), &cx, &cy);
  
  gtk_widget_set_sensitive (ins->priv->centerh_button, TRUE);
  gtk_widget_set_sensitive (ins->priv->centerv_button, TRUE);
  
  if (cx == ax)
    gtk_widget_set_sensitive (ins->priv->centerh_button, FALSE);
  if (cy == ay)
    gtk_widget_set_sensitive (ins->priv->centerv_button, FALSE);
}

static void
glide_inspector_actor_center_horizontally (GtkWidget *button,
					   gpointer user_data)
{
  GlideInspectorActor *ins = (GlideInspectorActor *)user_data;
  gfloat x, y;
  
  glide_inspector_actor_get_center_pos (CLUTTER_ACTOR (ins->priv->actor), &x, &y);
  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (ins->priv->actor),
					 ins->priv->actor, "Center actor horizontally");
  clutter_actor_set_x (CLUTTER_ACTOR (ins->priv->actor), x);
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (ins->priv->actor),
				       ins->priv->actor);
}

static void
glide_inspector_actor_center_vertically (GtkWidget *button,
					 gpointer user_data)
{
  GlideInspectorActor *ins = (GlideInspectorActor *)user_data;
  gfloat x, y;
  
  glide_inspector_actor_get_center_pos (CLUTTER_ACTOR (ins->priv->actor), &x, &y);
  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (ins->priv->actor),
					 ins->priv->actor, "Center actor vertically");
  clutter_actor_set_y (CLUTTER_ACTOR (ins->priv->actor), y);
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (ins->priv->actor),
				       ins->priv->actor);

}

static void
glide_inspector_actor_apply_geometry (GlideInspectorActor *ins)
{
  gfloat width, height, x, y;
  
  width = gtk_spin_button_get_value (GTK_SPIN_BUTTON (ins->priv->width_spin));
  height = gtk_spin_button_get_value (GTK_SPIN_BUTTON (ins->priv->height_spin));
  x = gtk_spin_button_get_value (GTK_SPIN_BUTTON (ins->priv->x_spin));
  y = gtk_spin_button_get_value (GTK_SPIN_BUTTON (ins->priv->y_spin));

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (ins->priv->actor),
					 ins->priv->actor, "Modify actor geometry");
  clutter_actor_set_position (CLUTTER_ACTOR (ins->priv->actor), x, y);
  clutter_actor_set_size (CLUTTER_ACTOR (ins->priv->actor), width, height);
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (ins->priv->actor),
				       ins->priv->actor);
}

static void
glide_inspector_actor_geometry_spin_changed (GtkWidget *spin,
					     gpointer user_data)
{
  GlideInspectorActor *ins = (GlideInspectorActor *)user_data;
  
  if (ins->priv->ignore_change)
    return;
  glide_inspector_actor_apply_geometry (ins);
}

static void
glide_inspector_actor_update_geometry (GlideInspectorActor *ins)
{
  gfloat width, height;
  gfloat x,y;
  
  clutter_actor_get_size (CLUTTER_ACTOR (ins->priv->actor), &width, &height);
  clutter_actor_get_position (CLUTTER_ACTOR (ins->priv->actor), &x, &y);

  ins->priv->ignore_change = TRUE;
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (ins->priv->x_spin), x);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (ins->priv->y_spin), y);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (ins->priv->width_spin), width);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (ins->priv->height_spin), height);
  ins->priv->ignore_change = FALSE;
}

static void
glide_inspector_actor_allocation_changed (GObject *object,
					  GParamSpec *pspec,
					  gpointer user_data)
{
  GlideInspectorActor *ins = (GlideInspectorActor *)user_data;
  
  glide_inspector_actor_update_center_button_sensitive (ins);
  glide_inspector_actor_update_geometry (ins);
}

static void
glide_inspector_actor_update_name (GlideInspectorActor *ins)
{
  gtk_entry_set_text (GTK_ENTRY (ins->priv->name_entry), clutter_actor_get_name (CLUTTER_ACTOR (ins->priv->actor)));
}

static void
glide_inspector_actor_name_entry_changed (GtkEntry *entry,
					  gpointer user_data)
{
  GlideInspectorActor *ins = (GlideInspectorActor *)user_data;
  
  clutter_actor_set_name (CLUTTER_ACTOR (ins->priv->actor),
			  gtk_entry_get_text (entry));
}

static void
glide_inspector_actor_finalize (GObject *object)
{
  
}

static void
glide_inspector_actor_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorActor *inspector = GLIDE_INSPECTOR_ACTOR (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      g_value_set_object (value, inspector->priv->actor);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_actor_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorActor *inspector = GLIDE_INSPECTOR_ACTOR (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      glide_inspector_actor_set_actor (inspector, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static GtkWidget *
glide_inspector_actor_make_geometry_control (GlideInspectorActor *ins, const gchar *text, GtkWidget **privloc)
{
  GtkWidget *align = gtk_alignment_new (0,0.5,1,0.0);
  GtkWidget *hbox = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new(text);
  // TODO: Range
  GtkWidget *button = gtk_spin_button_new_with_range(0,1000,1);
  
  glide_gtk_util_set_widget_font_small (button);
  
  *privloc = button;

  gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
  gtk_container_add (GTK_CONTAINER (align), label);
  
  gtk_box_pack_start (GTK_BOX (hbox), align, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  
  g_signal_connect (button, "value-changed", G_CALLBACK (glide_inspector_actor_geometry_spin_changed),
		    ins);

  return hbox;
}

static GtkWidget *
glide_inspector_actor_make_geometry_controls (GlideInspectorActor *ins)
{
  GtkWidget *ret = gtk_alignment_new (0.5,0,0.5,1);
  GtkWidget *vbox = gtk_vbox_new (FALSE, 0);
  GtkWidget *hbox1, *hbox2;
  
  gtk_alignment_set_padding (GTK_ALIGNMENT (ret), 2, 0, 0, 0);
  
  hbox1 = gtk_hbox_new (TRUE, 10);
  hbox2 = gtk_hbox_new (TRUE, 10);
  
  gtk_box_pack_start (GTK_BOX (hbox1), glide_inspector_actor_make_geometry_control (ins, "x:", &ins->priv->x_spin),
		      TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox1), glide_inspector_actor_make_geometry_control (ins, "y:", &ins->priv->y_spin),
		      TRUE, TRUE, 0);
  

  gtk_box_pack_start (GTK_BOX (hbox2), glide_inspector_actor_make_geometry_control (ins, "Width:", &ins->priv->width_spin),
		      TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), glide_inspector_actor_make_geometry_control (ins, "Height:", &ins->priv->height_spin),
		      TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox1, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, FALSE, 0);
  
  
  gtk_container_add (GTK_CONTAINER (ret), vbox);
  return ret;
}

static GtkWidget *
glide_inspector_actor_make_geometry_box (GlideInspectorActor *ins)
{
  GtkWidget *ret = gtk_vbox_new (FALSE, 0);
  GtkWidget *geometry_label = gtk_label_new (NULL);
  GtkWidget *geometry_controls = glide_inspector_actor_make_geometry_controls (ins);
  
  gtk_label_set_markup (GTK_LABEL (geometry_label), "<b>Geometry:</b>");
  gtk_misc_set_alignment (GTK_MISC (geometry_label), 0, 0);

  gtk_box_pack_start (GTK_BOX (ret), geometry_label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (ret), geometry_controls, FALSE, FALSE, 0);

  return ret;
}

static GtkWidget *
glide_inspector_actor_make_name_box (GlideInspectorActor *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 5);
  GtkWidget *name_label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0, 0.5, 1);
  
  ins->priv->name_entry = gtk_entry_new ();
  glide_gtk_util_set_widget_font_small (ins->priv->name_entry);
  
  gtk_label_set_markup (GTK_LABEL (name_label), "<b>Name:</b>");
  gtk_container_add (GTK_CONTAINER (align), ins->priv->name_entry);
  
  gtk_box_pack_start (GTK_BOX (ret), name_label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (ret), align, TRUE, TRUE, 0);
  
  g_signal_connect (ins->priv->name_entry, "changed",
		    G_CALLBACK (glide_inspector_actor_name_entry_changed),
		    ins);
  
  return ret;
}

static GtkWidget *
glide_inspector_actor_make_center_box (GlideInspectorActor *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *center_label = gtk_label_new (NULL);
  GtkWidget *button_box = gtk_hbox_new (TRUE, 0);
  GtkWidget *alignment = gtk_alignment_new (0.5,0,1,1);
  GtkWidget *hori = gtk_button_new_with_label ("Horizontally");
  GtkWidget *vert = gtk_button_new_with_label ("Vertically");
  
  gtk_button_set_relief (GTK_BUTTON (hori), GTK_RELIEF_NONE);
  gtk_button_set_relief (GTK_BUTTON (vert), GTK_RELIEF_NONE);
  
  glide_gtk_util_set_widget_font_small (gtk_bin_get_child (GTK_BIN (hori)));
  glide_gtk_util_set_widget_font_small (gtk_bin_get_child (GTK_BIN (vert)));
  
  gtk_label_set_markup (GTK_LABEL (center_label), "<b>Center: </b>");

  gtk_box_pack_start (GTK_BOX (button_box), hori, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (button_box), vert, FALSE, FALSE, 0);
  
  gtk_container_add (GTK_CONTAINER (alignment), button_box);
  
  gtk_box_pack_start (GTK_BOX (ret), center_label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (ret), alignment, TRUE, FALSE, 0);
  
  g_signal_connect (hori, "clicked", G_CALLBACK (glide_inspector_actor_center_horizontally), ins);
  g_signal_connect (vert, "clicked", G_CALLBACK (glide_inspector_actor_center_vertically), ins);

  ins->priv->centerh_button = hori;
  ins->priv->centerv_button = vert;

  return ret;
}

static void
glide_inspector_actor_setup_ui (GlideInspectorActor *ins)
{
  GtkWidget *vbox = gtk_vbox_new (FALSE, 2);
  GtkWidget *name_box = glide_inspector_actor_make_name_box (ins);
  GtkWidget *geometry_box = glide_inspector_actor_make_geometry_box (ins);
  GtkWidget *center_box = glide_inspector_actor_make_center_box (ins);
  
  gtk_box_pack_start (GTK_BOX (vbox), name_box, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (vbox), geometry_box, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (vbox), center_box, FALSE, FALSE, 2);
  
  gtk_container_add (GTK_CONTAINER (ins), vbox);
}

static void
glide_inspector_actor_init (GlideInspectorActor *inspector)
{
  inspector->priv = GLIDE_INSPECTOR_ACTOR_GET_PRIVATE (inspector);
  
  gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
  glide_inspector_actor_setup_ui (inspector);

  gtk_alignment_set (GTK_ALIGNMENT (inspector), .5, 0, 0.8, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (inspector), 5, 0, 0, 0);
}

static void
glide_inspector_actor_class_init (GlideInspectorActorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_inspector_actor_finalize;
  object_class->get_property = glide_inspector_actor_get_property;
  object_class->set_property = glide_inspector_actor_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_ACTOR,
				   g_param_spec_object ("actor",
							"Actor",
							"The actor we are inspecting",
							GLIDE_TYPE_ACTOR,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideInspectorActorPrivate));
}

GtkWidget *
glide_inspector_actor_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_INSPECTOR_ACTOR, NULL);
}


GlideActor *
glide_inspector_actor_get_actor (GlideInspectorActor *inspector)
{
  return inspector->priv->actor;
}

void
glide_inspector_actor_set_actor (GlideInspectorActor *inspector,
				 GlideActor *actor)
{
  if (inspector->priv->allocation_changed_id)
    {
      g_signal_handler_disconnect (inspector->priv->actor, inspector->priv->allocation_changed_id);
      inspector->priv->allocation_changed_id = 0;
    }
  inspector->priv->actor = actor;
  
  if (!actor)
    return;
  
  if (GLIDE_IS_SLIDE (actor))
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), TRUE);
      glide_inspector_actor_update_name (inspector);
      glide_inspector_actor_update_geometry (inspector);
      
      inspector->priv->allocation_changed_id = g_signal_connect (actor, "notify::allocation",
								 G_CALLBACK (glide_inspector_actor_allocation_changed),
								 inspector);
    }
  
  g_object_notify (G_OBJECT (inspector), "actor");
}
