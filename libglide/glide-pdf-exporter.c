/*
 * glide-pdf-exporter.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-pdf-exporter
 * @short_description: An exporter for Portable Document Format (PDF) files.
 *
 */


#include "glide-pdf-exporter.h"
#include "glide-actor.h"
#include "glide-gtk-util.h"

#include <cairo.h>
#include <cairo-pdf.h>

static void
glide_pdf_exporter_export_real (GlideDocument *d,
				const gchar *filename)
{
  cairo_surface_t *pdf_surface;
  cairo_t *cr;
  gint width, height, i;
  
  glide_document_get_size (d, &width, &height);
  
  pdf_surface = cairo_pdf_surface_create (filename, width, height);
  cr = cairo_create (pdf_surface);
  
  for (i = 0; i < glide_document_get_n_slides (d); i++)
    {
      GlideSlide *s = glide_document_get_nth_slide (d, i);
      
      glide_actor_print (GLIDE_ACTOR (s), cr);
      cairo_surface_show_page (pdf_surface);
    }
    
  cairo_surface_flush (pdf_surface);
  
  cairo_destroy (cr);
  cairo_surface_destroy (pdf_surface);
  
}

static void
glide_pdf_exporter_response_callback (GtkDialog *dialog,
				      int response,
				      gpointer user_data)
{
  GlideDocument *d = (GlideDocument *)user_data;
  if (response == GTK_RESPONSE_ACCEPT)
    {
      gchar *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
      
      glide_pdf_exporter_export_real (d, filename);
      g_free (filename);
    }
  
  gtk_widget_destroy (GTK_WIDGET (dialog));
}

/**
 * glide_pdf_exporter_export:
 * @document: A #GlideDocument
 *
 * Displays a file selection dialog and exports @document to
 * the selected file in PDF format.
 *
 */
void
glide_pdf_exporter_export (GlideDocument *document)
{
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));
  
  glide_gtk_util_show_save_dialog (G_CALLBACK (glide_pdf_exporter_response_callback), document);
}
