/*
 * glide-cairo-util.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/** 
 * SECTION:glide-cairo-util
 * @short_description: Glide cairo utility functions.
 *
 */



#include "glide-cairo-util.h"

void
glide_cairo_set_clutter_color (cairo_t *cr, ClutterColor *c)
{
  cairo_set_source_rgba (cr, c->red/255.0, c->green/255.0, c->blue/255.0, c->alpha/255.0);
}

void
glide_cairo_set_fg_color (cairo_t *cr, 
			  GtkWidget *widget,
			  GtkStateType state)
{
  GdkColor c;
  GtkStyle *style = gtk_widget_get_style (widget);
  
  c = style->fg[state];
  gdk_cairo_set_source_color (cr, &c);
  
}
			  
