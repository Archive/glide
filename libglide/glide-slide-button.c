/*
 * glide-slide-button.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-slide-button
 * @short_description: A button containing a slide thumbnail.
 *
 */

#include "glide-slide-button.h"
#include "glide-slide-button-priv.h"

#include "glide-cairo-util.h"

#define GLIDE_SLIDE_BUTTON_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_SLIDE_BUTTON, GlideSlideButtonPrivate))

G_DEFINE_TYPE(GlideSlideButton, glide_slide_button, GTK_TYPE_BUTTON);

enum {
  PROP_O,
  PROP_SLIDE
};

static void
glide_slide_button_undo_position_changed (GlideUndoManager *manager,
					  gpointer user_data)
{
  GlideSlideButton *b = (GlideSlideButton *)user_data;
  
  if (CLUTTER_ACTOR_IS_VISIBLE (b->priv->slide))
    gtk_widget_queue_draw (GTK_WIDGET (b));
}

static gboolean
glide_slide_button_drawing_area_expose (GtkWidget *drawing_area,
					GdkEventExpose *event,
					gpointer user_data)
{
  GlideSlideButton *b = (GlideSlideButton *)user_data;
  cairo_t *cr = gdk_cairo_create (gtk_widget_get_window (drawing_area));
  gfloat width, height;
  
  clutter_actor_get_size (CLUTTER_ACTOR (b->priv->slide), &width, &height);

  cairo_save (cr);
  cairo_scale (cr, 80.0/width, 60.0/height);
  glide_actor_print (GLIDE_ACTOR (b->priv->slide), cr);
  cairo_restore (cr);

  if (CLUTTER_ACTOR_IS_VISIBLE (b->priv->slide))
    {
      glide_cairo_set_fg_color (cr, drawing_area, GTK_STATE_NORMAL);
      cairo_set_line_width (cr, 3);
      cairo_rectangle (cr, 0, 0, 80, 60);
      cairo_stroke (cr);
    }

  cairo_destroy (cr);
  
  return FALSE;
}

static void
glide_slide_button_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideSlideButton *button = GLIDE_SLIDE_BUTTON (object);
  
  switch (prop_id)
    {
    case PROP_SLIDE:
      g_value_set_object (value, button->priv->slide);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_slide_button_set_property (GObject *object,
				 guint prop_id,
				 const GValue *value,
				 GParamSpec *pspec)
{
  GlideSlideButton *button = GLIDE_SLIDE_BUTTON (object);
  
  switch (prop_id)
    {
    case PROP_SLIDE:
      glide_slide_button_set_slide (button, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


static void
glide_slide_button_finalize (GObject *object)
{
  GlideSlideButton *button = (GlideSlideButton *)object;
  
  if (button->priv->manager)
    {
      if (button->priv->position_changed_id)
	{
	  g_signal_handler_disconnect (button->priv->manager, button->priv->position_changed_id);
	}
      g_object_unref (G_OBJECT (button->priv->manager));
    }
}

static void
glide_slide_button_clicked (GlideSlideButton *button, gpointer user_data)
{
  GlideStageManager *m = glide_actor_get_stage_manager (GLIDE_ACTOR (button->priv->slide));
  
  glide_stage_manager_set_current_slide (m, glide_slide_get_index (button->priv->slide));
}

static void
glide_slide_button_init (GlideSlideButton *button)
{
  button->priv = GLIDE_SLIDE_BUTTON_GET_PRIVATE (button);

  button->priv->drawing_area = gtk_drawing_area_new ();
  gtk_container_add (GTK_CONTAINER (button), button->priv->drawing_area);
  gtk_widget_show (GTK_WIDGET (button->priv->drawing_area));
  
  g_signal_connect (button, "clicked", G_CALLBACK (glide_slide_button_clicked), NULL);
  g_signal_connect (button->priv->drawing_area, "expose-event",
		    G_CALLBACK (glide_slide_button_drawing_area_expose),
		    button);

  gtk_widget_set_size_request (button->priv->drawing_area, 80, 60);
  
  gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
  gtk_button_set_focus_on_click (GTK_BUTTON (button), FALSE);
}

static void
glide_slide_button_class_init (GlideSlideButtonClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_slide_button_finalize;

  object_class->get_property = glide_slide_button_get_property;
  object_class->set_property = glide_slide_button_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_SLIDE,
				   g_param_spec_object ("slide",
							"Slide",
							"The slide the button should display",
							GLIDE_TYPE_SLIDE,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideSlideButtonPrivate));
}

/**
 * glide_slide_button_new:
 *
 * Returns a new #GlideSlideButton.
 *
 * Return value: The newly allocated #GlideSlideButton
 */

GtkWidget *
glide_slide_button_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_SLIDE_BUTTON, NULL);
}

/**
 * glide_slide_button_get_slide:
 * @button: A #GlideSlideButton
 * 
 * Returns the slide @button displays.
 *
 * Return value: The #GlideSlide for @button
 */
GlideSlide *
glide_slide_button_get_slide (GlideSlideButton *button)
{
  g_return_val_if_fail (GLIDE_IS_SLIDE_BUTTON (button), NULL);
  return button->priv->slide;
}

/**
 * glide_slide_button_set_slide:
 * @button: A #GlideSlideButton
 * @slide: The #GlideSlide to set for @button
 *
 * Sets the slide displayed for @button to @slide.
 *
 */
void
glide_slide_button_set_slide (GlideSlideButton *button,
			      GlideSlide *slide)
{
  g_return_if_fail (GLIDE_IS_SLIDE_BUTTON (button));
  g_return_if_fail (GLIDE_IS_SLIDE (slide));
  button->priv->manager = 
    (GlideUndoManager *)g_object_ref 
    (G_OBJECT (glide_stage_manager_get_undo_manager 
	       (glide_actor_get_stage_manager 
		(GLIDE_ACTOR (slide)))));
  button->priv->slide = slide;
  g_object_notify (G_OBJECT (button), "slide");
  
  button->priv->position_changed_id = g_signal_connect (button->priv->manager, "position-changed",
							G_CALLBACK (glide_slide_button_undo_position_changed), button);
}
