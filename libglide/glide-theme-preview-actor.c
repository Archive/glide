/*
 * glide-theme-preview-actor.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-theme-preview-actor
 * @short_description: A actor for rendering theme previews.
 *
 */

#include "glide-theme-preview-actor.h"
#include "glide-theme-preview-actor-priv.h"

#include "glide-theme-manager.h"

#include "glide-debug.h"

#define GLIDE_THEME_PREVIEW_ACTOR_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_THEME_PREVIEW_ACTOR, GlideThemePreviewActorPrivate))

G_DEFINE_TYPE(GlideThemePreviewActor, glide_theme_preview_actor, CLUTTER_TYPE_GROUP);

enum {
  PROP_0,
  PROP_THEME,
  PROP_SELECTED
};

static void
glide_theme_preview_actor_update_texture (GlideThemePreviewActor *preview)
{
  guint width, height;
  cairo_t *cr;
  
  clutter_cairo_texture_get_surface_size (CLUTTER_CAIRO_TEXTURE (preview->priv->preview_texture), &width, &height);
  cr = clutter_cairo_texture_create (CLUTTER_CAIRO_TEXTURE (preview->priv->preview_texture));
  glide_theme_preview (preview->priv->theme, cr, width, height);
  cairo_destroy (cr);
}

static void
glide_theme_preview_actor_finalize (GObject *object)
{
  GlideThemePreviewActor *preview = (GlideThemePreviewActor *)object;
  
  g_object_unref (G_OBJECT (preview->priv->border));
}

static void
glide_theme_preview_actor_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideThemePreviewActor *preview = (GlideThemePreviewActor *)object;
  switch (prop_id)
    {
    case PROP_THEME:
      g_value_set_object (value, preview->priv->theme);
      break;
    case PROP_SELECTED:
      g_value_set_boolean (value, preview->priv->selected);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_theme_preview_actor_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideThemePreviewActor *preview = (GlideThemePreviewActor *)object;
  switch (prop_id)
    {
    case PROP_THEME:
      glide_theme_preview_actor_set_theme (preview, (GlideTheme *)g_value_get_object (value));
      break;
    case PROP_SELECTED:
      glide_theme_preview_actor_set_selected (preview, g_value_get_boolean (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_theme_preview_actor_add_border (GlideThemePreviewActor *preview)
{
  ClutterActor *border = clutter_rectangle_new();
  ClutterColor trans = {0x00,0x00,0x00,0x00};
  ClutterColor grey = {0xdf,0xdf,0xdf,0xcc};
  
  clutter_rectangle_set_color (CLUTTER_RECTANGLE (border), &trans);
  clutter_rectangle_set_border_color (CLUTTER_RECTANGLE (border), &grey);
  
  clutter_rectangle_set_border_width (CLUTTER_RECTANGLE (border), 2);
  
  clutter_actor_set_size (border, 160, 120);
  clutter_actor_set_position (border, 0, 0);
  
  preview->priv->border = (ClutterActor *)g_object_ref ((GObject *)border);  
}

static void
glide_theme_preview_actor_init (GlideThemePreviewActor *preview)
{
  preview->priv = GLIDE_THEME_PREVIEW_ACTOR_GET_PRIVATE (preview);
  
  preview->priv->preview_texture = clutter_cairo_texture_new (160, 120);
  
  clutter_container_add_actor (CLUTTER_CONTAINER (preview), preview->priv->preview_texture);
  clutter_actor_set_position (preview->priv->preview_texture, 0, 0);

  glide_theme_preview_actor_add_border (preview);
  
  clutter_actor_set_reactive (CLUTTER_ACTOR (preview), TRUE);
}

static void
glide_theme_preview_actor_class_init (GlideThemePreviewActorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  object_class->finalize = glide_theme_preview_actor_finalize;
  object_class->get_property = glide_theme_preview_actor_get_property;
  object_class->set_property = glide_theme_preview_actor_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_THEME,
				   g_param_spec_object ("theme",
							"Theme",
							"The theme we are previewing",
							GLIDE_TYPE_THEME,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class,
				   PROP_SELECTED,
				   g_param_spec_boolean("selected",
							"Selected",
							"Whether the theme preview is selected",
							FALSE,
							G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideThemePreviewActorPrivate));
}

ClutterActor *
glide_theme_preview_actor_new ()
{
  return (ClutterActor *)g_object_new (GLIDE_TYPE_THEME_PREVIEW_ACTOR, NULL);
}

void
glide_theme_preview_actor_set_theme (GlideThemePreviewActor *preview,
			       GlideTheme *theme)
{
  preview->priv->theme = theme;
  g_object_notify (G_OBJECT (preview), "theme");
  
  glide_theme_preview_actor_update_texture (preview);
}

GlideTheme *
glide_theme_preview_actor_get_theme (GlideThemePreviewActor *preview)
{
  return preview->priv->theme;
}


gboolean
glide_theme_preview_actor_get_selected (GlideThemePreviewActor *preview)
{
  return preview->priv->selected;
}

void
glide_theme_preview_actor_set_selected (GlideThemePreviewActor *preview,
					gboolean selected)
{
  preview->priv->selected = selected;
  g_object_notify (G_OBJECT (preview), "selected");
  
  if (selected)
    {
      clutter_container_add_actor (CLUTTER_CONTAINER (preview), preview->priv->border);
      clutter_actor_show (preview->priv->border);
    }
  else
    {
      clutter_container_remove_actor (CLUTTER_CONTAINER (preview), preview->priv->border);
    }
}
