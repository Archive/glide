/*
 * glide-theme-preview-widget.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-theme-preview-widget
 * @short_description: A widget for rendering theme previews.
 *
 */

#include "glide-theme-preview-widget.h"
#include "glide-theme-preview-widget-priv.h"

#include "glide-theme-manager.h"

#include "glide-debug.h"

#define GLIDE_THEME_PREVIEW_WIDGET_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_THEME_PREVIEW_WIDGET, GlideThemePreviewWidgetPrivate))

G_DEFINE_TYPE(GlideThemePreviewWidget, glide_theme_preview_widget, GTK_TYPE_DRAWING_AREA);

enum {
  PROP_0,
  PROP_THEME
};

static gboolean
glide_theme_preview_widget_expose_event (GtkWidget *widget,
				  GdkEventExpose *event)
{
  GlideThemePreviewWidget *preview = (GlideThemePreviewWidget *)widget;
  GtkAllocation alloc;
  cairo_t *cr = gdk_cairo_create (gtk_widget_get_window (widget));
  
  cairo_set_source_rgba (cr, 0, 0, 0, 1);
  cairo_paint (cr);
  
  gtk_widget_get_allocation (widget, &alloc);

  glide_theme_preview (preview->priv->theme, cr, alloc.width, alloc.height);
  
  cairo_destroy (cr);
  
  return FALSE;
}

static void
glide_theme_preview_widget_finalize (GObject *object)
{
}

static void
glide_theme_preview_widget_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideThemePreviewWidget *preview = (GlideThemePreviewWidget *)object;
  switch (prop_id)
    {
    case PROP_THEME:
      g_value_set_object (value, preview->priv->theme);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_theme_preview_widget_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideThemePreviewWidget *preview = (GlideThemePreviewWidget *)object;
  switch (prop_id)
    {
    case PROP_THEME:
      glide_theme_preview_widget_set_theme (preview, (GlideTheme *)g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_theme_preview_widget_init (GlideThemePreviewWidget *preview)
{
  preview->priv = GLIDE_THEME_PREVIEW_WIDGET_GET_PRIVATE (preview);
}

static void
glide_theme_preview_widget_class_init (GlideThemePreviewWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = glide_theme_preview_widget_finalize;
  object_class->get_property = glide_theme_preview_widget_get_property;
  object_class->set_property = glide_theme_preview_widget_set_property;
  
  widget_class->expose_event = glide_theme_preview_widget_expose_event;

  g_object_class_install_property (object_class,
				   PROP_THEME,
				   g_param_spec_object ("theme",
							"Theme",
							"The theme we are previewing",
							GLIDE_TYPE_THEME,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  
  g_type_class_add_private (object_class, sizeof(GlideThemePreviewWidgetPrivate));
}

GtkWidget *
glide_theme_preview_widget_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_THEME_PREVIEW_WIDGET, NULL);
}

void
glide_theme_preview_widget_set_theme (GlideThemePreviewWidget *preview,
			       GlideTheme *theme)
{
  preview->priv->theme = theme;
  g_object_notify (G_OBJECT (preview), "theme");
  
  gtk_widget_queue_draw (GTK_WIDGET (preview));
}

GlideTheme *
glide_theme_preview_widget_get_theme (GlideThemePreviewWidget *preview)
{
  return preview->priv->theme;
}
