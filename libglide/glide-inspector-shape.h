/*
 * glide-inspector-shape.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_INSPECTOR_SHAPE_H__
#define __GLIDE_INSPECTOR_SHAPE_H__

#include <gtk/gtk.h>
#include "glide-actor.h"
#include "glide-binding.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_INSPECTOR_SHAPE              (glide_inspector_shape_get_type())
#define GLIDE_INSPECTOR_SHAPE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_INSPECTOR_SHAPE, GlideInspectorShape))
#define GLIDE_INSPECTOR_SHAPE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_INSPECTOR_SHAPE, GlideInspectorShapeClass))
#define GLIDE_IS_INSPECTOR_SHAPE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_INSPECTOR_SHAPE))
#define GLIDE_IS_INSPECTOR_SHAPE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_INSPECTOR_SHAPE))
#define GLIDE_INSPECTOR_SHAPE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_INSPECTOR_SHAPE, GlideInspectorShapeClass))

typedef struct _GlideInspectorShapePrivate GlideInspectorShapePrivate;


typedef struct _GlideInspectorShape GlideInspectorShape;

struct _GlideInspectorShape
{
  GtkAlignment alignment;
  
  GlideInspectorShapePrivate *priv;
};

typedef struct _GlideInspectorShapeClass GlideInspectorShapeClass;

struct _GlideInspectorShapeClass
{
  GtkAlignmentClass parent_class;
};

GType glide_inspector_shape_get_type (void) G_GNUC_CONST;
GtkWidget *glide_inspector_shape_new (void);

GlideActor *glide_inspector_shape_get_actor (GlideInspectorShape *inspector);
void glide_inspector_shape_set_actor (GlideInspectorShape *inspector, GlideActor *actor);

G_END_DECLS

#endif
