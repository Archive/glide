/*
 * glide-inspector-image.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-inspector-image
 * @short_description: An inspector page for image actors.
 *
 */

#include "glide-inspector-image.h"
#include "glide-inspector-image-priv.h"

#include "glide-image.h"

#include "glide-undo-manager.h"

#include "glide-gtk-util.h"

#include <string.h>
#include <math.h>

#define GLIDE_INSPECTOR_IMAGE_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_INSPECTOR_IMAGE, GlideInspectorImagePrivate))

G_DEFINE_TYPE(GlideInspectorImage, glide_inspector_image, GTK_TYPE_ALIGNMENT);

enum {
  PROP_0,
  PROP_ACTOR
};

static void
glide_inspector_image_update_filename (GlideInspectorImage *ins)
{
  gchar *path = glide_actor_get_resource_path (ins->priv->actor,
					       glide_image_get_filename (GLIDE_IMAGE (ins->priv->actor)));
  ins->priv->ignore_set = TRUE;
  gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (ins->priv->chooser_button), path);
  ins->priv->ignore_set = FALSE;
  g_free (path);
}

static void
glide_inspector_image_file_set (GtkFileChooserButton *file_button,
				gpointer user_data)
{
  GlideInspectorImage *ins = (GlideInspectorImage *)user_data;
  gchar *filename;
  
  if (ins->priv->ignore_set)
    return;
  
  filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_button));

  // TODO: Error
  // TODO: Doesn't work?
  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (ins->priv->actor),
					 ins->priv->actor, "Modify image filename");
  glide_image_set_from_file (GLIDE_IMAGE (ins->priv->actor), filename, NULL);
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (ins->priv->actor),
				       ins->priv->actor);
}

static void
glide_inspector_image_finalize (GObject *object)
{
  
}

static void
glide_inspector_image_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorImage *inspector = GLIDE_INSPECTOR_IMAGE (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      g_value_set_object (value, inspector->priv->actor);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_image_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorImage *inspector = GLIDE_INSPECTOR_IMAGE (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      glide_inspector_image_set_actor (inspector, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static GtkWidget*
glide_inspector_image_make_file_box (GlideInspectorImage *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0, 0.6, 1);
  GtkWidget *button = gtk_file_chooser_button_new ("Select image", GTK_FILE_CHOOSER_ACTION_OPEN);
  
  gtk_label_set_markup (GTK_LABEL (label), "<b>Image:</b>");
  
  gtk_container_add (GTK_CONTAINER (align), button);
  
  gtk_box_pack_start (GTK_BOX(ret), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(ret), align, TRUE, TRUE, 0);
  
  ins->priv->chooser_button = button;
  
  g_signal_connect (button, "file-set", G_CALLBACK (glide_inspector_image_file_set), ins);

  return ret;
}

static void
glide_inspector_image_setup_ui (GlideInspectorImage *ins)
{
  GtkWidget *vbox = gtk_vbox_new (FALSE, 0);
  GtkWidget *file_box = glide_inspector_image_make_file_box (ins);
  
  gtk_box_pack_start (GTK_BOX (vbox), file_box, FALSE, FALSE, 0);
  
  gtk_container_add (GTK_CONTAINER (ins), vbox);
}

static void
glide_inspector_image_init (GlideInspectorImage *inspector)
{
  inspector->priv = GLIDE_INSPECTOR_IMAGE_GET_PRIVATE (inspector);
  
  gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
  
  glide_inspector_image_setup_ui (inspector);

  gtk_alignment_set (GTK_ALIGNMENT (inspector), .5, 0, 0.8, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (inspector), 5, 0, 0, 0);
}

static void
glide_inspector_image_class_init (GlideInspectorImageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_inspector_image_finalize;
  object_class->get_property = glide_inspector_image_get_property;
  object_class->set_property = glide_inspector_image_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_ACTOR,
				   g_param_spec_object ("actor",
							"Actor",
							"The actor we are inspecting",
							GLIDE_TYPE_ACTOR,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideInspectorImagePrivate));
}

GtkWidget *
glide_inspector_image_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_INSPECTOR_IMAGE, NULL);
}


GlideActor *
glide_inspector_image_get_actor (GlideInspectorImage *inspector)
{
  return inspector->priv->actor;
}

void
glide_inspector_image_set_actor (GlideInspectorImage *inspector,
				 GlideActor *actor)
{
  inspector->priv->actor = actor;
  
  if (!actor)
    return;
  
  if (!GLIDE_IS_IMAGE (actor))
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), TRUE);
      glide_inspector_image_update_filename (inspector);
    }
  
  g_object_notify (G_OBJECT (inspector), "actor");
}
