/*
 * glide-slide-box.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-slide-box
 * @short_description: Displays a list of slide preview buttons.
 *
 */

#include "glide-slide-box.h"
#include "glide-slide-box-priv.h"

#include "glide-slide-button.h"

#define GLIDE_SLIDE_BOX_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_SLIDE_BOX, GlideSlideBoxPrivate))

G_DEFINE_TYPE(GlideSlideBox, glide_slide_box, GTK_TYPE_VBOX);

enum {
  PROP_0,
  PROP_STAGE_MANAGER
};

static void
glide_slide_box_remove_children (GlideSlideBox *box)
{
  GList *children = gtk_container_get_children (GTK_CONTAINER (box));
  GList *t;
  
  for (t = children; t; t = t->next)
    {
      GtkWidget *child = (GtkWidget *)t->data;
      gtk_widget_destroy (child);
    }
}

static GtkWidget*
glide_slide_box_find_slide_button (GlideSlideBox *box,
				   GlideSlide *slide)
{
  GList *children = gtk_container_get_children (GTK_CONTAINER (box));
  GList *t;
  
  for (t = children; t; t = t->next)
    {
      GtkBin *alignment = (GtkBin *)t->data;
      GlideSlideButton *button = GLIDE_SLIDE_BUTTON (gtk_bin_get_child (alignment));
      
      if (glide_slide_button_get_slide (button) == slide)
	return (GtkWidget *)button;
    }
  return NULL;
}
				   

static void
glide_slide_box_document_slide_added (GlideDocument *document,
				      GlideSlide *slide,
				      gpointer data)
{
  GlideSlideBox *box = (GlideSlideBox *)data;
  GtkWidget *b = glide_slide_button_new ();
  GtkWidget *alignment = gtk_alignment_new (0,0,0,0);
  
  gtk_container_add (GTK_CONTAINER (alignment), b);
  gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 5, 5, 9, 0);
  
  gtk_box_pack_start (GTK_BOX (box), alignment, FALSE, FALSE, 0);
  gtk_box_reorder_child (GTK_BOX (box), alignment, glide_slide_get_index (slide));
  gtk_widget_show_all (alignment);  
  
  glide_slide_button_set_slide (GLIDE_SLIDE_BUTTON (b), slide);
  
  g_message ("Box %p slide added", box);
}

static void
glide_slide_box_document_slide_removed (GlideDocument *document,
					GlideSlide *slide,
					gpointer data)
{
  GlideSlideBox *box = (GlideSlideBox *)data;
  GtkWidget *button = glide_slide_box_find_slide_button (box, slide);
  
  if (!button)
    {
      g_warning ("glide_slide_box_document_slided_removed: No button for slide with index %d",
		 glide_slide_get_index (slide));
      return;
    }
  gtk_widget_destroy (button);

}

static void
glide_slide_box_finalize (GObject *object)
{
  GlideSlideBox *box = (GlideSlideBox *)object;
  g_object_unref (G_OBJECT (box->priv->document));
}

static void
glide_slide_box_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideSlideBox *box = GLIDE_SLIDE_BOX (object);
  
  switch (prop_id)
    {
    case PROP_STAGE_MANAGER:
      g_value_set_object (value, box->priv->manager);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_slide_box_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideSlideBox *box = GLIDE_SLIDE_BOX (object);
  
  switch (prop_id)
    {
    case PROP_STAGE_MANAGER:
      glide_slide_box_set_stage_manager (box, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_slide_box_init (GlideSlideBox *box)
{
  box->priv = GLIDE_SLIDE_BOX_GET_PRIVATE (box);
}

static void
glide_slide_box_class_init (GlideSlideBoxClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_slide_box_finalize;
  object_class->get_property = glide_slide_box_get_property;
  object_class->set_property = glide_slide_box_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_STAGE_MANAGER,
				   g_param_spec_object ("stage-manager",
							"Stage manager",
							"The stage manager containing slides the box should render.",
							GLIDE_TYPE_STAGE_MANAGER,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideSlideBoxPrivate));
}

/**
 * glide_slide_box_new:
 *
 * Constructs a new #GlideSlideBox.
 *
 * Return value: The newly allocated #GlideSlideBox.
 */
GtkWidget *
glide_slide_box_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_SLIDE_BOX, NULL);
}


/**
 * glide_slide_box_get_stage_manager:
 * @box: A #GlideSlideBox.
 *
 * Returns the #GlideStageManager that @box is watching.
 *
 * Return value: The #GlideStageManager for @box.
 */
GlideStageManager *
glide_slide_box_get_stage_manager (GlideSlideBox *box)
{
  g_return_val_if_fail (GLIDE_IS_SLIDE_BOX(box), NULL);
  return box->priv->manager;
}

/**
 * glide_slide_box_set_stage_manager:
 * @box: A #GlideSlideBox.
 * @manager: A #GlideStageManager to set as the manager for @box.
 *
 * Sets @manager as the stage manager for @box. @box will listen to the
 * slide-added and slide-removed signals on the managers document
 * to populate its contents
 */
void
glide_slide_box_set_stage_manager (GlideSlideBox *box,
				   GlideStageManager *manager)
{
  GlideDocument *doc;
  
  g_return_if_fail (GLIDE_IS_SLIDE_BOX (box));
  g_return_if_fail (GLIDE_IS_STAGE_MANAGER (manager));

  doc  = glide_stage_manager_get_document (manager);

  if (box->priv->manager)
    {
      g_signal_handler_disconnect (box->priv->document, box->priv->slide_added_id);
      g_signal_handler_disconnect (box->priv->document, box->priv->slide_removed_id);
      glide_slide_box_remove_children (box);
    }
  box->priv->manager = manager;
  
  if (box->priv->document)
    g_object_unref (G_OBJECT (box->priv->document));
  box->priv->document = (GlideDocument *)g_object_ref (G_OBJECT (doc));

  box->priv->slide_added_id = g_signal_connect (doc, "slide-added",
						G_CALLBACK (glide_slide_box_document_slide_added),
						box);
  box->priv->slide_removed_id = g_signal_connect (doc, "slide-removed",
		    G_CALLBACK (glide_slide_box_document_slide_removed),
		    box);

  g_object_notify (G_OBJECT (box), "stage-manager");
}

