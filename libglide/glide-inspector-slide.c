/*
 * glide-inspector-slide.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-inspector-slide
 * @short_description: An inspector page for slide actors.
 *
 */

#include "glide-inspector-slide.h"
#include "glide-inspector-slide-priv.h"

#include "glide-slide.h"

#include "glide-undo-manager.h"

#include "glide-gtk-util.h"

#include <string.h>
#include <math.h>

#define GLIDE_INSPECTOR_SLIDE_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_INSPECTOR_SLIDE, GlideInspectorSlidePrivate))

G_DEFINE_TYPE(GlideInspectorSlide, glide_inspector_slide, GTK_TYPE_ALIGNMENT);

enum {
  PROP_0,
  PROP_ACTOR
};

static void
glide_inspector_slide_update_color (GlideInspectorSlide *ins)
{
  ClutterColor cc;
  GdkColor c;
  
  glide_slide_get_color (GLIDE_SLIDE (ins->priv->actor), &cc);
  glide_gdk_color_from_clutter_color (&cc, &c);
  
  ins->priv->ignore_set = TRUE;
  gtk_color_button_set_color (GTK_COLOR_BUTTON (ins->priv->color_button), &c);
  ins->priv->ignore_set = FALSE;
}

static void
glide_inspector_slide_update_filename (GlideInspectorSlide *ins)
{
  gchar *path = glide_actor_get_resource_path (ins->priv->actor,
					       glide_slide_get_background (GLIDE_SLIDE (ins->priv->actor)));
  ins->priv->ignore_set = TRUE;
  gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (ins->priv->chooser_button), path);
  ins->priv->ignore_set = FALSE;
  g_free (path);
}

static void
glide_inspector_slide_color_changed (GObject *object,
				     GParamSpec *pspec,
				     gpointer user_data)
{
  GlideInspectorSlide *ins = (GlideInspectorSlide *)user_data;
  glide_inspector_slide_update_color (ins);
}


static void
glide_inspector_slide_background_changed (GObject *object,
					  GParamSpec *pspec,
					  gpointer user_data)
{
  GlideInspectorSlide *ins = (GlideInspectorSlide *)user_data;
  glide_inspector_slide_update_filename (ins);
}

static void
glide_inspector_slide_color_set (GtkColorButton *widg,
				 gpointer user_data)
{
  GlideInspectorSlide *ins = (GlideInspectorSlide *)user_data;
  GdkColor c;
  ClutterColor cc;
  
  if (ins->priv->ignore_set)
    return;
  
  gtk_color_button_get_color (widg, &c);
  glide_clutter_color_from_gdk_color (&c, &cc);
  
  glide_undo_manager_start_slide_action (glide_actor_get_undo_manager (ins->priv->actor), GLIDE_SLIDE (ins->priv->actor),
					 "Set slide color.");
  glide_slide_set_color (GLIDE_SLIDE (ins->priv->actor), &cc);
  glide_undo_manager_end_slide_action (glide_actor_get_undo_manager (ins->priv->actor), GLIDE_SLIDE (ins->priv->actor));
}

static void
glide_inspector_slide_file_set (GtkFileChooserButton *file_button,
				gpointer user_data)
{
  GlideInspectorSlide *ins = (GlideInspectorSlide *)user_data;
  gchar *filename;
  
  if (ins->priv->ignore_set)
    return;
  
  filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_button));
  
  glide_undo_manager_start_slide_action (glide_actor_get_undo_manager (ins->priv->actor), GLIDE_SLIDE (ins->priv->actor),
					 "Set slide background.");
  glide_slide_set_background (GLIDE_SLIDE (ins->priv->actor), filename);
  glide_undo_manager_end_slide_action (glide_actor_get_undo_manager (ins->priv->actor), GLIDE_SLIDE (ins->priv->actor));
  
  g_free (filename);
}

static void
glide_inspector_slide_finalize (GObject *object)
{
  //  GlideInspectorSlide *inspector = (GlideInspectorSlide *)object;

}

static void
glide_inspector_slide_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorSlide *inspector = GLIDE_INSPECTOR_SLIDE (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      g_value_set_object (value, inspector->priv->actor);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_slide_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorSlide *inspector = GLIDE_INSPECTOR_SLIDE (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      glide_inspector_slide_set_actor (inspector, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static GtkWidget*
glide_inspector_slide_make_color_box (GlideInspectorSlide *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0, 0.6, 1);
  GtkWidget *button = gtk_color_button_new ();
  
  gtk_label_set_markup (GTK_LABEL (label), "<b>Background Color:</b>");
  
  gtk_container_add (GTK_CONTAINER (align), button);
  
  gtk_box_pack_start (GTK_BOX(ret), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(ret), align, TRUE, TRUE, 0);
  
  ins->priv->color_button = button;
  g_signal_connect (button, "color-set", G_CALLBACK (glide_inspector_slide_color_set), ins);

  return ret;
}

static GtkWidget*
glide_inspector_slide_make_file_box (GlideInspectorSlide *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0, 0.6, 1);
  GtkWidget *button = gtk_file_chooser_button_new ("Select image", GTK_FILE_CHOOSER_ACTION_OPEN);
  
  gtk_label_set_markup (GTK_LABEL (label), "<b>Background Image:</b>");
  
  gtk_container_add (GTK_CONTAINER (align), button);
  
  gtk_box_pack_start (GTK_BOX(ret), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(ret), align, TRUE, TRUE, 0);
  
  ins->priv->chooser_button = button;
  
  g_signal_connect (button, "file-set", G_CALLBACK (glide_inspector_slide_file_set), ins);

  return ret;
}

static void
glide_inspector_slide_setup_ui (GlideInspectorSlide *ins)
{
  GtkWidget *vbox = gtk_vbox_new (FALSE, 0);
  GtkWidget *file_box = glide_inspector_slide_make_file_box (ins);
  GtkWidget *color_box = glide_inspector_slide_make_color_box (ins);
  
  gtk_box_pack_start (GTK_BOX (vbox), file_box, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), color_box, FALSE, FALSE, 0);
  
  gtk_container_add (GTK_CONTAINER (ins), vbox);

}

static void
glide_inspector_slide_init (GlideInspectorSlide *inspector)
{
  inspector->priv = GLIDE_INSPECTOR_SLIDE_GET_PRIVATE (inspector);
  
  glide_inspector_slide_setup_ui (inspector);
  
  gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);

  gtk_alignment_set (GTK_ALIGNMENT (inspector), .5, 0, 0.8, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (inspector), 5, 0, 0, 0);
}

static void
glide_inspector_slide_class_init (GlideInspectorSlideClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_inspector_slide_finalize;
  object_class->get_property = glide_inspector_slide_get_property;
  object_class->set_property = glide_inspector_slide_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_ACTOR,
				   g_param_spec_object ("actor",
							"Actor",
							"The actor we are inspecting",
							GLIDE_TYPE_ACTOR,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideInspectorSlidePrivate));
}

GtkWidget *
glide_inspector_slide_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_INSPECTOR_SLIDE, NULL);
}


GlideActor *
glide_inspector_slide_get_actor (GlideInspectorSlide *inspector)
{
  return inspector->priv->actor;
}

void
glide_inspector_slide_set_actor (GlideInspectorSlide *inspector,
				 GlideActor *actor)
{
  if (inspector->priv->background_notify_id)
    {
      g_signal_handler_disconnect (inspector->priv->actor,
				   inspector->priv->background_notify_id);
      inspector->priv->background_notify_id = 0;
      
      g_signal_handler_disconnect (inspector->priv->actor,
				   inspector->priv->color_notify_id);
      inspector->priv->color_notify_id = 0;
      
    }

  if (!actor)
    {
      inspector->priv->actor = NULL;
      return;
    }

  inspector->priv->actor = actor;
  
  if (!GLIDE_IS_SLIDE (actor))
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), TRUE);
      glide_inspector_slide_update_filename (inspector);
      glide_inspector_slide_update_color (inspector);
      
      inspector->priv->background_notify_id = g_signal_connect (actor, "notify::background", G_CALLBACK (glide_inspector_slide_background_changed),
							  inspector);
      inspector->priv->color_notify_id = g_signal_connect (actor, "notify::color", G_CALLBACK (glide_inspector_slide_color_changed),
							  inspector);
    }
  
  g_object_notify (G_OBJECT (inspector), "actor");
}
