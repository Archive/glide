/*
 * glide-print-util.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/** 
 * SECTION:glide-print-util
 * @short_description: Glide print utility functions.
 *
 */

#include "glide-print-util.h"
#include "glide-actor.h"

void 
glide_print_util_begin_print (GtkPrintOperation *print,
			      GtkPrintContext *context,
			      gpointer user_data)
{
  GlideStageManager *manager = (GlideStageManager *)user_data;
  
  gtk_print_operation_set_n_pages (print, glide_document_get_n_slides (glide_stage_manager_get_document (manager)));
}


void
glide_print_util_draw_page (GtkPrintOperation *print,
			    GtkPrintContext *context,
			    gint page_nr,
			    gpointer user_data)
{
  GlideStageManager *manager = (GlideStageManager *)user_data;
  GlideSlide *slide;
  GlideDocument *document;
  cairo_t *cr;
  gfloat width, height, s_width, s_height, p_height;
  
  width = gtk_print_context_get_width (context);
  height = .75 * width;
  
  p_height = gtk_print_context_get_width (context);
  
  document = glide_stage_manager_get_document (manager);
  slide = glide_document_get_nth_slide (document, page_nr);
  
  clutter_actor_get_size (CLUTTER_ACTOR (slide), &s_width,
			  &s_height);
  
  cr = gtk_print_context_get_cairo_context (context);

  cairo_move_to (cr, 0, p_height/2.0-height/2.0);
  cairo_scale (cr, width/s_width, height/s_height);

  glide_actor_print (GLIDE_ACTOR (slide), cr);
}

void
glide_print_util_print (GlideStageManager *manager)
{
  GtkPrintOperation *print = gtk_print_operation_new ();
  GtkPrintSettings *settings = gtk_print_settings_new ();
  GtkPrintOperationResult res;

  gtk_print_settings_set_orientation (settings,
				      GTK_PAGE_ORIENTATION_LANDSCAPE);
  
  gtk_print_operation_set_print_settings (print, settings);
  
  g_signal_connect (print, "begin_print", 
		    G_CALLBACK (glide_print_util_begin_print), manager);
  g_signal_connect (print, "draw_page",
		    G_CALLBACK (glide_print_util_draw_page), manager);
  res = gtk_print_operation_run (print, GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG,
				 NULL, NULL);
  
  g_object_unref (print);
  
}
