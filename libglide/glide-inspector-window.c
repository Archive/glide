/*
 * glide-inspector-window.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * SECTION:glide-inspector-window
 * @short_description: A floating window containing the inspector.
 */

#include "glide-inspector-notebook.h"
#include "glide-inspector-window.h"
#include "glide-inspector-window-priv.h"

#define GLIDE_INSPECTOR_WINDOW_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_INSPECTOR_WINDOW, GlideInspectorWindowPrivate))

G_DEFINE_TYPE(GlideInspectorWindow, glide_inspector_window, GTK_TYPE_WINDOW);

enum {
  PROP_O,
  PROP_INSPECTOR
};

static gboolean
glide_inspector_window_delete_event (GtkWidget *w, gpointer user_data)
{
  gtk_widget_hide (w);
  return TRUE;
}

static void
glide_inspector_window_get_property (GObject *object,
				     guint prop_id,
				     GValue *value,
				     GParamSpec *pspec)
{
  GlideInspectorWindow *w = GLIDE_INSPECTOR_WINDOW (object);
  
  switch (prop_id)
    {
    case PROP_INSPECTOR:
      g_value_set_object (value, w->priv->inspector);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_window_finalize (GObject *object)
{
}

static void
glide_inspector_window_position (GlideInspectorWindow *window)
{
  GdkScreen *screen = gtk_window_get_screen (GTK_WINDOW (window));
  gint width, height;
  
  width = gdk_screen_get_width (screen);
  height = gdk_screen_get_height (screen);
  
  gtk_window_move (GTK_WINDOW (window), .85*width, .25*height);
}

static void
glide_inspector_window_init (GlideInspectorWindow *window)
{
  window->priv = GLIDE_INSPECTOR_WINDOW_GET_PRIVATE (window);
  window->priv->inspector = glide_inspector_notebook_new ();
  
  g_signal_connect (window, "delete-event",
		    G_CALLBACK (glide_inspector_window_delete_event), NULL);
  
  gtk_window_set_skip_taskbar_hint (GTK_WINDOW (window), TRUE);
  gtk_window_set_skip_pager_hint (GTK_WINDOW (window), TRUE);
  gtk_window_set_keep_above (GTK_WINDOW (window), TRUE);
  
  gtk_container_add (GTK_CONTAINER (window), window->priv->inspector);
  
  gtk_widget_show_all (GTK_WIDGET (window));
  
  glide_inspector_window_position (window);
}
 
static void
glide_inspector_window_class_init (GlideInspectorWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = glide_inspector_window_finalize;

  object_class->get_property = glide_inspector_window_get_property;
  
  g_object_class_install_property (object_class,
				   PROP_INSPECTOR,
				   g_param_spec_object ("inspector",
							"Inspector",
							"The inspector notebook object we contain",
							GLIDE_TYPE_INSPECTOR_NOTEBOOK,
							G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  
  g_type_class_add_private (object_class, sizeof(GlideInspectorWindowPrivate));
}
 
GtkWidget *
glide_inspector_window_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_INSPECTOR_WINDOW, NULL);
}

GtkWidget *
glide_inspector_window_get_inspector (GlideInspectorWindow *w)
{
  return w->priv->inspector;
}


