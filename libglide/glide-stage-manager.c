/*
 * glide-stage-manager.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-stage-manager
 * @short_description: Orchestrates editing and presenting actions.
 *
 */

 
#include <glib/gi18n.h>
#include "glide-stage-manager.h"

#include <math.h>

#include "glide-stage-manager-priv.h"
#include "glide-manipulator.h"
#include "glide-actor.h"
#include "glide-slide.h"

#include "glide-animations.h"

#include "glide-debug.h"

G_DEFINE_TYPE(GlideStageManager, glide_stage_manager, G_TYPE_OBJECT)

#define GLIDE_STAGE_MANAGER_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_STAGE_MANAGER, GlideStageManagerPrivate))

enum {
  PROP_0,
  PROP_STAGE,
  PROP_SELECTION,
  PROP_DOCUMENT,
  PROP_CURRENT_SLIDE,
  PROP_PRESENTING,
  PROP_UNDO_MANAGER
};

enum {
  SELECTION_CHANGED,
  LAST_SIGNAL
};

static guint stage_manager_signals[LAST_SIGNAL] = { 0, };

static void
glide_stage_manager_effect_fade_completed (ClutterTimeline *t, gpointer user_data)
{
  ClutterActor *a = (ClutterActor *)user_data;
  
  clutter_actor_set_opacity (a, 0xff);
  clutter_actor_hide (a);
}

static void
glide_stage_manager_effect_completed (ClutterTimeline *t, gpointer user_data)
{
  GlideStageManager *manager = (GlideStageManager *)user_data;
  manager->priv->effect_timeline = NULL;
}

static void 
glide_stage_manager_effect_fade (GlideStageManager *manager, 
				 ClutterActor *a, ClutterActor *b)
{
  ClutterTimeline *timeline = clutter_timeline_new (350);
  
  if (manager->priv->effect_timeline)
    clutter_timeline_advance (manager->priv->effect_timeline, clutter_timeline_get_duration (manager->priv->effect_timeline));

  clutter_actor_show_all (b);
  clutter_actor_raise (b, a);
  
  clutter_actor_set_opacity (b, 0x00);
  
  clutter_actor_animate_with_timeline (a, CLUTTER_LINEAR, timeline, "opacity", 0x00, NULL);
  clutter_actor_animate_with_timeline (b, CLUTTER_LINEAR, timeline, "opacity", 0xff, NULL);
  
  clutter_timeline_start (timeline);
  manager->priv->effect_timeline = timeline;
  
  g_signal_connect (timeline, "completed", G_CALLBACK (glide_stage_manager_effect_fade_completed), a);
  g_signal_connect (timeline, "completed", G_CALLBACK (glide_stage_manager_effect_completed), manager);
}

static void
glide_stage_manager_effect_fade_in (ClutterActor *actor) 
{
  clutter_actor_set_opacity (actor, 0x00);
  clutter_actor_animate (actor, CLUTTER_LINEAR, 500, "opacity", 0xff, NULL);
}

static void
glide_stage_manager_finalize (GObject *object)
{
  GlideStageManager *manager = GLIDE_STAGE_MANAGER (object);
  GLIDE_NOTE (STAGE_MANAGER, "Finalizing stage manager: %p",
	      object);
  
  if (manager->priv->button_notify_id)
    g_signal_handler_disconnect (manager->priv->stage, manager->priv->button_notify_id);
  if (manager->priv->key_notify_id)
    g_signal_handler_disconnect (manager->priv->stage, manager->priv->key_notify_id);
  
  if (manager->priv->manip)
    g_object_unref (G_OBJECT (manager->priv->manip));
  g_object_unref (G_OBJECT (manager->priv->document));

  G_OBJECT_CLASS (glide_stage_manager_parent_class)->finalize (object);
}


static void
glide_stage_manager_get_property (GObject *object,
				  guint prop_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  GlideStageManager *manager = GLIDE_STAGE_MANAGER (object);
  
  switch (prop_id)
    {
    case PROP_STAGE:
      g_value_set_object (value, manager->priv->stage);
      break;
    case PROP_SELECTION:
      g_value_set_object (value, manager->priv->selection);
      break;
    case PROP_DOCUMENT:
      g_value_set_object (value, manager->priv->document);
      break;
    case PROP_UNDO_MANAGER:
      g_value_set_object (value, manager->priv->undo_manager);
      break;
    case PROP_CURRENT_SLIDE:
      g_value_set_uint (value, manager->priv->current_slide);
      break;
    case PROP_PRESENTING:
      g_value_set_boolean (value, manager->priv->presenting);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_stage_manager_set_selection_real (GlideStageManager *m,
					GlideActor *a)
{
  GlideActor *old = m->priv->selection;
  
  if (old == a)
    return;
  
  GLIDE_NOTE (STAGE_MANAGER, "Selection changed from: %s (%p) to %s (%p)",
	      old ? GLIDE_ACTOR_DISPLAY_NAME (old) : "unknown", old,
	      a ? GLIDE_ACTOR_DISPLAY_NAME (a) : "unknown", a);
  
  if (a)
    GLIDE_NOTE (STAGE_MANAGER, "New selection geometry: (%f, %f), (%f, %f)",
		clutter_actor_get_x (CLUTTER_ACTOR (a)), clutter_actor_get_y (CLUTTER_ACTOR (a)),
		clutter_actor_get_width (CLUTTER_ACTOR (a)), clutter_actor_get_height (CLUTTER_ACTOR (a)));
  m->priv->selection = a;

  
  if (a){

    clutter_actor_raise_top (CLUTTER_ACTOR (a));
    
    if (GLIDE_IS_SLIDE (a))
      glide_manipulator_set_target (m->priv->manip, NULL);
    else
      {
	glide_manipulator_set_target(m->priv->manip, CLUTTER_ACTOR (a));
	glide_manipulator_set_width_only(m->priv->manip, FALSE);
      }
  }
  
  g_signal_emit (m, stage_manager_signals[SELECTION_CHANGED], 0, old);
}

static void
glide_stage_manager_add_manipulator (GlideStageManager *manager)
{
  GlideManipulator *manip;
  ClutterActor *parent;
  
  if (!manager->priv->manip)
    manip = glide_manipulator_new (NULL);
  else
    manip = manager->priv->manip;
  
  if ((parent = clutter_actor_get_parent (CLUTTER_ACTOR (manip))))
    {
      clutter_container_remove_actor (CLUTTER_CONTAINER (parent), CLUTTER_ACTOR (manip));
    }
  
  glide_slide_add_actor_content (glide_document_get_nth_slide (manager->priv->document,
										manager->priv->current_slide), 
						  CLUTTER_ACTOR (manip));
  
  clutter_actor_hide_all (CLUTTER_ACTOR (manip));
  
  manager->priv->manip = g_object_ref (G_OBJECT (manip));
}

/**
 * glide_stage_manager_set_slide:
 * @manager: A #GlideStageManager
 * @slide: Index of new current slide for @manager
 *
 * Sets the current slide for @manager to the slide at
 * index @slide in the document.
 *
 */
void 
glide_stage_manager_set_slide (GlideStageManager *manager,
			       guint slide)
{
  GlideSlide *a, *b;
  
  g_return_if_fail (GLIDE_IS_STAGE_MANAGER (manager));

  b = glide_document_get_nth_slide (manager->priv->document, slide);
  
  if (manager->priv->current_slide == slide)
    return;
  
  if (manager->priv->current_slide >= 0 && !(manager->priv->current_slide >= glide_document_get_n_slides (manager->priv->document)))
    a = glide_document_get_nth_slide (manager->priv->document,
				      manager->priv->current_slide);
  else
    a = NULL;
  
  if (!a)
    clutter_actor_show_all (CLUTTER_ACTOR (b));
  else if (manager->priv->presenting)
    {
      clutter_actor_hide_all (CLUTTER_ACTOR (a));
      clutter_actor_show_all (CLUTTER_ACTOR (b));
    }
  else
    {
      glide_stage_manager_effect_fade (manager, CLUTTER_ACTOR (a), CLUTTER_ACTOR (b));
    }
  
  manager->priv->current_slide = slide;
  
  glide_stage_manager_add_manipulator (manager);
  glide_stage_manager_set_selection (manager, GLIDE_ACTOR (b));
    
  GLIDE_NOTE (STAGE_MANAGER, "Switching to slide index: %u", glide_slide_get_index (b));

  g_object_notify (G_OBJECT (manager), "current-slide");
}

static void
glide_stage_manager_document_slide_removed_cb (GlideDocument *document, 
					       GlideSlide *slide, 
					       gpointer data)
{
  GlideStageManager *manager = (GlideStageManager *)data;
  ClutterContainer *parent = CLUTTER_CONTAINER (clutter_actor_get_parent (CLUTTER_ACTOR (slide)));
  clutter_container_remove_actor (parent, CLUTTER_ACTOR (slide));
  
  if (manager->priv->current_slide < glide_document_get_n_slides(manager->priv->document))
    glide_stage_manager_set_slide (manager, manager->priv->current_slide);
  else
    glide_stage_manager_set_slide (manager, manager->priv->current_slide-1);
}

static void
glide_stage_manager_document_slide_added_cb (GlideDocument *document, 
					     GlideSlide *slide, 
					     gpointer data)
{
  GlideStageManager *manager = (GlideStageManager *)data;
  gfloat width, height;
  
  clutter_container_add_actor (CLUTTER_CONTAINER (manager->priv->stage), CLUTTER_ACTOR (slide));
  glide_actor_set_stage_manager (GLIDE_ACTOR (slide), manager);
  
  clutter_actor_get_size (manager->priv->stage, &width, &height);
  clutter_actor_set_size (CLUTTER_ACTOR (slide), width, height);
  
  glide_stage_manager_set_slide (manager, manager->priv->current_slide+1);
  glide_stage_manager_set_selection (manager, GLIDE_ACTOR (slide));
}

/**
 * glide_stage_manager_set_document:
 * @manager: A #GlideStageManager
 * @document: A #GlideDocument
 *
 * Sets @document as the document of @manager.
 *
 * BUG: Should probably be a construct only property?
 */
void
glide_stage_manager_set_document (GlideStageManager *manager,
				  GlideDocument *document)
{
  g_return_if_fail (GLIDE_IS_STAGE_MANAGER (manager));
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));

  manager->priv->document = g_object_ref (document);
  g_signal_connect (document, "slide-added", G_CALLBACK (glide_stage_manager_document_slide_added_cb), manager);
  g_signal_connect (document, "slide-removed", G_CALLBACK (glide_stage_manager_document_slide_removed_cb), manager);
}

static void
glide_stage_manager_timeline_completed (ClutterTimeline *timeline,
					gpointer user_data)
{
  GlideStageManager *manager = (GlideStageManager *)user_data;

  manager->priv->animating = FALSE;
}

/**
 * glide_stage_manager_advance_slide:
 * @manager: A #GlideStageManager 
 *
 * Advances to the next slide for @manager with animations.
 *
 */
void
glide_stage_manager_advance_slide (GlideStageManager *manager)
{
  g_return_if_fail (GLIDE_IS_STAGE_MANAGER (manager));

  if (manager->priv->animating)
    return;
  if (manager->priv->current_slide + 1 < glide_document_get_n_slides(manager->priv->document))
    {
      ClutterTimeline *t;
      GlideSlide *a, *b;
      const GlideAnimationInfo *a_info;

      
      a = glide_document_get_nth_slide (manager->priv->document, manager->priv->current_slide);
      b = glide_document_get_nth_slide (manager->priv->document, manager->priv->current_slide+1);

      a_info = glide_slide_get_animation (a);
      
      if (!a_info || !a_info->animation ||!strcmp(a_info->animation->name, "None"))
	{
	  glide_stage_manager_set_slide_next (manager);
	  return;
	}

      manager->priv->current_slide++;
      
      t = glide_animation_manager_do_animation (a_info, CLUTTER_ACTOR (a), CLUTTER_ACTOR (b));
      manager->priv->animating = TRUE;
      g_signal_connect (t, "completed", G_CALLBACK (glide_stage_manager_timeline_completed), manager);
      // XXX: Maybe not?
      g_object_notify (G_OBJECT (manager), "current-slide");
    }

  else
    glide_stage_manager_set_presenting (manager, FALSE);
}

/**
 * glide_stage_manager_reverse_slide:
 * @manager: A #GlideStageManager
 *
 * Advances to the previous slide for @manager with animations.
 *
 * BUG: This is for presenting, but kind of weird with the set_slide_next/prev API.
 *      actually the whole slide API sucks...have to use document+manager to get a slide?
 *     
 */
void
glide_stage_manager_reverse_slide (GlideStageManager *manager)
{
  g_return_if_fail (GLIDE_IS_STAGE_MANAGER (manager));

  glide_stage_manager_set_slide_prev (manager);
}

static gboolean
glide_stage_manager_button_pressed (ClutterActor *actor,
				    ClutterEvent *event,
				    GlideStageManager *manager)
{
  GLIDE_NOTE (STAGE_MANAGER, "Button press event at: "
	      "(%f, %f)", event->button.x, event->button.y);
  if (event->button.button == 1)
    {
      if (manager->priv->presenting)
	glide_stage_manager_advance_slide (manager);
      else
	glide_stage_manager_set_selection (manager, GLIDE_ACTOR (glide_document_get_nth_slide (manager->priv->document, manager->priv->current_slide)));

      return TRUE;
    }
  else if (event->button.button == 3)
    {
      if (manager->priv->presenting)
	glide_stage_manager_reverse_slide (manager);
      return TRUE;
    }
  return FALSE;
}

static gboolean
glide_stage_manager_key_pressed (ClutterActor *actor,
				 ClutterKeyEvent *event,
				 gpointer user_data)
{
  GlideStageManager *m = (GlideStageManager *)user_data;
  ClutterBindingPool *pool;
  
  pool = clutter_binding_pool_find (g_type_name (GLIDE_TYPE_STAGE_MANAGER));
  
  return clutter_binding_pool_activate (pool, event->keyval, event->modifier_state,
					G_OBJECT (m));  
}

static void
glide_stage_manager_set_property (GObject *object,
				  guint prop_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
  GlideStageManager *manager = GLIDE_STAGE_MANAGER (object);
  
  switch (prop_id)
    {
    case PROP_STAGE:
      g_return_if_fail (manager->priv->stage == NULL);
      manager->priv->stage = CLUTTER_ACTOR (g_value_get_object (value));
      
      manager->priv->button_notify_id = g_signal_connect (G_OBJECT (manager->priv->stage), "button-press-event", G_CALLBACK(glide_stage_manager_button_pressed), manager);
      manager->priv->key_notify_id = g_signal_connect (G_OBJECT (manager->priv->stage), "key-press-event", G_CALLBACK(glide_stage_manager_key_pressed), manager);
      break;
    case PROP_DOCUMENT:
      g_return_if_fail (manager->priv->document == NULL);
      glide_stage_manager_set_document (manager, GLIDE_DOCUMENT (g_value_get_object (value)));
      break;
    case PROP_SELECTION:
      glide_stage_manager_set_selection_real (manager,
	      GLIDE_ACTOR (g_value_get_object (value)));
      break;
    case PROP_CURRENT_SLIDE:
      glide_stage_manager_set_slide (manager,
				     g_value_get_uint (value));
      break;
    case PROP_PRESENTING:
      glide_stage_manager_set_presenting (manager,
					  g_value_get_boolean (value));
      break;
    case PROP_UNDO_MANAGER:
      glide_stage_manager_set_undo_manager (manager,
					    GLIDE_UNDO_MANAGER (g_value_get_object (value)));
      break;
    default: 
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static GObject *
glide_stage_manager_constructor (GType type,
				 guint n_properties,
				 GObjectConstructParam *properties)
{
  GObject *obj;
  guint i, n;
  GlideStageManager *manager;

  {
    /* Always chain up to the parent constructor */
    GObjectClass *parent_class;  
    parent_class = G_OBJECT_CLASS (glide_stage_manager_parent_class);
    obj = parent_class->constructor (type, n_properties, properties);
  }
  manager = GLIDE_STAGE_MANAGER (obj);
  n = glide_document_get_n_slides (manager->priv->document);
  for (i = 0; i < n; i++)
    {
      GlideSlide *slide = glide_document_get_nth_slide (manager->priv->document, i);
      clutter_container_add_actor (CLUTTER_CONTAINER (manager->priv->stage),
				   CLUTTER_ACTOR (slide));
      clutter_actor_hide (CLUTTER_ACTOR (slide));
    }
  
  manager->priv->current_slide = n-1;
  if (n > 0)
    clutter_actor_show_all (CLUTTER_ACTOR (glide_document_get_nth_slide (manager->priv->document, 0)));  

  //glide_stage_manager_add_manipulator (manager);

  return obj;
}

static gboolean
glide_stage_manager_binding_end_presentation (GlideStageManager         *self,
					      const gchar         *action,
					      guint                keyval,
					      ClutterModifierType  modifiers)
{
  if (self->priv->presenting)
    {
      glide_stage_manager_set_presenting (self, FALSE);
      return TRUE;
    }
  return FALSE;
}

static gboolean
glide_stage_manager_binding_next (GlideStageManager         *self,
				  const gchar         *action,
				  guint                keyval,
				  ClutterModifierType  modifiers)
{
  if (self->priv->presenting)
    {
      glide_stage_manager_advance_slide (self);
      
      return TRUE;
    }
  return FALSE;
}

static gboolean
glide_stage_manager_binding_move_up (GlideStageManager         *self,
				     const gchar         *action,
				     guint                keyval,
				     ClutterModifierType  modifiers)
{
  ClutterActor *selection = CLUTTER_ACTOR (glide_stage_manager_get_selection (self));

   if (self->priv->presenting)
    return FALSE;
  
  if (!selection || GLIDE_IS_SLIDE(selection))
    return FALSE;

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
					 GLIDE_ACTOR (selection),
					 "Move actor");
  clutter_actor_set_y (selection, clutter_actor_get_y (selection) - 1);
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
				       GLIDE_ACTOR (selection));
  
  return TRUE;
}

static gboolean
glide_stage_manager_binding_move_down (GlideStageManager         *self,
				     const gchar         *action,
				     guint                keyval,
				     ClutterModifierType  modifiers)
{
  ClutterActor *selection = CLUTTER_ACTOR (glide_stage_manager_get_selection (self));

  if (self->priv->presenting)
    return FALSE;
  
  if (!selection || GLIDE_IS_SLIDE(selection))
    return FALSE;

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
					 GLIDE_ACTOR (selection),
					 "Move actor");  
  clutter_actor_set_y (selection, clutter_actor_get_y (selection) + 1);
glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
				       GLIDE_ACTOR (selection));
  
  return TRUE;
}

static gboolean
glide_stage_manager_binding_move_left (GlideStageManager         *self,
				       const gchar         *action,
				       guint                keyval,
				       ClutterModifierType  modifiers)
{
  ClutterActor *selection = CLUTTER_ACTOR (glide_stage_manager_get_selection (self));

  if (self->priv->presenting)
    {
      glide_stage_manager_reverse_slide (self);
      return TRUE;
    }
  
  if (!selection || GLIDE_IS_SLIDE(selection))
    return FALSE;

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
					 GLIDE_ACTOR (selection),
					 "Move actor");  
  clutter_actor_set_x (selection, clutter_actor_get_x (selection) - 1);
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
				     GLIDE_ACTOR (selection));
  
  return TRUE;
}

static gboolean
glide_stage_manager_binding_move_right (GlideStageManager         *self,
					const gchar         *action,
					guint                keyval,
					ClutterModifierType  modifiers)
{
  ClutterActor *selection = CLUTTER_ACTOR (glide_stage_manager_get_selection (self));

  if (self->priv->presenting)
    {
      glide_stage_manager_advance_slide (self);
	
      return TRUE;
    }
  if (!selection || GLIDE_IS_SLIDE(selection))
    return FALSE;

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
					 GLIDE_ACTOR (selection),
					 "Move actor");  
  clutter_actor_set_x (selection, clutter_actor_get_x (selection) + 1);
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
				     GLIDE_ACTOR (selection));
  
  return TRUE;
}

static gboolean
glide_stage_manager_binding_move_up_snap (GlideStageManager         *self,
					  const gchar         *action,
					  guint                keyval,
					  ClutterModifierType  modifiers)
{
  ClutterActor *selection = CLUTTER_ACTOR (glide_stage_manager_get_selection (self));

  if (self->priv->presenting)
    return FALSE;
  
  if (!selection || GLIDE_IS_SLIDE(selection))
    return FALSE;

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
					 GLIDE_ACTOR (selection),
					 "Move actor");  
  clutter_actor_set_y (selection, (clutter_actor_get_y(selection) - 10)-(gint)(clutter_actor_get_y(selection) - 10)%10);

  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
				       GLIDE_ACTOR (selection));
  
  return TRUE;
}

static gboolean
glide_stage_manager_binding_move_down_snap (GlideStageManager         *self,
					    const gchar         *action,
					    guint                keyval,
					    ClutterModifierType  modifiers)
{
  ClutterActor *selection = CLUTTER_ACTOR (glide_stage_manager_get_selection (self));

  if (self->priv->presenting)
    return FALSE;
  
  if (!selection || GLIDE_IS_SLIDE(selection))
    return FALSE;

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
					 GLIDE_ACTOR (selection),
					 "Move actor");
  clutter_actor_set_y (selection, (clutter_actor_get_y(selection) + 10)-(gint)(clutter_actor_get_y(selection) + 10)%10);  
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
				       GLIDE_ACTOR (selection));
  
  return TRUE;
}

static gboolean
glide_stage_manager_binding_move_left_snap (GlideStageManager         *self,
					    const gchar         *action,
					    guint                keyval,
					    ClutterModifierType  modifiers)
{
  ClutterActor *selection = CLUTTER_ACTOR (glide_stage_manager_get_selection (self));

  if (self->priv->presenting)
    {
      return FALSE;
    }
  
  if (!selection || GLIDE_IS_SLIDE(selection))
    return FALSE;

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
					 GLIDE_ACTOR (selection),
					 "Move actor");  
  clutter_actor_set_x (selection, (clutter_actor_get_x(selection) - 10)-(gint)(clutter_actor_get_x(selection) - 10)%10);  
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
				       GLIDE_ACTOR (selection));

  
  return TRUE;
}

static gboolean
glide_stage_manager_binding_move_right_snap (GlideStageManager         *self,
					     const gchar         *action,
					     guint                keyval,
					     ClutterModifierType  modifiers)
{
  ClutterActor *selection = CLUTTER_ACTOR (glide_stage_manager_get_selection (self));

  if (self->priv->presenting)
    {
       return FALSE;
    }
  if (!selection || GLIDE_IS_SLIDE(selection))
    return FALSE;

  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
					 GLIDE_ACTOR (selection),
					 "Move actor");
  clutter_actor_set_x (selection, (clutter_actor_get_x(selection) + 10)-(gint)(clutter_actor_get_x(selection) + 10)%10);  
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (selection)),
				       GLIDE_ACTOR (selection));
  
  return TRUE;
}

static void
glide_stage_manager_class_init (GlideStageManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterBindingPool *binding_pool;
  
  object_class->finalize = glide_stage_manager_finalize;
  object_class->set_property = glide_stage_manager_set_property;
  object_class->get_property = glide_stage_manager_get_property;
  object_class->constructor = glide_stage_manager_constructor;
  
  g_object_class_install_property (object_class,
				   PROP_STAGE,
				   g_param_spec_object ("stage",
							"Stage",
							"The ClutterStage to manage",
							CLUTTER_TYPE_STAGE,
							G_PARAM_READWRITE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class,
				   PROP_SELECTION,
				   g_param_spec_object ("selection",
							"Selection",
							"The GlideActor currently selected",
							GLIDE_TYPE_ACTOR,
							G_PARAM_READWRITE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class,
				   PROP_DOCUMENT,
				   g_param_spec_object ("document",
							"Document",
							"The document rendered by the stage manager",
							GLIDE_TYPE_DOCUMENT,
							G_PARAM_READWRITE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_STRINGS));
  
  g_object_class_install_property (object_class,
				   PROP_CURRENT_SLIDE,
				   g_param_spec_uint ("current-slide",
						      "Current Slide",
						      "The currently displayed slide",
						      0, G_MAXUINT, G_MAXUINT, 
						      G_PARAM_READWRITE));

  g_object_class_install_property (object_class,
				   PROP_PRESENTING,
				   g_param_spec_boolean("presenting",
							"Presenting",
							"Whether we are currently involved in a presentation",
							FALSE,
							G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class,
				   PROP_UNDO_MANAGER,
				   g_param_spec_object ("undo-manager",
							"Undo Manager",
							"The GlideUndoManager for the state",
							GLIDE_TYPE_ACTOR,
							G_PARAM_READWRITE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_STRINGS));

  
  // Argument is old selection
  stage_manager_signals[SELECTION_CHANGED] = 
    g_signal_new ("selection-changed",
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST,
		  0,
		  NULL, NULL,
		  g_cclosure_marshal_VOID__OBJECT,
		  G_TYPE_NONE, 1,
		  G_TYPE_OBJECT);
  
  binding_pool = clutter_binding_pool_get_for_class (klass);
  
  clutter_binding_pool_install_action (binding_pool, "next",
				       CLUTTER_space, 0,
				       G_CALLBACK(glide_stage_manager_binding_next),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "end",
				       CLUTTER_Escape, 0,
				       G_CALLBACK(glide_stage_manager_binding_end_presentation),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "move-up",
				       CLUTTER_Up, 0,
				       G_CALLBACK(glide_stage_manager_binding_move_up),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "move-down",
				       CLUTTER_Down, 0,
				       G_CALLBACK(glide_stage_manager_binding_move_down),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "move-left",
				       CLUTTER_Left, 0,
				       G_CALLBACK(glide_stage_manager_binding_move_left),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "move-right",
				       CLUTTER_Right, 0,
				       G_CALLBACK(glide_stage_manager_binding_move_right),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "move-up-snap",
				       CLUTTER_Up, CLUTTER_SHIFT_MASK,
				       G_CALLBACK(glide_stage_manager_binding_move_up_snap),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "move-down-snap",
				       CLUTTER_Down, CLUTTER_SHIFT_MASK,
				       G_CALLBACK(glide_stage_manager_binding_move_down_snap),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "move-left-snap",
				       CLUTTER_Left, CLUTTER_SHIFT_MASK,
				       G_CALLBACK(glide_stage_manager_binding_move_left_snap),
				       NULL, NULL);
  clutter_binding_pool_install_action (binding_pool, "move-right-snap",
				       CLUTTER_Right, CLUTTER_SHIFT_MASK,
				       G_CALLBACK(glide_stage_manager_binding_move_right_snap),
				       NULL, NULL);

  g_type_class_add_private (object_class, sizeof(GlideStageManagerPrivate));
}

static void
glide_stage_manager_init (GlideStageManager *manager)
{
  manager->priv = GLIDE_STAGE_MANAGER_GET_PRIVATE (manager);
}

/**
 * glide_stage_manager_new:
 * @document: A #GlideDocument
 * @stage: A #ClutterStage
 *
 * Constructs a new stage manager, which manages @document on @stage.
 * including creating/adding the actors.
 *
 * Return value: The new #GlideStageManager
 */
GlideStageManager *
glide_stage_manager_new (GlideDocument *document, ClutterStage *stage)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  g_return_val_if_fail (CLUTTER_IS_STAGE (stage), NULL);
  return g_object_new (GLIDE_TYPE_STAGE_MANAGER,
		       "document", document,
		       "stage", stage,
		       NULL);
}

/**
 * glide_stage_manager_get_stage:
 * @manager: A #GlideStageManager
 *
 * Returns the #ClutterStage managed by @manager.
 *
 * Return value: The #ClutterStage for @manager.
 */
ClutterStage *
glide_stage_manager_get_stage (GlideStageManager *manager)
{
  g_return_val_if_fail (GLIDE_IS_STAGE_MANAGER (manager), NULL);
  return (ClutterStage *)manager->priv->stage;
}

/**
 * glide_stage_manager_get_selection:
 * @manager: A #GlideStageManager
 *
 * Returns the currently selected actor for @manager.
 *
 * Return value: The currently selected actor for @manager.
 */
GlideActor *
glide_stage_manager_get_selection (GlideStageManager *manager)
{
  g_return_val_if_fail (GLIDE_IS_STAGE_MANAGER (manager), NULL);
  return manager->priv->selection;
}

/**
 * glide_stage_manager_set_selection:
 * @manager: A #GlideStageManager
 * @actor: A #GlideActor to set as the new selection, or %NULL.
 *
 * Sets @actor as the current selection of @manager.
 */
void
glide_stage_manager_set_selection (GlideStageManager *manager,
				   GlideActor *actor)
{
  g_return_if_fail (GLIDE_IS_STAGE_MANAGER (manager));
  glide_stage_manager_set_selection_real (manager, actor);
}

/**
 * glide_stage_manager_get_manipulator:
 * @manager: A #GlideStageManager
 *
 * Returns the #GlideManipulator for @manager, used to manipulate
 * the selected actor.
 *
 * Return value: The #GlideManipulator for @manager
 */
GlideManipulator *
glide_stage_manager_get_manipulator (GlideStageManager *manager)
{
  g_return_val_if_fail (GLIDE_IS_STAGE_MANAGER (manager), NULL);
  return manager->priv->manip;
}

static void
glide_stage_manager_get_center_pos (ClutterActor *actor, 
				    gfloat *x,
				    gfloat *y)
{
  ClutterActor *parent = clutter_actor_get_stage (actor);
  gfloat p_width, p_height, width, height;
  
  clutter_actor_get_size (parent, &p_width, &p_height);
  clutter_actor_get_size (actor, &width, &height);
  
  *x = floor(p_width/2.0 - width/2.0);
  *y = floor(p_height/2.0 - height/2.0);
}

/**
 * glide_stage_manager_add_actor:
 * @manager: A #GlideStageManager
 * @actor: A new #GlideActor to add
 *
 * Adds @actor to the current slide for @manager. 
 * Primarily used for newly created actors.
 */
void 
glide_stage_manager_add_actor (GlideStageManager *manager,
			       GlideActor *actor)
{
  GlideSlide *current_slide;
  gfloat x, y;
  
  g_return_if_fail (GLIDE_IS_STAGE_MANAGER (manager));
  g_return_if_fail (GLIDE_IS_ACTOR (actor));

  glide_actor_set_stage_manager (actor, manager);
  
  current_slide = glide_document_get_nth_slide (manager->priv->document,
						manager->priv->current_slide);

  glide_slide_add_actor_content (current_slide, CLUTTER_ACTOR (actor));
  
  glide_stage_manager_get_center_pos (CLUTTER_ACTOR (actor), &x, &y);
  clutter_actor_set_position (CLUTTER_ACTOR (actor), x, y);
  
  clutter_actor_show (CLUTTER_ACTOR (actor));
  
  glide_stage_manager_effect_fade_in (CLUTTER_ACTOR (actor));
  
  glide_stage_manager_set_selection (manager, actor);
}

void
glide_stage_manager_set_slide_next (GlideStageManager *manager)
{
  if (manager->priv->current_slide + 1 < glide_document_get_n_slides(manager->priv->document))
    glide_stage_manager_set_slide (manager, manager->priv->current_slide + 1);
}

void
glide_stage_manager_set_slide_prev (GlideStageManager *manager)
{
  if (manager->priv->current_slide > 0)
    glide_stage_manager_set_slide (manager, manager->priv->current_slide - 1);
}

gint
glide_stage_manager_get_current_slide (GlideStageManager *manager)
{
  return manager->priv->current_slide;
}

void
glide_stage_manager_set_current_slide (GlideStageManager *manager, guint slide)
{
  glide_stage_manager_set_slide (manager, slide);
}

// TODO: Error handling.
void
glide_stage_manager_load_slides (GlideStageManager *manager, JsonArray *slides)
{
  GList *slides_list, *s;
  
  // Handle broken first slide.
  
  slides_list = json_array_get_elements (slides);
  for (s = slides_list; s; s = s->next)
    {
      JsonNode *n = s->data;
      JsonObject *slide = json_node_get_object (n);
      GlideSlide *gs = glide_document_append_slide (manager->priv->document);
      
      glide_actor_set_stage_manager (GLIDE_ACTOR (gs), manager);
      
      glide_slide_construct_from_json (gs, slide, manager);
    }
}

void
glide_stage_manager_set_presenting (GlideStageManager *manager, gboolean presenting)
{
  if (presenting != manager->priv->presenting)
    {
      manager->priv->presenting = presenting;
      if (presenting)
      	glide_stage_manager_set_selection (manager, NULL);
      else
	glide_stage_manager_add_manipulator (manager);
      g_object_notify (G_OBJECT (manager), "presenting");
    }
}

gboolean
glide_stage_manager_get_presenting (GlideStageManager *manager)
{
  return manager->priv->presenting;
}

void
glide_stage_manager_set_slide_background (GlideStageManager *manager, const gchar *bg)
{
  GlideSlide *s = glide_document_get_nth_slide (manager->priv->document, manager->priv->current_slide);

  glide_undo_manager_start_slide_action (manager->priv->undo_manager, s, "Set slide background.");
  glide_slide_set_background (s, bg);
  glide_undo_manager_end_slide_action (manager->priv->undo_manager, s);
}

void
glide_stage_manager_delete_selection (GlideStageManager *manager)
{
  GlideActor *selection = glide_stage_manager_get_selection (manager);
  
  if (!selection || GLIDE_IS_SLIDE(selection))
    return;
  
  glide_undo_manager_append_delete (manager->priv->undo_manager, selection);
  
  glide_stage_manager_set_selection (manager, NULL);
  clutter_container_remove_actor (CLUTTER_CONTAINER (clutter_actor_get_parent (CLUTTER_ACTOR (selection))), CLUTTER_ACTOR (selection));
}

void
glide_stage_manager_set_undo_manager (GlideStageManager *manager, GlideUndoManager *undo_manager)
{
  manager->priv->undo_manager = undo_manager;

  g_object_notify (G_OBJECT (manager), "undo-manager");
}

GlideUndoManager *
glide_stage_manager_get_undo_manager (GlideStageManager *manager)
{
  return manager->priv->undo_manager;
}

GlideDocument *
glide_stage_manager_get_document (GlideStageManager *manager)
{
  return manager->priv->document;
}
