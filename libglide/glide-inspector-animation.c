/*
 * glide-inspector-animation.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * SECTION:glide-inspector-animation
 * @short_description: An inspector page for animation settings.
 *
 */

#include "glide-inspector-animation.h"
#include "glide-inspector-animation-priv.h"

#include "glide-slide.h"

#include <string.h>

#include "glide-animation-manager.h"

#define GLIDE_INSPECTOR_ANIMATION_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_INSPECTOR_ANIMATION, GlideInspectorAnimationPrivate))

G_DEFINE_TYPE(GlideInspectorAnimation, glide_inspector_animation, GTK_TYPE_ALIGNMENT);

enum {
  PROP_0,
  PROP_ACTOR
};

static void
glide_inspector_animation_update_duration (GlideInspectorAnimation *ins)
{
  GlideAnimationInfo *info = (GlideAnimationInfo *)glide_slide_get_animation (GLIDE_SLIDE (ins->priv->actor));
  
  if (info)
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (ins->priv->duration_spin), info->duration/1000);
}

static void
glide_inspector_animation_set_animation_direction (GlideInspectorAnimation *ins)
{
  const GlideAnimationInfo *info = glide_slide_get_animation (GLIDE_SLIDE (ins->priv->actor));
  GtkTreeModel *m = gtk_combo_box_get_model (GTK_COMBO_BOX (ins->priv->direction_box));
  GtkTreeIter iter;
  const gchar *option;
  
  if (!info)
    return;
  else if (!info->option && info->animation->list_options)
    {
      gtk_tree_model_get_iter_first (m, &iter);
      gtk_combo_box_set_active_iter (GTK_COMBO_BOX (ins->priv->direction_box), &iter);
      
      return;
    }
  else
    option = info->option;
  
  if (!info->animation->list_options)
    return;
  
  gtk_tree_model_get_iter_first (m, &iter);
  do {
    gchar *e;
    
    gtk_tree_model_get (m, &iter, 0, &e, -1);
    if (!strcmp (e, option))
      {
	gtk_combo_box_set_active_iter (GTK_COMBO_BOX (ins->priv->direction_box), &iter);
	g_free (e);
	return;
      }
    g_free (e);
  } while (gtk_tree_model_iter_next (m, &iter));

  gtk_tree_model_get_iter_first (m, &iter);
  gtk_combo_box_set_active_iter (GTK_COMBO_BOX (ins->priv->direction_box), &iter);
}

static void
glide_inspector_animation_update_direction_box (GlideInspectorAnimation *insp,
						const GlideAnimation *anim)
{
  GtkListStore *store;
  GtkTreeIter iter;
  GList *options, *o;
  
  store = gtk_list_store_new (1, G_TYPE_STRING);
  gtk_combo_box_set_model (GTK_COMBO_BOX (insp->priv->direction_box), GTK_TREE_MODEL (store));
  g_object_unref (store);
  
  if (anim && anim->list_options)
    options = anim->list_options ();
  else
      return;
  
  for (o = options; o; o = o->next)
    {
      gchar *option = (gchar *)o->data;
      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter, 0, option,
			  -1);
    }
  
  glide_inspector_animation_set_animation_direction (insp);
}

static void
glide_inspector_animation_duration_spin_changed (GtkWidget *spin,
						 gpointer user_data)
{
  GlideInspectorAnimation *ins = (GlideInspectorAnimation *)user_data;
  GlideAnimationInfo *info;
  
  info = glide_slide_get_animation (GLIDE_SLIDE (ins->priv->actor));
  info->duration = 1000 * gtk_spin_button_get_value (GTK_SPIN_BUTTON (ins->priv->duration_spin));
}

static void
glide_inspector_animation_direction_box_changed (GtkWidget *cbox,
						 gpointer user_data)
{
  GlideInspectorAnimation *ins = (GlideInspectorAnimation *)user_data;
  GlideAnimationInfo *info;
  gchar *option = NULL;
  GtkTreeIter iter;
  
  
  info = glide_slide_get_animation (GLIDE_SLIDE (ins->priv->actor));
  if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (cbox), &iter))
    {
      GtkTreeModel *model;
      
      model = gtk_combo_box_get_model (GTK_COMBO_BOX (cbox));
      gtk_tree_model_get(model, &iter, 0, &option, -1);
    }
  info->option = option;
}

static void
glide_inspector_animation_effect_box_changed (GtkWidget *cbox,
					      gpointer user_data)
{
  GlideInspectorAnimation *ins = (GlideInspectorAnimation *)user_data;
  GtkTreeIter iter;
  gchar *animation;
  GlideAnimationInfo info, *old_info;
  const GlideAnimation *ga;
  
  if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (cbox), &iter))
    {
      GtkTreeModel *model;
      
      model = gtk_combo_box_get_model (GTK_COMBO_BOX (cbox));
      gtk_tree_model_get (model, &iter, 0, &animation, -1);
    }
  else
    {
      return;
    }
  
  if (!strcmp(animation, "None"))
    {
      gtk_widget_set_sensitive (ins->priv->duration_spin, FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (ins->priv->duration_spin, TRUE);
      gtk_spin_button_set_value (GTK_SPIN_BUTTON (ins->priv->duration_spin), 1.0);
    }
  
  old_info = glide_slide_get_animation (GLIDE_SLIDE (ins->priv->actor));
  ga = glide_animation_manager_get_animation (animation);

  info.duration = 1000*gtk_spin_button_get_value (GTK_SPIN_BUTTON (ins->priv->duration_spin));
  info.animation = ga;
  if (old_info && old_info->animation && 
      !strcmp (old_info->animation->name, animation))
    {
      info.option = g_strdup (old_info->option);
      info.duration = old_info->duration;
    }
  else
    info.option = NULL;
  
  glide_slide_set_animation (GLIDE_SLIDE (ins->priv->actor), &info);
  glide_inspector_animation_update_direction_box (ins, ga);
  g_free (animation);
}

static void
glide_inspector_effect_box_set_animation (GlideInspectorAnimation *ins)
{
  const GlideAnimationInfo *info = glide_slide_get_animation (GLIDE_SLIDE (ins->priv->actor));
  GtkTreeModel *m = gtk_combo_box_get_model (GTK_COMBO_BOX (ins->priv->effect_box));
  GtkTreeIter iter;
  const gchar *animation;
  
  if (!info || !info->animation)
    {
      animation = "None";
    }
  else
    {
      animation = info->animation->name;
    }
  
  gtk_tree_model_get_iter_first (m, &iter);
  do {
    gchar *e;
    
    gtk_tree_model_get (m, &iter, 0, &e, -1);
    if (!strcmp (e, animation))
      {
	gtk_combo_box_set_active_iter (GTK_COMBO_BOX (ins->priv->effect_box), &iter);

	g_free (e);
	return;
      }
    g_free (e);
  } while (gtk_tree_model_iter_next (m, &iter));
  // TODO: What to do when say loading a file that has an animation our version doesn't have?
}


static void
glide_inspector_animation_setup_comboboxes (GlideInspectorAnimation *ins)
{
  GtkListStore *store = gtk_list_store_new (1, G_TYPE_STRING);
  GtkCellRenderer *renderer;
  GtkTreeIter iter;
  const GList *animations, *a;

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, 0, "None", -1);
  
  animations = glide_animation_manager_get_animations ();
  for (a = animations; a; a = a->next)
    {
      GlideAnimation *animation = (GlideAnimation *)a->data;
      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter, 0, animation->name,
			  -1);
    }
  
  gtk_combo_box_set_model (GTK_COMBO_BOX (ins->priv->effect_box), GTK_TREE_MODEL (store));
  g_object_unref (store);
  
  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (ins->priv->effect_box), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT(ins->priv->effect_box), renderer, "text", 0, NULL);

  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (ins->priv->direction_box), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT(ins->priv->direction_box), renderer, "text", 0, NULL);
  
  g_signal_connect (ins->priv->effect_box, "changed", G_CALLBACK (glide_inspector_animation_effect_box_changed), ins);
  g_signal_connect (ins->priv->direction_box, "changed", G_CALLBACK (glide_inspector_animation_direction_box_changed), ins);
}

static void
glide_inspector_animation_finalize (GObject *object)
{
  
}

static void
glide_inspector_animation_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorAnimation *inspector = GLIDE_INSPECTOR_ANIMATION (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      g_value_set_object (value, inspector->priv->actor);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_animation_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorAnimation *inspector = GLIDE_INSPECTOR_ANIMATION (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      glide_inspector_animation_set_actor (inspector, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static GtkWidget *
glide_inspector_animation_make_duration (GlideInspectorAnimation *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 10);
  GtkWidget *duration_label = gtk_label_new (NULL);
  GtkWidget *al = gtk_alignment_new (1, 0, 0.4, 1);
  ins->priv->duration_spin = gtk_spin_button_new_with_range (0, 10, 0.1);
  
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (ins->priv->duration_spin), 1.0);
  
  gtk_label_set_markup (GTK_LABEL (duration_label), "<b>Duration:</b>");
  
  gtk_container_add (GTK_CONTAINER (al), ins->priv->duration_spin);

  gtk_box_pack_start (GTK_BOX (ret), duration_label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (ret), al, TRUE, TRUE, 0);
  
  g_signal_connect (ins->priv->duration_spin, "value-changed",
		    G_CALLBACK (glide_inspector_animation_duration_spin_changed), 
		    ins);
  
  return ret;
}

static void
glide_inspector_animation_setup_ui (GlideInspectorAnimation *ins)
{
  GtkWidget *effect_label, *direction_label;
  GtkWidget *box = gtk_vbox_new (FALSE, 2);
  GtkWidget *duration_box;
 
  ins->priv->main_vbox = box;
  ins->priv->effect_box = gtk_combo_box_new ();
  ins->priv->direction_box = gtk_combo_box_new ();
  
  effect_label = gtk_label_new (NULL);
  direction_label = gtk_label_new (NULL);

  gtk_label_set_markup (GTK_LABEL (effect_label), "<b>Effect:</b>");
  gtk_misc_set_alignment (GTK_MISC (effect_label), 0, 0);
  gtk_label_set_markup (GTK_LABEL (direction_label), "<b>Direction:</b>");
  gtk_misc_set_alignment (GTK_MISC (direction_label), 0, 0);
  
  duration_box = glide_inspector_animation_make_duration (ins);

  gtk_box_pack_start (GTK_BOX (box), effect_label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box), ins->priv->effect_box, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box), direction_label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box), ins->priv->direction_box, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box), duration_box, FALSE, FALSE, 0);

  gtk_container_add (GTK_CONTAINER (ins), box);
  
  glide_inspector_animation_setup_comboboxes (ins);
}

static void
glide_inspector_animation_init (GlideInspectorAnimation *inspector)
{
  inspector->priv = GLIDE_INSPECTOR_ANIMATION_GET_PRIVATE (inspector);
  
  glide_inspector_animation_setup_ui (inspector);
  gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);

  gtk_alignment_set (GTK_ALIGNMENT (inspector), .5, 0, 0.8, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (inspector), 5, 0, 0, 0);
}

static void
glide_inspector_animation_class_init (GlideInspectorAnimationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_inspector_animation_finalize;
  object_class->get_property = glide_inspector_animation_get_property;
  object_class->set_property = glide_inspector_animation_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_ACTOR,
				   g_param_spec_object ("actor",
							"Actor",
							"The actor we are inspecting",
							GLIDE_TYPE_ACTOR,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideInspectorAnimationPrivate));
}

GtkWidget *
glide_inspector_animation_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_INSPECTOR_ANIMATION, NULL);
}


GlideActor *
glide_inspector_animation_get_actor (GlideInspectorAnimation *inspector)
{
  return inspector->priv->actor;
}

void
glide_inspector_animation_set_actor (GlideInspectorAnimation *inspector,
				   GlideActor *actor)
{
  inspector->priv->actor = actor;
  if (!actor)
    return;
  
  if (!GLIDE_IS_SLIDE (actor))
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), TRUE);
      glide_inspector_animation_update_duration (inspector);
      glide_inspector_effect_box_set_animation (inspector);
    }
  
  g_object_notify (G_OBJECT (inspector), "actor");
}
