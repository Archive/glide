/*
 * glide-window-private.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 
#ifndef __GLIDE_WINDOW_PRIVATE_H__
#define __GLIDE_WINDOW_PRIVATE_H__

#include "glide-window.h"
#include "glide-stage-manager.h"
#include "glide-document.h"

G_BEGIN_DECLS

struct _GlideWindowPrivate
{
  GtkBuilder *builder;
  
  GtkWidget *embed;
  ClutterActor *stage;
  
  GlideStageManager *manager;
  GlideDocument *document;
  
  gint lfw, lfh;
  gint old_document_width, old_document_height;
  
  JsonNode *copy_buffer;
  gboolean keep_buffer;
  
  GtkRecentManager *recent_manager;
  GlideUndoManager *undo_manager;
  
  GtkWidget *slide_box;
  GtkWidget *inspector_notebook;
  GtkWidget *inspector_window;

  gboolean inspector_was_showing;
};

G_END_DECLS

#endif  /* __GLIDE_WINDOW_PRIVATE_H__  */
