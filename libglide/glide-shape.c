/*
 * glide-shape.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * SECTION:glide-shape
 * @short_description: An actor for displaying and editing 2D Vector Shapes.
 *
 */

#include "glide-shape.h"
#include "glide-shape-priv.h"

#include "glide-json-util.h"

#define GLIDE_SHAPE_GET_PRIVATE(obj)   (G_TYPE_INSTANCE_GET_PRIVATE ((obj), GLIDE_TYPE_SHAPE, GlideShapePrivate))

G_DEFINE_TYPE (GlideShape, glide_shape, GLIDE_TYPE_ACTOR);

enum
  {
    PROP_0,
    
    PROP_COLOR,
    PROP_BORDER_COLOR,
    PROP_BORDER_WIDTH,
    PROP_HAS_BORDER
  };

static const ClutterColor default_color = {255,255,255,255};
static const ClutterColor default_border_color = {0,0,0,255};

static void 
glide_shape_paint_rectangle (ClutterActor *self)
{
  GlideShapePrivate *priv = GLIDE_SHAPE(self)->priv;
  ClutterGeometry geom;
  guint8 tmp_alpha;

  clutter_actor_get_allocation_geometry (self, &geom);

  if (priv->has_border)
    {
      tmp_alpha = clutter_actor_get_paint_opacity (self)
                * priv->border_color.alpha
                / 255;

      cogl_set_source_color4ub (priv->border_color.red,
                                priv->border_color.green,
                                priv->border_color.blue,
                                tmp_alpha);

      cogl_rectangle (priv->border_width, 0,
                      geom.width,
                      priv->border_width);

      cogl_rectangle (geom.width - priv->border_width,
                      priv->border_width,
                      geom.width,
                      geom.height);

      cogl_rectangle (0, geom.height - priv->border_width,
                      geom.width - priv->border_width,
                      geom.height);

      cogl_rectangle (0, 0,
                      priv->border_width,
                      geom.height - priv->border_width);

      tmp_alpha = clutter_actor_get_paint_opacity (self)
                * priv->color.alpha
                / 255;

      cogl_set_source_color4ub (priv->color.red,
                                priv->color.green,
                                priv->color.blue,
                                tmp_alpha);

      cogl_rectangle (priv->border_width, priv->border_width,
                      geom.width - priv->border_width,
                      geom.height - priv->border_width);
    }
  else
    {
      tmp_alpha = clutter_actor_get_paint_opacity (self)
                * priv->color.alpha
                / 255;

      cogl_set_source_color4ub (priv->color.red,
                                priv->color.green,
                                priv->color.blue,
                                tmp_alpha);

      cogl_rectangle (0, 0, geom.width, geom.height);
    }
}

static void
glide_shape_paint (ClutterActor *self)
{
  glide_shape_paint_rectangle (self);
}

static void
glide_shape_set_property (GObject      *object,
				guint         prop_id,
				const GValue *value,
				GParamSpec   *pspec)
{
  GlideShape *shape = (GlideShape *)object;

  switch (prop_id)
    {
    case PROP_COLOR:
      glide_shape_set_color (shape, clutter_value_get_color (value));
      break;
    case PROP_BORDER_COLOR:
      glide_shape_set_border_color (shape,
                                          clutter_value_get_color (value));
      break;
    case PROP_BORDER_WIDTH:
      glide_shape_set_border_width (shape,
                                          g_value_get_uint (value));
      break;
    case PROP_HAS_BORDER:
      glide_shape_set_has_border (shape, g_value_get_boolean (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
glide_shape_get_property (GObject    *object,
				guint       prop_id,
				GValue     *value,
				GParamSpec *pspec)
{
  GlideShapePrivate *priv = GLIDE_SHAPE(object)->priv;

  switch (prop_id)
    {
    case PROP_COLOR:
      clutter_value_set_color (value, &priv->color);
      break;
    case PROP_BORDER_COLOR:
      clutter_value_set_color (value, &priv->border_color);
      break;
    case PROP_BORDER_WIDTH:
      g_value_set_uint (value, priv->border_width);
      break;
    case PROP_HAS_BORDER:
      g_value_set_boolean (value, priv->has_border);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static gboolean
glide_shape_button_press (ClutterActor *actor,
			      ClutterButtonEvent *event)
{
  GlideStageManager *m;
  GlideActor *ga = GLIDE_ACTOR (actor);
  GlideShape *shape = GLIDE_SHAPE (actor);
  gfloat ax, ay;
  
  m = glide_actor_get_stage_manager (ga);
  
  glide_stage_manager_set_selection (m, ga);
  
  clutter_actor_get_position (actor, &ax, &ay);
  shape->priv->drag_center_x = event->x - ax;
  shape->priv->drag_center_y = event->y - ay;
  shape->priv->dragging = TRUE;
  shape->priv->motion_since_press = FALSE;
  
  clutter_grab_pointer (actor);
  
  glide_actor_start_undo (ga, "Move shape");


  return TRUE;
}

static gboolean
glide_shape_button_release (ClutterActor *actor,
			    ClutterButtonEvent *bev)
{
  GlideShape *shape = GLIDE_SHAPE (actor);

  if (shape->priv->dragging)
    {
      if (!shape->priv->motion_since_press)
	glide_undo_manager_cancel_actor_action (glide_actor_get_undo_manager (GLIDE_ACTOR (actor)));
      else
	glide_actor_end_undo (GLIDE_ACTOR (actor));
      
      clutter_ungrab_pointer ();
      shape->priv->dragging = FALSE;
      
      return TRUE;
    }
  
  return FALSE;

}

static gboolean
glide_shape_motion (ClutterActor *actor,
		    ClutterMotionEvent *mev)
{
  GlideShape *shape = GLIDE_SHAPE (actor);
  
  if (shape->priv->dragging)
    {
      shape->priv->motion_since_press = TRUE;
      clutter_actor_set_position (actor,
				  mev->x - shape->priv->drag_center_x,
				  mev->y - shape->priv->drag_center_y);
      
      return TRUE;
    }
  return FALSE;
}

static void
glide_shape_finalize (GObject *object)
{
  G_OBJECT_CLASS (glide_shape_parent_class)->finalize (object);
}
static void
glide_json_object_add_shape_properties (JsonObject *obj, GlideShape *shape)
{
  JsonNode *n = json_node_new (JSON_NODE_OBJECT);
  JsonObject *shape_obj = json_object_new ();
  ClutterColor c;
  gchar *cs;
  
  json_node_set_object (n, shape_obj);
  
  glide_shape_get_color (shape, &c);
  cs = clutter_color_to_string (&c);
  glide_json_object_set_string (shape_obj, "color", cs);
  g_free (cs);
  
  glide_shape_get_border_color (shape, &c);
  cs = clutter_color_to_string (&c);
  glide_json_object_set_string (shape_obj, "border-color", cs);
  g_free (cs);
  
  glide_json_object_set_double (shape_obj, "border-width",
				glide_shape_get_border_width (shape));
  glide_json_object_set_boolean (shape_obj, "has-border",
				 glide_shape_get_has_border (shape));
  
  json_object_set_member (obj, "shape-properties", n);
}

static JsonNode *
glide_shape_serialize (GlideActor *self)
{
  JsonNode *node = json_node_new (JSON_NODE_OBJECT);
  JsonObject *obj = json_object_new ();
  
  json_node_set_object (node, obj);
  
  glide_json_object_set_string (obj, "type", "shape");
  glide_json_object_add_actor_geometry (obj, CLUTTER_ACTOR (self));
  glide_json_object_add_shape_properties (obj, GLIDE_SHAPE (self));
    
  return node;
}

void
glide_shape_deserialize (GlideActor *actor, JsonObject *actor_obj)
{
  GlideShape *shape = GLIDE_SHAPE (actor);
  JsonObject *shape_props;
  ClutterColor c;
  
  {
    JsonNode *n = json_object_get_member (actor_obj, "shape-properties");
    shape_props = json_node_get_object (n);
  }
  clutter_color_from_string (&c, glide_json_object_get_string (shape_props, "color"));
  glide_shape_set_color (shape, &c);
  clutter_color_from_string (&c, glide_json_object_get_string (shape_props, "border-color"));
  glide_shape_set_border_color (shape, &c);
  
  glide_shape_set_border_width (shape, glide_json_object_get_double (shape_props, "border-width"));
  glide_shape_set_has_border (shape, glide_json_object_get_boolean (shape_props, "has-border"));
}
			    

static void
glide_shape_class_init (GlideShapeClass *klass)
{
  GlideActorClass *glide_actor_class = GLIDE_ACTOR_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;
  
  g_type_class_add_private (klass, sizeof (GlideShapePrivate));
  
  gobject_class->finalize = glide_shape_finalize;
  gobject_class->set_property = glide_shape_set_property;
  gobject_class->get_property = glide_shape_get_property;
  
  glide_actor_class->serialize = glide_shape_serialize;
  glide_actor_class->deserialize = glide_shape_deserialize;
  
  actor_class->paint = glide_shape_paint;
  actor_class->button_press_event = glide_shape_button_press;
  actor_class->button_release_event = glide_shape_button_release;
  actor_class->motion_event = glide_shape_motion;

  pspec = clutter_param_spec_color ("color",
                                    "Color",
                                    "The color of the rectangle",
                                    &default_color,
                                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT);
  g_object_class_install_property (gobject_class, PROP_COLOR, pspec);

  pspec = clutter_param_spec_color ("border-color",
                                    "Border Color",
                                    "The color of the border of the rectangle",
                                    &default_border_color,
                                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT);
  g_object_class_install_property (gobject_class, PROP_BORDER_COLOR, pspec);

  g_object_class_install_property (gobject_class,
                                   PROP_BORDER_WIDTH,
                                   g_param_spec_uint ("border-width",
                                                      "Border Width",
                                                      "The width of the border of the rectangle",
                                                      0, G_MAXUINT,
                                                      5,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT));
  g_object_class_install_property (gobject_class,
                                   PROP_HAS_BORDER,
                                   g_param_spec_boolean ("has-border",
                                                         "Has Border",
                                                         "Whether the rectangle should have a border",
                                                         TRUE,
                                                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT));
}

static void
glide_shape_init (GlideShape *self)
{
  self->priv = GLIDE_SHAPE_GET_PRIVATE (self);
  
  clutter_actor_set_size (CLUTTER_ACTOR (self), 100, 100);
}

ClutterActor*
glide_shape_new ()
{
  return g_object_new (GLIDE_TYPE_SHAPE, NULL);
}

void
glide_shape_set_color (GlideShape *shape,
		       const ClutterColor *color)
{
  shape->priv->color = *color;
  clutter_actor_queue_redraw (CLUTTER_ACTOR (shape));
  
  g_object_notify (G_OBJECT (shape), "color");
}

void
glide_shape_set_border_color (GlideShape *shape,
		       const ClutterColor *border_color)
{
  shape->priv->border_color = *border_color;
  clutter_actor_queue_redraw (CLUTTER_ACTOR (shape));
  
  g_object_notify (G_OBJECT (shape), "border-color");
}

void
glide_shape_get_color (GlideShape *shape, ClutterColor *color)
{
  *color = shape->priv->color;
}

void
glide_shape_get_border_color (GlideShape *shape, ClutterColor *border_color)
{
  *border_color = shape->priv->border_color;
}

guint
glide_shape_get_border_width (GlideShape *shape)
{
  return shape->priv->border_width;
}

void
glide_shape_set_border_width (GlideShape *shape, guint border_width)
{
  shape->priv->border_width = border_width;
  clutter_actor_queue_redraw (CLUTTER_ACTOR (shape));
  
  g_object_notify (G_OBJECT (shape), "border-width");
}

void
glide_shape_set_has_border (GlideShape *shape, gboolean has_border)
{
  shape->priv->has_border = has_border;
  clutter_actor_queue_redraw (CLUTTER_ACTOR (shape));
  
  g_object_notify (G_OBJECT (shape), "has-border");
}

gboolean
glide_shape_get_has_border (GlideShape *shape)
{
  return shape->priv->has_border;
}
