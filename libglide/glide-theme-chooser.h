/*
 * glide-theme-chooser.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_THEME_CHOOSER_H__
#define __GLIDE_THEME_CHOOSER_H__

#include <gtk/gtk.h>
#include "glide-theme.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_THEME_CHOOSER              (glide_theme_chooser_get_type())
#define GLIDE_THEME_CHOOSER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_THEME_CHOOSER, GlideThemeChooser))
#define GLIDE_THEME_CHOOSER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_THEME_CHOOSER, GlideThemeChooserClass))
#define GLIDE_IS_THEME_CHOOSER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_THEME_CHOOSER))
#define GLIDE_IS_THEME_CHOOSER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_THEME_CHOOSER))
#define GLIDE_THEME_CHOOSER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_THEME_CHOOSER, GlideThemeChooserClass))

typedef struct _GlideThemeChooserPrivate GlideThemeChooserPrivate;


typedef struct _GlideThemeChooser GlideThemeChooser;

struct _GlideThemeChooser
{
  GtkWindow window;
  
  GlideThemeChooserPrivate *priv;
};

typedef struct _GlideThemeChooserClass GlideThemeChooserClass;

struct _GlideThemeChooserClass
{
  GtkWindowClass parent_class;
};

GType glide_theme_chooser_get_type (void) G_GNUC_CONST;
GtkWidget *glide_theme_chooser_new (void);

GlideTheme *glide_theme_chooser_get_theme (GlideThemeChooser *chooser);

G_END_DECLS

#endif
