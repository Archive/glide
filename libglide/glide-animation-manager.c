/*
 * glide-animation-manager.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-animation-manager
 * @short_description: Manages and performs animations.
 *
 */

#include <gobject/gvaluecollector.h>
#include <glib.h>
#include <string.h>

#include "glide-animation-manager.h"

#include "glide-animations.h"
#include "glide-vala-animations.h"


static GList *animations = NULL;

void
glide_animation_manager_register_animation (GlideAnimationCallback callback, 
					    GlideAnimationListOptionsCallback list_options,
					    const gchar *name)
{
  GlideAnimation *animation = g_malloc (sizeof (GlideAnimation));
  
  animation->name = g_strdup (name);
  animation->animate = callback;
  animation->list_options = list_options;
  
  animations = g_list_append (animations, animation);
}

const GList *
glide_animation_manager_get_animations ()
{
  return animations;
}

const GlideAnimation *
glide_animation_manager_get_animation (const gchar *animation)
{
  GList *t;
  
  for (t = animations; t; t = t->next)
    {
      GlideAnimation *a = (GlideAnimation *)t->data;
      
      if (!strcmp (a->name, animation))
	return a;
    }
  return NULL;
}

ClutterTimeline *
glide_animation_manager_do_animation (const GlideAnimationInfo *info,
				      ClutterActor *a,
				      ClutterActor *b)
{
  return info->animation->animate (info, a, b);
}


GType
glide_animation_info_get_type (void)
{
  static GType _glide_animation_info_type = 0;
  
  if (G_UNLIKELY (_glide_animation_info_type == 0))
    {
      _glide_animation_info_type =
	g_boxed_type_register_static ("GlideAnimationInfo",
				      (GBoxedCopyFunc) glide_animation_info_copy,
				      (GBoxedFreeFunc) glide_animation_info_free);
    }
  return _glide_animation_info_type;
}

GlideAnimationInfo *
glide_animation_info_copy (const GlideAnimationInfo *info)
{
  if (G_LIKELY (info != NULL))
    return g_slice_dup (GlideAnimationInfo, info);
  
  return NULL;
}

void
glide_animation_info_free (GlideAnimationInfo *info)
{
  if (G_LIKELY (info != NULL))
    {
      g_free (info->option);
      g_slice_free (GlideAnimationInfo, info);
    }
}

GlideAnimationInfo *
glide_animation_info_new ()
{
  return g_slice_new (GlideAnimationInfo);
}

void
glide_value_set_animation_info (GValue *value,
				const GlideAnimationInfo *info)
{
  g_return_if_fail (GLIDE_VALUE_HOLDS_ANIMATION_INFO (value));
  
  value->data[0].v_pointer = glide_animation_info_copy (info);
}

G_CONST_RETURN GlideAnimationInfo *
glide_value_get_animation_info (const GValue *value)
{
  g_return_val_if_fail (GLIDE_VALUE_HOLDS_ANIMATION_INFO (value), NULL);
  
  return value->data[0].v_pointer;
}

static void
glide_value_init_animation_info (GValue *value)
{
  value->data[0].v_pointer = NULL;
}

static void
glide_value_free_animation_info (GValue *value)
{
  if (!(value->data[1].v_uint & G_VALUE_NOCOPY_CONTENTS))
    glide_animation_info_free (value->data[0].v_pointer);
}

static void
glide_value_copy_animation_info (const GValue *src,
				 GValue *dest)
{
  dest->data[0].v_pointer = glide_animation_info_copy (src->data[0].v_pointer);
}

static gpointer
glide_value_peek_animation_info (const GValue *value)
{
  return value->data[0].v_pointer;
}

static gchar *
glide_value_collect_animation_info (GValue      *value,
                             guint        n_collect_values,
                             GTypeCValue *collect_values,
                             guint        collect_flags)
{
  if (!collect_values[0].v_pointer)
      value->data[0].v_pointer = NULL;
  else
    {
      if (collect_flags & G_VALUE_NOCOPY_CONTENTS)
        {
          value->data[0].v_pointer = collect_values[0].v_pointer;
          value->data[1].v_uint = G_VALUE_NOCOPY_CONTENTS;
        }
      else
        {
          value->data[0].v_pointer =
            glide_animation_info_copy (collect_values[0].v_pointer);
        }
    }

  return NULL;
}

static gchar *
glide_value_lcopy_animation_info (const GValue *value,
                           guint         n_collect_values,
                           GTypeCValue  *collect_values,
                           guint         collect_flags)
{
  GlideAnimationInfo **animation_info_p = collect_values[0].v_pointer;

  if (!animation_info_p)
    return g_strdup_printf ("value location for '%s' passed as NULL",
                            G_VALUE_TYPE_NAME (value));

  if (!value->data[0].v_pointer)
    *animation_info_p = NULL;
  else
    {
      if (collect_flags & G_VALUE_NOCOPY_CONTENTS)
        *animation_info_p = value->data[0].v_pointer;
      else
        *animation_info_p = glide_animation_info_copy (value->data[0].v_pointer);
    }

  return NULL;
}

static void
param_animation_info_init (GParamSpec *pspec)
{
  GlideParamSpecAnimationInfo *aspec = GLIDE_PARAM_SPEC_ANIMATION_INFO (pspec);
  
  aspec->default_value = NULL;
}

static void
param_animation_info_finalize (GParamSpec *pspec)
{
  GlideParamSpecAnimationInfo *aspec = GLIDE_PARAM_SPEC_ANIMATION_INFO (pspec);
  
  glide_animation_info_free (aspec->default_value);
}

static void
param_animation_info_set_default (GParamSpec *pspec,
				  GValue *value)
{
  value->data[0].v_pointer = GLIDE_PARAM_SPEC_ANIMATION_INFO (pspec)->default_value;
  value->data[1].v_uint = G_VALUE_NOCOPY_CONTENTS;
}

static gint
param_animation_info_values_cmp (GParamSpec *pspec,
				 const GValue *value1,
				 const GValue *value2)
{
  GlideAnimationInfo *a, *b;
  
  a = (GlideAnimationInfo *)value1->data[0].v_pointer;
  b = (GlideAnimationInfo *)value1->data[1].v_pointer;
  
  return strcmp (a->animation->name, b->animation->name);
}

static const GTypeValueTable _glide_animation_info_value_table = {
  glide_value_init_animation_info,
  glide_value_free_animation_info,
  glide_value_copy_animation_info,
  glide_value_peek_animation_info,
  "p",
  glide_value_collect_animation_info,
  "p",
  glide_value_lcopy_animation_info
};

GType
glide_param_animation_info_get_type (void)
{
  static GType pspec_type = 0;
  
  if (G_UNLIKELY (pspec_type == 0))
    {
      const GParamSpecTypeInfo pspec_info = {
	sizeof (GlideParamSpecAnimationInfo),
	16,
	param_animation_info_init,
	GLIDE_TYPE_ANIMATION_INFO,
	param_animation_info_finalize,
	param_animation_info_set_default,
	NULL,
	param_animation_info_values_cmp,
      };
      
      pspec_type = g_param_type_register_static ("GlideParamSpecAnimationInfo",
						 &pspec_info);
    }
  return pspec_type;
}

GParamSpec *
glide_param_spec_animation_info (const gchar *name,
				 const gchar *nick,
				 const gchar *blurb,
				 const GlideAnimationInfo *default_value,
				 GParamFlags flags)
{
  GlideParamSpecAnimationInfo *aspec;
  
  aspec = g_param_spec_internal (GLIDE_TYPE_PARAM_ANIMATION_INFO,
				  name, nick, blurb, flags);

  aspec->default_value = glide_animation_info_copy (default_value);
  
  return G_PARAM_SPEC (aspec);
}

void 
glide_animation_manager_register_animations ()
{
  glide_animation_manager_register_animation ((GlideAnimationCallback)glide_animate_fade, NULL, "Fade");
  glide_animation_manager_register_animation ((GlideAnimationCallback)glide_animate_fall, NULL, "Fall");
  glide_animation_manager_register_animation ((GlideAnimationCallback)glide_animate_drop, NULL, "Drop");
  glide_animation_manager_register_animation (glide_animations_animate_zoom_contents, NULL, "Zoom Contents");
  glide_animation_manager_register_animation ((GlideAnimationCallback)glide_animate_pivot, (GlideAnimationListOptionsCallback)glide_animate_pivot_list_options, "Pivot");
  glide_animation_manager_register_animation ((GlideAnimationCallback)glide_animate_slide, (GlideAnimationListOptionsCallback)glide_animate_slide_list_options, "Slide");
  glide_animation_manager_register_animation ((GlideAnimationCallback)glide_animate_zoom, (GlideAnimationListOptionsCallback)glide_animate_zoom_list_options, "Zoom");
  glide_animation_manager_register_animation ((GlideAnimationCallback)glide_animate_panel, (GlideAnimationListOptionsCallback)glide_animate_panel_list_options, "Panel");
  glide_animation_manager_register_animation (glide_animations_animate_doorway, NULL, "Doorway");
}

void
glide_animation_manager_free_options (GList *l)
{
  GList *t;
  for (t = l; t; t = t->next)
    {
      g_free (t->data);
    }
  g_list_free (l);
    
}
