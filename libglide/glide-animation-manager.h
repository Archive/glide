/*
 * glide-animation-manager.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __GLIDE_ANIMATION_MANAGER_H__
#define __GLIDE_ANIMATION_MANAGER_H__

#include <glib-object.h>
#include <clutter/clutter.h>
#include <glib.h>

G_BEGIN_DECLS

typedef struct _GlideAnimationInfo GlideAnimationInfo;
typedef struct _GlideAnimation GlideAnimation;

typedef ClutterTimeline *(*GlideAnimationCallback) (const GlideAnimationInfo *info,
						   ClutterActor *a,
						   ClutterActor *b);
typedef GList *(*GlideAnimationListOptionsCallback) (void);

struct _GlideAnimation {
  gchar *name;
  GlideAnimationCallback animate;
  GlideAnimationListOptionsCallback list_options;
};

struct _GlideAnimationInfo {
  const GlideAnimation *animation;
  guint duration;
  
  gchar *option;
};

void glide_animation_manager_register_animation (GlideAnimationCallback callback, GlideAnimationListOptionsCallback list_options, const gchar *name);
const GList *glide_animation_manager_get_animations ();
const GlideAnimation *glide_animation_manager_get_animation (const gchar *animation);

ClutterTimeline * glide_animation_manager_do_animation (const GlideAnimationInfo *info, ClutterActor *a, ClutterActor *b);

void glide_animation_manager_register_animations();

#define GLIDE_TYPE_ANIMATION_INFO (glide_animation_info_get_type())

GType glide_animation_info_get_type (void) G_GNUC_CONST;

GlideAnimationInfo *glide_animation_info_new ();

GlideAnimationInfo *glide_animation_info_copy (const GlideAnimationInfo *info);
void glide_animation_info_free (GlideAnimationInfo *info);


#define GLIDE_TYPE_PARAM_ANIMATION_INFO           (glide_param_animation_info_get_type ())
#define GLIDE_PARAM_SPEC_ANIMATION_INFO(pspec)    (G_TYPE_CHECK_INSTANCE_CAST ((pspec), GLIDE_TYPE_PARAM_ANIMATION_INFO, GlideParamSpecAnimationInfo))
#define GLIDE_IS_PARAM_SPEC_ANIMATION_INFO(pspec) (G_TYPE_CHECK_INSTANCE_TYPE ((pspec), GLIDE_TYPE_PARAM_ANIMATION_INFO))

#define GLIDE_VALUE_HOLDS_ANIMATION_INFO(x) (G_VALUE_HOLDS ((x), GLIDE_TYPE_ANIMATION_INFO))

typedef struct _GlideParamSpecAnimationInfo GlideParamSpecAnimationInfo;

struct _GlideParamSpecAnimationInfo
{
  GParamSpec parent_instance;
  
  GlideAnimationInfo *default_value;
};

void glide_value_set_animation_info (GValue *value, const GlideAnimationInfo *info);
G_CONST_RETURN GlideAnimationInfo *glide_value_get_animation_info (const GValue *value);

GType glide_param_animation_info_get_type (void) G_GNUC_CONST;
GParamSpec *glide_param_spec_animation_info (const gchar *name,
					     const gchar *nick,
					     const gchar *blurb,
					     const GlideAnimationInfo *default_value,
					     GParamFlags flags);

void glide_animation_manager_free_options (GList *l);


G_END_DECLS
#endif
