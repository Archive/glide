/*
 * glide-animation-manager.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-animation-manager
 * @short_description: Manages and performs animations.
 *
 */

#include "glide-theme-manager.h"
#include "glide-dirs.h"
#include "glide-debug.h"

static GList *themes = NULL;
static GHashTable *loaded_themes = NULL;

GList *
glide_theme_manager_get_themes ()
{
  return g_hash_table_get_values (loaded_themes);
}

static gboolean
glide_theme_manager_theme_hash_find (gpointer key,
				     gpointer value,
				     gpointer user_data)
{
  const gchar *theme_path = (const gchar *)key;
  GlideTheme *theme = (GlideTheme *)value;
  const gchar *name = (const gchar *)user_data;
  
  if (!strcmp (glide_theme_get_name (theme), name))
    return TRUE;
  return FALSE;
}

GlideTheme *
glide_theme_manager_get_theme (const gchar *theme_name)
{
  return g_hash_table_find (loaded_themes, (GHRFunc)glide_theme_manager_theme_hash_find, (gpointer)theme_name);
}

GlideTheme *
glide_theme_manager_load_theme (const gchar *theme_path)
{
  GlideTheme *ret;
  
  if (G_UNLIKELY (loaded_themes == NULL))
    loaded_themes = g_hash_table_new (g_str_hash, g_str_equal);

  if (ret = g_hash_table_lookup (loaded_themes, theme_path)){}
  else
    {
      ret  = glide_theme_new (theme_path);
      g_hash_table_insert (loaded_themes, (gpointer)g_strdup (theme_path), (gpointer)ret);
    }
  return ret;
}

void
glide_theme_manager_load_all ()
{
  GList *t;
  
  for (t = themes; t; t = t->next)
    {
      const gchar *p = (const gchar *)t->data;
      glide_theme_manager_load_theme (p);
    }
}

void
glide_theme_manager_refresh_theme_list ()
{
  GDir *dir;
  GError *e = NULL;
  gchar *theme_dir;
  const gchar *theme_file;
  GLIDE_NOTE(THEME_MANAGER, "Loading themes");
  
  if (themes)
    {
      g_list_foreach (themes, (GFunc) (g_free), NULL);
      g_list_free (themes);
      
      themes = NULL;
    }
  
  theme_dir = glide_dirs_get_glide_theme_dir ();
  
  dir = g_dir_open (theme_dir, 0, &e);
  if (e)
    {
      g_warning ("Error loading themes: %s", e->message);

      g_error_free (e);      
      g_free (theme_dir);
      return;
    }
  
  while ((theme_file = g_dir_read_name (dir)))
    {
      
      if (g_str_has_suffix (theme_file, "glide-theme"))
	{
	  gchar *full_path = g_build_filename (theme_dir, theme_file, NULL);

	  GLIDE_NOTE (THEME_MANAGER, "Found theme: %s",full_path);
	  themes = g_list_append (themes, full_path);
	}
    }

  g_dir_close (dir);
  
  g_free (theme_dir);
}
