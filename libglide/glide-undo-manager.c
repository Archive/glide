/*
 * glide-undo-manager.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-undo-manager
 * @short_description: Orchestrates and manages undo actions for a document.
 *
 */



#include "glide-undo-manager.h"
#include "glide-stage-manager.h"
#include "glide-actor.h"
#include "glide-slide.h"

#include "glide-undo-manager-priv.h"

#include "glide-debug.h"

#include <girepository.h>

G_DEFINE_TYPE(GlideUndoManager, glide_undo_manager, G_TYPE_OBJECT)

#define GLIDE_UNDO_MANAGER_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_UNDO_MANAGER, GlideUndoManagerPrivate))

enum {
  POSITION_CHANGED,
  LAST_SIGNAL
};

static guint undo_manager_signals[LAST_SIGNAL] = { 0, };

static GList *
glide_undo_manager_free_undo_info (GList *list)
{
  GlideUndoInfo *info = (GlideUndoInfo *)list->data;
  if (!info)
    return list->next;

  info->free_callback (info);
  g_free (info->label);
  g_free (info);
  
  return list->next;
}

typedef struct _GlideUndoDeleteActorData {
  ClutterActor *parent;
  ClutterActor *actor;
} GlideUndoDeleteActorData;

static void
glide_undo_delete_actor_info_free_callback (GlideUndoInfo *info)
{
  GlideUndoDeleteActorData *data = 
    (GlideUndoDeleteActorData *)info->user_data;
  g_object_unref (G_OBJECT (data->parent));
  g_object_unref (G_OBJECT (data->actor));
  
  g_free (data);
}

static gboolean
glide_undo_delete_actor_undo_callback (GlideUndoManager *undo_manager,
				       GlideUndoInfo *info)
{
  GlideUndoDeleteActorData *data = 
    (GlideUndoDeleteActorData *)info->user_data;
  
  clutter_container_add_actor (CLUTTER_CONTAINER (data->parent),
			       data->actor);
  clutter_actor_show (data->actor);
  
  return TRUE;
}

static gboolean
glide_undo_delete_actor_redo_callback (GlideUndoManager *undo_manager,
				       GlideUndoInfo *info)
{
  GlideUndoDeleteActorData *data =
    (GlideUndoDeleteActorData *)info->user_data;
  GlideStageManager *manager;
  
  manager = glide_actor_get_stage_manager (GLIDE_ACTOR (data->actor));
  if (glide_stage_manager_get_selection (manager) == (GlideActor *)data->actor)
    glide_stage_manager_set_selection (manager, NULL);
  
  
  clutter_container_remove_actor (CLUTTER_CONTAINER (data->parent),
				  data->actor);
  
  return TRUE;
}

typedef struct _GlideUndoSlideData {
  GlideSlide *slide;
  
  ClutterColor old_color;
  gchar *old_background;
  
  ClutterColor new_color;
  gchar *new_background;
} GlideUndoSlideData;

static void
glide_undo_slide_info_free_callback (GlideUndoInfo *info)
{
  GlideUndoSlideData *d = (GlideUndoSlideData *)info->user_data;
  
  g_free (d->old_background);
  g_free (d->new_background);

  g_object_unref (G_OBJECT (d->slide));  

  g_free (d);
}

static gboolean
glide_undo_slide_action_undo_callback (GlideUndoManager *undo_manager,
				       GlideUndoInfo *info)
{
  GlideUndoSlideData *data = (GlideUndoSlideData *)info->user_data;
  
  glide_slide_set_background (data->slide, data->old_background);
  glide_slide_set_color (data->slide, &data->old_color);
							      
  return TRUE;
}

static gboolean
glide_undo_slide_action_redo_callback (GlideUndoManager *undo_manager,
				       GlideUndoInfo *info)
{
  GlideUndoSlideData *data = (GlideUndoSlideData *)info->user_data;
  
  glide_slide_set_background (data->slide, data->new_background);
  glide_slide_set_color (data->slide, &data->new_color);
							      
  return TRUE;
}

typedef struct _GlideUndoActorData {
  ClutterActor *actor;

  JsonObject *old_state;
  JsonObject *new_state;
} GlideUndoActorData;

static void
glide_undo_actor_info_free_callback (GlideUndoInfo *info)
{
  GlideUndoActorData *data = (GlideUndoActorData *)info->user_data;
  
  g_object_unref (G_OBJECT (data->actor));
  json_object_unref (data->old_state);
  json_object_unref (data->new_state);
  
  g_free (data);
}

static gboolean
glide_undo_actor_action_undo_callback (GlideUndoManager *undo_manager,
				       GlideUndoInfo *info)
{
  GlideUndoActorData *data = (GlideUndoActorData *)info->user_data;
  
  glide_actor_deserialize (GLIDE_ACTOR (data->actor), data->old_state);
							      
  return TRUE;
}

static gboolean
glide_undo_actor_action_redo_callback (GlideUndoManager *undo_manager,
				       GlideUndoInfo *info)
{
  GlideUndoActorData *data = (GlideUndoActorData *)info->user_data;
  
  glide_actor_deserialize (GLIDE_ACTOR (data->actor), data->new_state);
							      
  return TRUE;
}

/**
 * glide_undo_manager_start_slide_action:
 * @manager: A #GlideUndoManager.
 * @slide: A #GlideSlide to record
 * @label: The undo label for this action.
 *
 * BUG: An equivalent of glide_undo_manager_start_actor_action to hack around
 * serialization API mess in #GlideSlide.
 */
void
glide_undo_manager_start_slide_action (GlideUndoManager *manager,
				       GlideSlide *slide,
				       const gchar *label)
{
  manager->priv->recorded_actor = (ClutterActor *)slide;
  manager->priv->recorded_label = g_strdup (label);
  
  manager->priv->recorded_background = g_strdup (glide_slide_get_background (slide));
  glide_slide_get_color (slide, &manager->priv->recorded_color);
}

/**
 * glide_undo_manager_end_slide_action:
 * @manager: A #GlideUndoManager
 * @slide: A #GlideSlide
 *
 * See glide_undo_manager_start_slide_action()
 *
 */
void
glide_undo_manager_end_slide_action (GlideUndoManager *manager,
				     GlideSlide *slide)
{
  GlideUndoInfo *info;
  GlideUndoSlideData *data;
  
  g_return_if_fail (GLIDE_IS_UNDO_MANAGER (manager));
  g_return_if_fail (GLIDE_IS_SLIDE (slide));
  
  if (manager->priv->recorded_actor != (ClutterActor *)slide)
    {
      g_warning ("Error, mismatched undo manager start/end slide actions.");
      return;
    }
  
  info = g_malloc (sizeof (GlideUndoInfo));
  data = g_malloc (sizeof (GlideUndoSlideData));
  
  info->undo_callback = glide_undo_slide_action_undo_callback;
  info->redo_callback = glide_undo_slide_action_redo_callback;
  info->free_callback = glide_undo_slide_info_free_callback;
  info->label = manager->priv->recorded_label;
  info->user_data = data;
  
  data->slide = (GlideSlide *)g_object_ref (G_OBJECT (slide));
  data->old_color = manager->priv->recorded_color;
  data->old_background = manager->priv->recorded_background;
  
  glide_slide_get_color (slide, &data->new_color);
  data->new_background = g_strdup (glide_slide_get_background (slide));
  
  glide_undo_manager_append_info (manager, info);
}

/**
 * glide_undo_manager_start_actor_action:
 * @manager: A #GlideUndoManager
 * @actor: A #GlideActor to record
 * @label: The undo label for this action
 *
 * Begins recording an undo action for an actor by snapshotting it's state
 * using glide_actor_serialize(). When balanced by a call to glide_undo_manager_end_actor_action()
 * an undo action representing the changes to @actor between these calls will be appended to
 * @manager.
 *
 */
void
glide_undo_manager_start_actor_action (GlideUndoManager *manager,
				       GlideActor *actor,
				       const gchar *label)
{
  JsonNode *anode;
  
  g_return_if_fail (GLIDE_IS_UNDO_MANAGER (manager));
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  
  manager->priv->recorded_actor = (ClutterActor *)actor;
  
  anode = glide_actor_serialize (actor);
  manager->priv->recorded_state = json_node_get_object (anode);
  
  manager->priv->recorded_label = g_strdup (label);
}

/**
 * glide_undo_manager_cancel_actor_action:
 * @manager: A #GlideUndoManager
 *
 * Cancels the recording of an actor action previously started
 * with glide_undo_manager_start_actor_action() .
 *
 */
void
glide_undo_manager_cancel_actor_action (GlideUndoManager *manager)
{
  g_return_if_fail (GLIDE_IS_UNDO_MANAGER (manager));
  
  manager->priv->recorded_actor = NULL;
  manager->priv->recorded_state = NULL;
  
  g_free (manager->priv->recorded_label);
}

/**
 * glide_undo_manager_end_actor_action:
 * @manager: A #GlideUndoManager
 * @actor: A #GlideActor, matching a balanced call to glide_undo_manager_start_actor_action().
 *
 * Finishes recording an actor action as started by glide_undo_manager_start_actor_action().
 *
 */

void
glide_undo_manager_end_actor_action (GlideUndoManager *manager,
				     GlideActor *actor)
{
  GlideUndoInfo *info;
  GlideUndoActorData *data;
  JsonNode *new_node;
  
  g_return_if_fail (GLIDE_IS_UNDO_MANAGER (manager));
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  
  if (manager->priv->recorded_actor != (ClutterActor *)actor)
    {
      g_warning ("Error, mismatched undo manager start/end actor actions.");
      return;
    }
  
  new_node = glide_actor_serialize (actor);

  info = g_malloc (sizeof (GlideUndoInfo));
  data = g_malloc (sizeof (GlideUndoActorData));
  
  info->undo_callback = glide_undo_actor_action_undo_callback;
  info->redo_callback = glide_undo_actor_action_redo_callback;
  info->free_callback = glide_undo_actor_info_free_callback;
  info->label = manager->priv->recorded_label;
  info->user_data = data;

  
  data->actor = (ClutterActor *)g_object_ref (G_OBJECT (actor));
  data->old_state = manager->priv->recorded_state;
  data->new_state = json_node_get_object (new_node);
  
  glide_undo_manager_append_info (manager, info);
}

/**
 * glide_undo_manager_append_delete:
 * @manager: A #GlideUndoManager
 * @a: The deleted actor
 *
 * Appends a new undo action to @manager for deleting @actor
 *
 */
void
glide_undo_manager_append_delete (GlideUndoManager *manager,
				  GlideActor *actor)
{
  GlideUndoInfo *info;
  GlideUndoDeleteActorData *data;
  ClutterActor *parent;
  
  g_return_if_fail (GLIDE_IS_UNDO_MANAGER (manager));
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  
  parent = clutter_actor_get_parent (CLUTTER_ACTOR (actor));
  if (!parent)
    {
      g_warning ("glide_undo_manager_append_delete: no parent.");
      return;
    }
  
  info = g_malloc (sizeof (GlideUndoInfo));
  data = g_malloc (sizeof (GlideUndoDeleteActorData));
  
  info->free_callback = glide_undo_delete_actor_info_free_callback;
  info->undo_callback = glide_undo_delete_actor_undo_callback;
  info->redo_callback = glide_undo_delete_actor_redo_callback;
  info->label = g_strdup("Delete object");
  info->user_data = data;
  
  data->actor = (ClutterActor *)g_object_ref (actor);
  data->parent = g_object_ref (parent);
  
  glide_undo_manager_append_info (manager, info);
}

/**
 * glide_undo_manager_append_insert:
 * @manager: A #GlideUndoManager
 * @a: The inserted actor
 *
 * Appends a new undo action to @manager for inserting @actor
 *
 */
void
glide_undo_manager_append_insert (GlideUndoManager *manager,
				  GlideActor *actor)
{
  GlideUndoInfo *info;
  GlideUndoDeleteActorData *data;
  ClutterActor *parent;

  g_return_if_fail (GLIDE_IS_UNDO_MANAGER (manager));
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  
  parent = clutter_actor_get_parent (CLUTTER_ACTOR (actor));
  if (!parent)
    {
      g_warning ("glide_undo_manager_append_delete: no parent.");
      return;
    }
  
  info = g_malloc (sizeof (GlideUndoInfo));
  data = g_malloc (sizeof (GlideUndoDeleteActorData));
  
  info->free_callback = glide_undo_delete_actor_info_free_callback;
  info->redo_callback = glide_undo_delete_actor_undo_callback;
  info->undo_callback = glide_undo_delete_actor_redo_callback;
  info->label = g_strdup("Insert object");
  info->user_data = data;
  
  data->actor = (ClutterActor *)g_object_ref (actor);
  data->parent = g_object_ref (parent);
  
  glide_undo_manager_append_info (manager, info);
}

static void
glide_undo_manager_init (GlideUndoManager *manager)
{
  manager->priv = GLIDE_UNDO_MANAGER_GET_PRIVATE (manager);
  
  manager->priv->infos = g_list_append (manager->priv->infos, NULL);
}

static void
glide_undo_manager_finalize (GObject *object)
{
  GlideUndoManager *manager = GLIDE_UNDO_MANAGER (object);
  GList *t = manager->priv->infos;
  while (t)
    t = glide_undo_manager_free_undo_info (t);
  g_list_free (manager->priv->infos);
}

static void
glide_undo_manager_class_init (GlideUndoManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = glide_undo_manager_finalize;
  
  undo_manager_signals[POSITION_CHANGED] = 
    g_signal_new ("position-changed",
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST,
		  0,
		  NULL, NULL,
		  gi_cclosure_marshal_generic,
		  G_TYPE_NONE, 0, NULL);
  
  g_type_class_add_private (object_class, sizeof(GlideUndoManagerPrivate));
}

/**
 * glide_undo_manager_new:
 *
 * Creates a new #GlideUndoManager.
 *
 * Return value: The newly allocated #GlideUndoManager.
 */
GlideUndoManager *
glide_undo_manager_new ()
{
  return g_object_new (GLIDE_TYPE_UNDO_MANAGER,
		       NULL);
}

/**
 * glide_undo_manager_append_info:
 * @manager: A #GlideUndoManager
 * @info: A #GlideUndoInfo to append.
 *
 * Appends @info to the list of undo's for @manager as the most recent
 * action.
 *
 */
void
glide_undo_manager_append_info (GlideUndoManager *manager, GlideUndoInfo *info)
{
  GList *t;
  
  g_return_if_fail (GLIDE_IS_UNDO_MANAGER (manager));
  
  t = g_list_next (manager->priv->position);
  while (t)
    t = glide_undo_manager_free_undo_info (t);
  if (manager->priv->position)
    {
      g_list_free (g_list_next (manager->priv->position));
      manager->priv->position->next = NULL;
    }
  else
    {
      g_list_free (manager->priv->infos);
      manager->priv->infos = NULL;
    }

  manager->priv->infos = g_list_append (manager->priv->infos, info);
  manager->priv->position = g_list_last (manager->priv->infos);
  
  g_signal_emit (manager, undo_manager_signals[POSITION_CHANGED], 0);
}

// TODO: Handle failed redo/undos.
/**
 * glide_undo_manager_redo:
 * @manager: A #GlideUndoManager
 *
 * Performs the last redo action for @manager and updates
 * the undo position.
 *
 * Return value: %TRUE on success, %FALSE on failure.
 */
gboolean
glide_undo_manager_redo (GlideUndoManager *manager)
{
  GlideUndoInfo *info;

  g_return_val_if_fail (GLIDE_IS_UNDO_MANAGER (manager), FALSE);
  
  if (!manager->priv->position->next)
    return FALSE;
  else
    info = (GlideUndoInfo *)manager->priv->position->next->data;
  
  manager->priv->position = manager->priv->position->next;
  g_signal_emit (manager, undo_manager_signals[POSITION_CHANGED], 0);  

  return info->redo_callback (manager, info);
}
/**
 * glide_undo_manager_undo:
 * @manager: A #GlideUndoManager
 *
 * Performs the last undo action for @manager and updates
 * the undo position.
 *
 * Return value: %TRUE on success, %FALSE on failure.
 */
gboolean
glide_undo_manager_undo (GlideUndoManager *manager)
{
  GlideUndoInfo *info;

  g_return_val_if_fail (GLIDE_IS_UNDO_MANAGER (manager), FALSE);

  if (!manager->priv->position->data)
    return FALSE;
  else
    info = (GlideUndoInfo *)manager->priv->position->data;
  
  manager->priv->position = manager->priv->position->prev;
  g_signal_emit (manager, undo_manager_signals[POSITION_CHANGED], 0);  
  
  return info->undo_callback (manager, info);
}

/**
 * glide_undo_manager_get_can_undo:
 * @manager: A #GlideUndoManager
 *
 * Returns whether @manager has available undo actions.
 *
 * Return value: %TRUE if @manager has available undo actions, %FALSE otherwise.
 */
gboolean 
glide_undo_manager_get_can_undo (GlideUndoManager *manager)
{
  g_return_val_if_fail (GLIDE_IS_UNDO_MANAGER (manager), FALSE);
  return (manager->priv->position && manager->priv->position->data != NULL);
}

/**
 * glide_undo_manager_get_can_redo:
 * @manager: A #GlideUndoManager
 *
 * Returns whether @manager has available redo actions.
 *
 * Return value: %TRUE if @manager has available redo actions, %FALSE otherwise.
 */
gboolean 
glide_undo_manager_get_can_redo (GlideUndoManager *manager)
{
  g_return_val_if_fail (GLIDE_IS_UNDO_MANAGER (manager), FALSE);
  return (manager->priv->position && manager->priv->position->next != NULL);
}

/**
 * glide_undo_manager_get_undo_label:
 * @manager: A #GlideUndoManager
 *
 * Returns the label of the next undo action for @manager.
 *
 * Return value: The label of the next undo action.
 */
const gchar *
glide_undo_manager_get_undo_label (GlideUndoManager *manager)
{
  GlideUndoInfo *info;

  g_return_val_if_fail (GLIDE_IS_UNDO_MANAGER (manager), NULL);
  info  = (GlideUndoInfo *)manager->priv->position->data;
  return info->label;
}

/**
 * glide_undo_manager_get_redo_label:
 * @manager: A #GlideUndoManager
 *
 * Returns the label of the next redo action for @manager.
 *
 * Return value: The label of the next redo action.
 */
const gchar *
glide_undo_manager_get_redo_label (GlideUndoManager *manager)
{
  GlideUndoInfo *info;

  g_return_val_if_fail (GLIDE_IS_UNDO_MANAGER (manager), NULL);
  info  = (GlideUndoInfo *)manager->priv->position->next->data;
  return info->label;
}

