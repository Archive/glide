/*
 * glide-inspector-window.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_INSPECTOR_WINDOW_H__
#define __GLIDE_INSPECTOR_WINDOW_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GLIDE_TYPE_INSPECTOR_WINDOW              (glide_inspector_window_get_type())
#define GLIDE_INSPECTOR_WINDOW(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_INSPECTOR_WINDOW, GlideInspectorWindow))
#define GLIDE_INSPECTOR_WINDOW_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_INSPECTOR_WINDOW, GlideInspectorWindowClass))
#define GLIDE_IS_INSPECTOR_WINDOW(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_INSPECTOR_WINDOW))
#define GLIDE_IS_INSPECTOR_WINDOW_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_INSPECTOR_WINDOW))
#define GLIDE_INSPECTOR_WINDOW_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_INSPECTOR_WINDOW, GlideInspectorWindowClass))

typedef struct _GlideInspectorWindowPrivate GlideInspectorWindowPrivate;


typedef struct _GlideInspectorWindow GlideInspectorWindow;

struct _GlideInspectorWindow
{
  GtkWindow window;
  
  GlideInspectorWindowPrivate *priv;
};

typedef struct _GlideInspectorWindowClass GlideInspectorWindowClass;

struct _GlideInspectorWindowClass
{
  GtkWindowClass parent_class;
};

GType glide_inspector_window_get_type (void) G_GNUC_CONST;
GtkWidget *glide_inspector_window_new (void);

GtkWidget *glide_inspector_window_get_inspector (GlideInspectorWindow *window);


G_END_DECLS

#endif
