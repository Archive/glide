/*
 * glide-binding.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * e-binding.h
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 */

/* This is a direct rip-off of Xfce's excellent ExoBinding API,
 * which binds two GObject properties together.  ExoBinding was
 * written by Benedikt Meurer <benny@xfce.org>. */

#ifndef __GLIDE_BINDING_H__
#define __GLIDE_BINDING_H__

#include "glide-gtk-util.h"

G_BEGIN_DECLS

typedef struct _GlideBinding GlideBinding;
typedef struct _GlideBindingBase GlideBindingBase;
typedef struct _GlideBindingLink GlideBindingLink;
typedef struct _GlideMutualBinding GlideMutualBinding;

typedef struct _GlideMutualBindingPool {
  GList *bindings;
} GlideMutualBindingPool;

typedef gboolean	(*GlideBindingTransform)	(const GValue *src_value,
						 GValue *dst_value,
						 gpointer user_data);

struct _GlideBindingBase {
	GDestroyNotify destroy;
};

struct _GlideBindingLink {
	GObject *dst_object;
	GParamSpec *dst_pspec;
	gulong dst_handler; /* only set for mutual bindings */
	gulong handler;
	GlideBindingTransform transform;
	gpointer user_data;
};

struct _GlideBinding {
	/*< private >*/
	GObject *src_object;
	GlideBindingBase base;
	GlideBindingLink link;
};

struct _GlideMutualBinding {
	/*< private >*/
	GlideBindingBase  base;
	GlideBindingLink  direct;
	GlideBindingLink  reverse;
};

GlideBinding *	glide_binding_new			(gpointer src_object,
						 const gchar *src_property,
						 gpointer dst_object,
						 const gchar *dst_property);
GlideBinding *	glide_binding_new_full		(gpointer src_object,
						 const gchar *src_property,
						 gpointer dst_object,
						 const gchar *dst_property,
						 GlideBindingTransform transform,
						 GDestroyNotify destroy_notify,
						 gpointer user_data);
GlideBinding *	glide_binding_new_with_negation	(gpointer src_object,
						 const gchar *src_property,
						 gpointer dst_object,
						 const gchar *dst_property);
void		glide_binding_unbind		(GlideBinding *binding);

GlideMutualBinding *glide_mutual_binding_new		(gpointer object1,
						 const gchar *property1,
						 gpointer object2,
						 const gchar *property2);
GlideMutualBinding *glide_mutual_binding_new_full	(gpointer object1,
						 const gchar *property1,
						 gpointer object2,
						 const gchar *property2,
						 GlideBindingTransform transform,
						 GlideBindingTransform reverse_transform,
						 GDestroyNotify destroy_notify,
						 gpointer user_data);
GlideMutualBinding *glide_mutual_binding_new_with_negation
						(gpointer object1,
						 const gchar *property1,
						 gpointer object2,
						 const gchar *property2);
void		glide_mutual_binding_unbind		(GlideMutualBinding *binding);

/* Useful transformation functions */
gboolean	glide_binding_transform_color_to_string
						(const GValue *src_value,
						 GValue *dst_value,
						 gpointer user_data);
gboolean	glide_binding_transform_string_to_color
						(const GValue *src_value,
						 GValue *dst_value,
						 gpointer user_data);

gboolean glide_binding_transform_gdk_color_to_clutter_color (const GValue *src_value,
							     GValue *dst_value,
							     gpointer user_data);
gboolean glide_binding_transform_clutter_color_to_gdk_color (const GValue *src_value,
							     GValue *dst_value,
							     gpointer user_data);

GlideMutualBindingPool *glide_mutual_binding_pool_new ();

GlideMutualBindingPool *
glide_mutual_binding_pool_add_full (GlideMutualBindingPool *pool,
				    GObject *object1,
				    const gchar *property1,
				    GObject *object2,
				    const gchar *property2,
				    GlideBindingTransform transform,
				    GlideBindingTransform reverse_transform);
				

void glide_mutual_binding_pool_unbind (GlideMutualBindingPool *pool);

void glide_mutual_binding_pool_free (GlideMutualBindingPool *pool);

G_END_DECLS

#endif /* __GLIDE_BINDING_H__ */
