/*
 * glide-inspector-notebook.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_INSPECTOR_NOTEBOOK_H__
#define __GLIDE_INSPECTOR_NOTEBOOK_H__

#include <gtk/gtk.h>
#include "glide-stage-manager.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_INSPECTOR_NOTEBOOK              (glide_inspector_notebook_get_type())
#define GLIDE_INSPECTOR_NOTEBOOK(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_INSPECTOR_NOTEBOOK, GlideInspectorNotebook))
#define GLIDE_INSPECTOR_NOTEBOOK_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_INSPECTOR_NOTEBOOK, GlideInspectorNotebookClass))
#define GLIDE_IS_INSPECTOR_NOTEBOOK(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_INSPECTOR_NOTEBOOK))
#define GLIDE_IS_INSPECTOR_NOTEBOOK_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_INSPECTOR_NOTEBOOK))
#define GLIDE_INSPECTOR_NOTEBOOK_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_INSPECTOR_NOTEBOOK, GlideInspectorNotebookClass))

typedef struct _GlideInspectorNotebookPrivate GlideInspectorNotebookPrivate;


typedef struct _GlideInspectorNotebook GlideInspectorNotebook;

struct _GlideInspectorNotebook
{
  GtkNotebook notebook;
  
  GlideInspectorNotebookPrivate *priv;
};

typedef struct _GlideInspectorNotebookClass GlideInspectorNotebookClass;

struct _GlideInspectorNotebookClass
{
  GtkNotebookClass parent_class;
};

GType glide_inspector_notebook_get_type (void) G_GNUC_CONST;
GtkWidget *glide_inspector_notebook_new (void);

GlideStageManager *glide_inspector_notebook_get_stage_manager (GlideInspectorNotebook *inspector);
void glide_inspector_notebook_set_stage_manager (GlideInspectorNotebook *inspector, GlideStageManager *manager);

void glide_inspector_notebook_get_text_color (GlideInspectorNotebook *ins, ClutterColor *cc);
const gchar *glide_inspector_notebook_get_font_name (GlideInspectorNotebook *ins);


G_END_DECLS

#endif
