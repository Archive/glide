/*
 * glide-theme-preview-widget.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_THEME_PREVIEW_WIDGET_H__
#define __GLIDE_THEME_PREVIEW_WIDGET_H__

#include <gtk/gtk.h>
#include "glide-theme.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_THEME_PREVIEW_WIDGET              (glide_theme_preview_widget_get_type())
#define GLIDE_THEME_PREVIEW_WIDGET(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_THEME_PREVIEW_WIDGET, GlideThemePreviewWidget))
#define GLIDE_THEME_PREVIEW_WIDGET_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_THEME_PREVIEW_WIDGET, GlideThemePreviewWidgetClass))
#define GLIDE_IS_THEME_PREVIEW_WIDGET(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_THEME_PREVIEW_WIDGET))
#define GLIDE_IS_THEME_PREVIEW_WIDGET_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_THEME_PREVIEW_WIDGET))
#define GLIDE_THEME_PREVIEW_WIDGET_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_THEME_PREVIEW_WIDGET, GlideThemePreviewWidgetClass))

typedef struct _GlideThemePreviewWidgetPrivate GlideThemePreviewWidgetPrivate;


typedef struct _GlideThemePreviewWidget GlideThemePreviewWidget;

struct _GlideThemePreviewWidget
{
  GtkDrawingArea drawing_area;
  
  GlideThemePreviewWidgetPrivate *priv;
};

typedef struct _GlideThemePreviewWidgetClass GlideThemePreviewWidgetClass;

struct _GlideThemePreviewWidgetClass
{
  GtkDrawingAreaClass parent_class;
};

GType glide_theme_preview_widget_get_type (void) G_GNUC_CONST;
GtkWidget *glide_theme_preview_widget_new (void);

void glide_theme_preview_widget_set_theme (GlideThemePreviewWidget *preview, GlideTheme *theme);
GlideTheme *glide_theme_preview_widget_get_theme (GlideThemePreviewWidget *preview);

G_END_DECLS

#endif
