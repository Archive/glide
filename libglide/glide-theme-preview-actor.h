/*
 * glide-theme-preview-actor.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_THEME_PREVIEW_ACTOR_H__
#define __GLIDE_THEME_PREVIEW_ACTOR_H__

#include <clutter/clutter.h>
#include "glide-theme.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_THEME_PREVIEW_ACTOR              (glide_theme_preview_actor_get_type())
#define GLIDE_THEME_PREVIEW_ACTOR(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_THEME_PREVIEW_ACTOR, GlideThemePreviewActor))
#define GLIDE_THEME_PREVIEW_ACTOR_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_THEME_PREVIEW_ACTOR, GlideThemePreviewActorClass))
#define GLIDE_IS_THEME_PREVIEW_ACTOR(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_THEME_PREVIEW_ACTOR))
#define GLIDE_IS_THEME_PREVIEW_ACTOR_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_THEME_PREVIEW_ACTOR))
#define GLIDE_THEME_PREVIEW_ACTOR_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_THEME_PREVIEW_ACTOR, GlideThemePreviewActorClass))

typedef struct _GlideThemePreviewActorPrivate GlideThemePreviewActorPrivate;


typedef struct _GlideThemePreviewActor GlideThemePreviewActor;

struct _GlideThemePreviewActor
{
  ClutterGroup group;
  
  GlideThemePreviewActorPrivate *priv;
};

typedef struct _GlideThemePreviewActorClass GlideThemePreviewActorClass;

struct _GlideThemePreviewActorClass
{
  ClutterGroupClass parent_class;
};

GType glide_theme_preview_actor_get_type (void) G_GNUC_CONST;
ClutterActor *glide_theme_preview_actor_new (void);

void glide_theme_preview_actor_set_theme (GlideThemePreviewActor *preview, GlideTheme *theme);
GlideTheme *glide_theme_preview_actor_get_theme (GlideThemePreviewActor *preview);

void glide_theme_preview_actor_set_selected (GlideThemePreviewActor *preview, gboolean selected);
gboolean glide_theme_preview_actor_get_selected (GlideThemePreviewActor *preview);

G_END_DECLS

#endif
