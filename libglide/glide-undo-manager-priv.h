/*
 * glide-undo-manager-priv.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 
#ifndef __GLIDE_UNDO_MANAGER_PRIVATE_H__
#define __GLIDE_UNDO_MANAGER_PRIVATE_H__

#include "glide-undo-manager.h"

G_BEGIN_DECLS

struct _GlideUndoManagerPrivate
{
  ClutterActor *recorded_actor;
  // TODO: Check. do we leak the nodes?...maybe this should be in
  // GlideActor too..
  JsonObject *recorded_state;
  gchar *recorded_label;
  
  gchar *recorded_background;
  ClutterColor recorded_color;

  GList *infos;
  GList *position;
};

G_END_DECLS

#endif  /* __GLIDE_UNDO_MANAGER_PRIVATE_H__  */
