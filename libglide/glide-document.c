/*
 * glide-document.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-document
 * @short_description: Representation of a loaded document file
 *
 */

#include <stdlib.h>
 
#include "glide-document.h"
#include "glide-document-priv.h"

#include <girepository.h>
#include <glib/gstdio.h>

#include <gio/gio.h>

#include "glide-debug.h"

#include "glide-slide.h"

#include "glide-gtk-util.h"

#include "glide.h"

#include "glide-dirs.h"

G_DEFINE_TYPE(GlideDocument, glide_document, G_TYPE_OBJECT)

#define GLIDE_DOCUMENT_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_DOCUMENT, GlideDocumentPrivate))

enum {
  PROP_0,
  PROP_NAME,
  PROP_PATH,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_DIRTY,
  PROP_THEME
};

#define DEFAULT_PRESENTATION_WIDTH 800
#define DEFAULT_PRESENTATION_HEIGHT 600

enum {
  SLIDE_ADDED,
  SLIDE_REMOVED,
  RESIZED,
  LAST_SIGNAL
};

static guint document_signals[LAST_SIGNAL] = { 0, };

static void
glide_document_finalize (GObject *object)
{
  GlideDocument *document = GLIDE_DOCUMENT (object);

  GLIDE_NOTE (DOCUMENT, "Finalizing document: %s",
	      document->priv->name);
  
  g_free (document->priv->name);
  g_free (document->priv->path);
  g_free (document->priv->working_path);
  
  G_OBJECT_CLASS (glide_document_parent_class)->finalize (object);
}


static void
glide_document_get_property (GObject *object,
				  guint prop_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  GlideDocument *document = GLIDE_DOCUMENT (object);
  
  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, document->priv->name);
      break;
    case PROP_PATH:
      g_value_set_string (value, document->priv->path);
      break;
    case PROP_WIDTH:
      g_value_set_int (value, document->priv->width);
      break;
    case PROP_HEIGHT:
      g_value_set_int (value, document->priv->height);
      break;
    case PROP_DIRTY:
      g_value_set_boolean (value, document->priv->dirty);
      break;
    case PROP_THEME:
      g_value_set_object (value, document->priv->theme);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_document_set_property (GObject *object,
			     guint prop_id,
			     const GValue *value,
			     GParamSpec *pspec)
{
  GlideDocument *document = GLIDE_DOCUMENT (object);
  
  switch (prop_id)
    {
    case PROP_NAME:
      g_return_if_fail (document->priv->name == NULL);
      document->priv->name = g_value_dup_string (value);
      GLIDE_NOTE (DOCUMENT, "Constructing new GlideDocument (%p): %s",
		  object, document->priv->name);
      break;
    case PROP_WIDTH:
      document->priv->width = g_value_get_int (value);
      break;
    case PROP_HEIGHT:
      document->priv->height = g_value_get_int (value);
      break;
    case PROP_PATH:
      if (document->priv->path)
	g_free (document->priv->path);
      document->priv->path = g_value_dup_string (value);
      break;
    case PROP_DIRTY:
      glide_document_set_dirty (document, g_value_get_boolean (value));
      break;
    case PROP_THEME:
      glide_document_set_theme (document, (GlideTheme *)g_value_get_object (value));
      break;
    default: 
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_document_class_init (GlideDocumentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = glide_document_finalize;
  object_class->set_property = glide_document_set_property;
  object_class->get_property = glide_document_get_property;

  g_object_class_install_property (object_class, PROP_NAME,
				   g_param_spec_string ("name",
							"Name",
							"Name of the document",
							NULL,
							G_PARAM_READWRITE |
							G_PARAM_STATIC_STRINGS |
							G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, PROP_PATH,
				   g_param_spec_string ("path",
							"Path",
							"Path of the document",
							NULL,
							G_PARAM_READWRITE |
							G_PARAM_STATIC_STRINGS |
							G_PARAM_CONSTRUCT_ONLY));

  g_object_class_install_property (object_class, PROP_WIDTH,
				   g_param_spec_int ("width",
						     "Width",
						     "Width of the document",
						     -1, G_MAXINT, DEFAULT_PRESENTATION_WIDTH,
						     G_PARAM_READWRITE |
						     G_PARAM_STATIC_STRINGS |
						     G_PARAM_CONSTRUCT));

  g_object_class_install_property (object_class, PROP_HEIGHT,
				   g_param_spec_int ("height",
						     "Height",
						     "Height of the document",
						     -1, G_MAXINT, DEFAULT_PRESENTATION_HEIGHT,
						     G_PARAM_READWRITE |
						     G_PARAM_STATIC_STRINGS |
						     G_PARAM_CONSTRUCT));
  
  g_object_class_install_property (object_class, PROP_DIRTY,
				   g_param_spec_boolean ("dirty",
							 "Dirty",
							 "Whether the document has dirty changes",
							 FALSE,
							 G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class,
				   PROP_THEME,
				   g_param_spec_object ("theme",
							"Theme",
							"The documents theme",
							GLIDE_TYPE_THEME,
							G_PARAM_READWRITE |
							G_PARAM_STATIC_STRINGS));



  document_signals[SLIDE_ADDED] = 
    g_signal_new ("slide-added",
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST,
		  0,
		  NULL, NULL,
		  gi_cclosure_marshal_generic,
		  G_TYPE_NONE, 1,
		  G_TYPE_OBJECT);

  document_signals[SLIDE_REMOVED] = 
    g_signal_new ("slide-removed",
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST,
		  0,
		  NULL, NULL,
		  gi_cclosure_marshal_generic,
		  G_TYPE_NONE, 1,
		  G_TYPE_OBJECT);
  
  document_signals[RESIZED] = 
    g_signal_new ("resized",
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST,
		  0,
		  NULL, NULL,
		  gi_cclosure_marshal_generic,
		  G_TYPE_NONE, 0, NULL);
							
  
  g_type_class_add_private (object_class, sizeof(GlideDocumentPrivate));
}

static void
glide_document_make_working_dir (GlideDocument *d)
{
  gchar *wdir, *rdir;
  wdir = glide_dirs_make_temp_dir ();
  rdir = g_strdup_printf("%s/resources", wdir);
  g_mkdir(rdir, 0700);
 
  if (d->priv->working_path)
    g_free (d->priv->working_path);

  d->priv->working_path = wdir;

  g_free (rdir);
}


static void
glide_document_init (GlideDocument *d)
{
  d->priv = GLIDE_DOCUMENT_GET_PRIVATE (d);
  
  glide_document_make_working_dir (d);
  glide_document_set_theme (d, glide_get_default_theme ());
}

/**
 * glide_document_new:
 * @name: The name of the new document.
 *
 * Creates a new document with @name.
 * 
 * BUG: Document names are currently meaningless?
 * Return value: The newly created #GlideDocument.
 */
GlideDocument *
glide_document_new (const gchar *name)
{
  return g_object_new (GLIDE_TYPE_DOCUMENT,
		       "name", name,
		       NULL);
}

/**
 * glide_document_get_name:
 * @document: A #GlideDocument
 *
 * Returns the name of @document.
 *
 * Return value: The name of @document
 */
const gchar *
glide_document_get_name (GlideDocument *document)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  return document->priv->name;
}

/**
 * glide_document_get_path:
 * @document: A #GlideDocument
 *
 * Returns the path of @document. Note that this is the
 * user visible document path, and the location the
 * archive will be saved to.
 *
 * Return value: The path of @document
 */
const gchar *
glide_document_get_path (GlideDocument *document)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  return document->priv->path;
}

/**
 * glide_document_get_working_path:
 * @document: A #GlideDocument
 *
 * Returns the working_path of @document. This is
 * the temporary path @document is currently extracted
 * to.
 *
 * Return value: The working_path of @document
 */
const gchar *
glide_document_get_working_path (GlideDocument *document)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  return document->priv->working_path;
}

/**
 * glide_document_set_path:
 * @document: A #GlideDocument
 * @path: The new path for @document
 *
 * Sets the path of @document to @path, future writes will
 * save the archive to this location.
 *
 */
void
glide_document_set_path (GlideDocument *document, const gchar *path)
{
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));
  document->priv->path = g_strdup (path);
  g_object_notify (G_OBJECT (document), "path");
}

/**
 * glide_document_get_n_slides:
 * @document: A #GlideDocument
 *
 * Returns the number of slides contained in @document.
 *
 * Return value: The number of slides in @document.
 */
guint
glide_document_get_n_slides (GlideDocument *document)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), 0);
  return g_list_length (document->priv->slides);
}

/**
 * glide_document_get_nth_slide:
 * @document: A #GlideDocument
 * @n: The slide index to get.
 *
 * Returns the slide with index @n from @document.
 *
 * Return value: The slide with index @n from @document.
 */
GlideSlide *
glide_document_get_nth_slide (GlideDocument *document,
			      guint n)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  g_return_val_if_fail (n < g_list_length (document->priv->slides), NULL);
  return GLIDE_SLIDE (g_list_nth_data (document->priv->slides, n));
}

static void
glide_document_update_slide_indices (GlideDocument *document)
{
  GList *l;
  guint i = 0;
  
  for (l = document->priv->slides; l; l = l->next)
    {
      GlideSlide *s = (GlideSlide *)l->data;
      
      glide_slide_set_index (s, i);
      i++;
    }
}

/**
 * glide_document_append_slide:
 * @document: A #GlideDocument
 *
 * Appends a newly created #GlideSlide to the end of @document.
 *
 * Return value: The newly created #GlideSlide.
 */
GlideSlide *
glide_document_append_slide (GlideDocument *document)
{
  GlideSlide *s;
  
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  
  s = glide_slide_new (document);
  
  glide_slide_set_index (s, g_list_length (document->priv->slides));
  document->priv->slides = g_list_append (document->priv->slides, s);
  
  g_signal_emit (document, document_signals[SLIDE_ADDED], 0, s);
  
  return s;
}

/**
 * glide_document_insert_slide:
 * @document: A #GlideDocument
 * @after: The slide index to insert after.
 *
 * Creates and inserts a new #GlideSlide in @document, after
 * the slide at index @after.
 *
 * Return value: The newly created #GlideSlide
 */
GlideSlide *
glide_document_insert_slide (GlideDocument *document, gint after)
{
  GlideSlide *s;
  
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  g_return_val_if_fail (after < g_list_length (document->priv->slides), NULL);

  s = glide_slide_new (document);

  document->priv->slides = g_list_insert (document->priv->slides, s, after+1);
  
  glide_slide_set_index (s, after+1);
  glide_document_update_slide_indices (document);
  
  g_signal_emit (document, document_signals[SLIDE_ADDED], 0, s);
  
  return s;
}

/**
 * glide_document_remove_slide:
 * @document: A #GlideDocument
 * @slide: The slide to remove.
 *
 * Removes the slide at @index from @document.
 *
 */
void
glide_document_remove_slide (GlideDocument *document, gint slide)
{
  GlideSlide *s;
  
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));
  g_return_if_fail (slide < g_list_length (document->priv->slides));

  s = g_list_nth_data (document->priv->slides, slide);
  document->priv->slides = g_list_remove (document->priv->slides, s);
  
  glide_document_update_slide_indices (document);

  g_signal_emit (document, document_signals[SLIDE_REMOVED], 0, s);
}

static void
glide_document_json_obj_set_name (GlideDocument *document, JsonObject *obj)
{
  JsonNode *node = json_node_new (JSON_NODE_VALUE);
  json_node_set_string (node, document->priv->name);
  
  json_object_set_member (obj, "name", node);
}

static void
glide_document_json_obj_set_slides (GlideDocument *document, JsonObject *obj)
{
  JsonNode *node = json_node_new (JSON_NODE_ARRAY);
  JsonArray *array = json_array_new ();
  GList *s;
  
  for (s = document->priv->slides; s; s = s->next)
    {
      JsonNode *n;
      GlideSlide *slide = (GlideSlide *)(s->data);
      
      n = glide_actor_serialize (GLIDE_ACTOR (slide));
      json_array_add_element (array, n);
    }
  json_node_take_array (node, array);

  json_object_set_member (obj, "slides", node);
  
}

/**
 * glide_document_serialize: 
 * @document: A #GlideDocument
 *
 * Serializes the contents of @document to JSON.
 *
 * Return value: A #JsonNode containing the serialized contents of @document.
 */
JsonNode *
glide_document_serialize(GlideDocument *document)
{
  JsonNode *node;
  JsonObject *obj;
  
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  
  node = json_node_new (JSON_NODE_OBJECT);
  
  obj = json_object_new ();
  json_node_set_object (node, obj);
  
  glide_document_json_obj_set_name (document, obj);
  glide_document_json_obj_set_slides (document, obj);
  
  return node;
}

/**
 * glide_document_get_height:
 * @document: A #Returns
 *
 * GlideDocument the height of @document.
 *
 * Return value: The height of @document.
 */
gint 
glide_document_get_height (GlideDocument *document)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), 0);
  return document->priv->height;
}

/**
 * glide_document_get_width:
 * @document: A #GlideDocument
 *
 * Returns the width of @document.
 *
 * Return value: The width of @document.
 */
gint 
glide_document_get_width (GlideDocument *document)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), 0);
  return document->priv->width;
}

/**
 * glide_document_get_size:
 * @document: A #GlideDocument.
 * @width: Location to write the width of @document.
 * @height: Location to write the height of @document.
 *
 * Returns the size of @document through @width and @height.
 *
 */
void
glide_document_get_size (GlideDocument *document, gint *width, gint *height)
{
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));
  *width = document->priv->width;
  *height = document->priv->height;
}

/**
 * glide_document_resize:
 * @document: A #GlideDocument
 * @width: The new width in pixels.
 * @height: The new height in pixels.
 *
 * Resizes @document and it's contents for display at @width by @height.
 *
 */
void
glide_document_resize (GlideDocument *document, gint width, gint height)
{
  GList *s;
  
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));

  document->priv->width = width;
  document->priv->height = height;

  g_signal_emit (document, document_signals[RESIZED], 0);  

  for (s=document->priv->slides; s; s=s->next)
    {
            GlideSlide *slide = (GlideSlide *)s->data;

	    glide_slide_resize (slide, width, height);
    }
}

/**
 * glide_document_get_dirty:
 * @document: A #GlideDocument
 *
 * Gets the value of the dirty flag for @document.
 *
 * Return value: %TRUE if @document has unsaved changes. False otherwise.
 */
gboolean
glide_document_get_dirty (GlideDocument *document)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), FALSE);
  return document->priv->dirty;
}

/**
 * glide_document_set_dirty:
 * @document: A #GlideDocument
 * @dirty: The new dirty value for @GlideDocument
 *
 * Sets the @dirty flag on @document, marking if @document
 * has unsaved changes.
 *
 */
void
glide_document_set_dirty (GlideDocument *document, gboolean dirty)
{
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));

  document->priv->dirty = dirty;
  g_object_notify (G_OBJECT (document), "dirty");
}

/**
 * glide_document_write_archive:
 * @document: A #GlideDocument
 *
 * Saves the archive file for @document at the path
 * of @document.
 */
void
glide_document_write_archive (GlideDocument *document)
{
  gchar *command;
  
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));

  command = g_strdup_printf("tar -cjspf %s %s",
				   document->priv->path,
				   document->priv->working_path);
  g_message("command %s", command);
  
  system (command);
}

/**
 * glide_document_write_json:
 * @document: A #GlideDocument
 *
 * Saves the JSON file for @document in the working path of
 * @document.
 */
void
glide_document_write_json (GlideDocument *document)
{
  GError *e = NULL;
  JsonNode *node;
  JsonGenerator *gen;
  gchar *json_path;
  
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));
  
  node = glide_document_serialize (document);
  gen = json_generator_new ();
  g_object_set (gen, "pretty", TRUE, NULL);
  
  json_generator_set_root (gen, node);
  
  if (!document->priv->working_path)
    glide_document_make_working_dir (document);
  json_path = g_strdup_printf("%s/document.json",document->priv->working_path);
  
  // TODO: Error
  json_generator_to_file (gen, json_path, &e);
  if (e)
    {
      g_warning ("Failed to write JSON to file (%s): %s", json_path, e->message);
      g_error_free (e);
    }
  
  g_object_unref (G_OBJECT (gen));
  g_free (json_path);
}

static void
glide_document_extract_archive (GlideDocument *d,
				const gchar *filename)
{
  gchar *command = g_strdup_printf ("tar --strip-components=2 -xjf %s -C %s",
				    filename, d->priv->working_path);
  
  system(command);
  g_free (command);
}

/**
 * glide_document_load_archive:
 * @document: A #GlideDocument
 * @filename: Path to a Glide archive file to open.
 *
 * Opens a Glide archive file, and returns a #JsonParser for
 * the contained document.
 *
 * Return value: A newly created #JsonParser for @filename.
 */
JsonParser *
glide_document_load_archive (GlideDocument *document,
			     const gchar *filename)
{
  JsonParser *p;
  GError *e = NULL;
  gchar *json_file;
  
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  
  p = json_parser_new ();

  glide_document_make_working_dir (document);
  glide_document_extract_archive (document, filename);
  
  json_file = g_strdup_printf("%s/document.json",document->priv->working_path);

  json_parser_load_from_file (p, json_file, &e);
  if (e)
    {
      gchar *sec = g_strdup_printf ("Failed to load the document: %s", filename);
      g_warning("Error loading file: %s", e->message);

      glide_gtk_util_show_error_dialog ("Failed to load document", sec);
					
      g_error_free (e);
      g_object_unref (G_OBJECT (p));
      g_free (json_file);
      g_free (sec);
      
      return NULL;
    }
  g_free (json_file);
  
  glide_document_set_path (document, filename);
  
  return p;
}

//TODO: Name sanitization
/**
 * glide_document_add_resource:
 * @document: A #GlideDocument
 * @filename: The path to the resource to add
 *
 * Adds @filename to @document as a resource, and returns
 * the resource name. The file will now be embedded in the
 * document archive and is accessible by resource name.
 *
 * Return value: The resource name for @filename in @document.
 */
gchar *
glide_document_add_resource (GlideDocument *document, const gchar *filename)
{
  GFile *from, *to;
  gchar *basename, *to_path;
  
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  
  if (!g_path_is_absolute (filename))
    return g_strdup (filename);
  
  basename = g_path_get_basename (filename);
  to_path = g_strdup_printf("%s/resources/%s", document->priv->working_path,
			    basename);

  from = g_file_new_for_path (filename);
  to = g_file_new_for_path (to_path);
  
  // TODO: Error
  g_file_copy (from, to, 0, NULL, NULL, NULL, NULL);
  
  g_free (to_path);
  
  g_object_unref (G_OBJECT (from));
  g_object_unref (G_OBJECT (to));
  
  return basename;
}

/**
 * glide_document_get_resource_path:
 * @document: A #GlideDocument
 * @resource_name: The resource name
 *
 * Returns a file path for @resource_name in the extracted
 * document archive.
 *
 * Return value: The path for @resource_name.
 */
gchar *
glide_document_get_resource_path (GlideDocument *document,
				  const gchar *resource_name)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  return g_strdup_printf("%s/resources/%s",document->priv->working_path,
			 resource_name);
}

/**
 * glide_document_get_theme:
 * @document: A #GlideDocument
 *
 * Returns the theme for @document.
 *
 * Return value: The #GlideTheme for @document.
 */
GlideTheme *
glide_document_get_theme (GlideDocument *document)
{
  g_return_val_if_fail (GLIDE_IS_DOCUMENT (document), NULL);
  return document->priv->theme;
}

/**
 * glide_document_set_theme:
 * @document: A #GlideDocument
 * @theme: The #GlideTheme to use for @document
 *
 * Sets the theme for @document to @theme
 */
void
glide_document_set_theme (GlideDocument *document, GlideTheme *theme)
{
  g_return_if_fail (GLIDE_IS_DOCUMENT (document));
  g_return_if_fail (GLIDE_IS_THEME (theme));

  document->priv->theme = theme;
  g_object_notify (G_OBJECT (document), "theme");
}
