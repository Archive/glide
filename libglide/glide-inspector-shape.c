/*
 * glide-inspector-shape.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-inspector-shape
 * @short_description: An inspector page for shape actors.
 *
 */

#include "glide-inspector-shape.h"
#include "glide-inspector-shape-priv.h"

#include "glide-shape.h"

#include "glide-undo-manager.h"

#include "glide-gtk-util.h"



#include <string.h>
#include <math.h>

#define GLIDE_INSPECTOR_SHAPE_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_INSPECTOR_SHAPE, GlideInspectorShapePrivate))

G_DEFINE_TYPE(GlideInspectorShape, glide_inspector_shape, GTK_TYPE_ALIGNMENT);

enum {
  PROP_0,
  PROP_ACTOR
};

static void
glide_inspector_shape_make_color_binding (GlideInspectorShape *ins,
					  GObject *actor, const gchar *actor_prop, 
					  GObject *button)
{
  glide_mutual_binding_pool_add_full (ins->priv->pool, actor, actor_prop, button, "color",
				      glide_binding_transform_clutter_color_to_gdk_color,
				      glide_binding_transform_gdk_color_to_clutter_color);
}

static void
glide_inspector_shape_bind (GlideInspectorShape *ins)
{
  glide_inspector_shape_make_color_binding (ins, G_OBJECT (ins->priv->actor), "color",
								       G_OBJECT (ins->priv->color_button));
  glide_inspector_shape_make_color_binding (ins, G_OBJECT (ins->priv->actor), "border-color",
					    G_OBJECT (ins->priv->border_color_button));
  glide_mutual_binding_pool_add_full (ins->priv->pool, G_OBJECT (ins->priv->actor), "border-width",
				      G_OBJECT (ins->priv->width_spin), "value",
				      NULL, NULL);
}

static void
glide_inspector_shape_finalize (GObject *object)
{
  GlideInspectorShape *ins = (GlideInspectorShape *)object;
  glide_mutual_binding_pool_free (ins->priv->pool);
}

static void
glide_inspector_shape_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorShape *inspector = GLIDE_INSPECTOR_SHAPE (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      g_value_set_object (value, inspector->priv->actor);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_shape_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorShape *inspector = GLIDE_INSPECTOR_SHAPE (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      glide_inspector_shape_set_actor (inspector, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static GtkWidget*
glide_inspector_shape_make_color_box (GlideInspectorShape *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0, 0.6, 1);
  GtkWidget *button = gtk_color_button_new ();
  
  gtk_label_set_markup (GTK_LABEL (label), "<b>Color:</b>");
  
  gtk_container_add (GTK_CONTAINER (align), button);
  
  gtk_box_pack_start (GTK_BOX(ret), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(ret), align, TRUE, TRUE, 0);
  
  ins->priv->color_button = button;

  return ret;
}

static GtkWidget*
glide_inspector_shape_make_border_color_box (GlideInspectorShape *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0, 0.6, 1);
  GtkWidget *button = gtk_color_button_new ();
  
  gtk_label_set_markup (GTK_LABEL (label), "<b>Border Color:</b>");
  
  gtk_container_add (GTK_CONTAINER (align), button);
  
  gtk_box_pack_start (GTK_BOX(ret), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(ret), align, TRUE, TRUE, 0);
  
  ins->priv->border_color_button = button;

  return ret;
}

static GtkWidget *
glide_inspector_shape_make_border_width_box (GlideInspectorShape *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 10);
  GtkWidget *width_label = gtk_label_new (NULL);
  GtkWidget *al = gtk_alignment_new (1, 0, 0.4, 1);
  ins->priv->width_spin = gtk_spin_button_new_with_range (0, 100, 0.1);

  gtk_spin_button_set_value (GTK_SPIN_BUTTON (ins->priv->width_spin), 1.0);
  gtk_label_set_markup (GTK_LABEL (width_label), "<b>Border Width:</b>");
  
  gtk_container_add (GTK_CONTAINER (al), ins->priv->width_spin);
  gtk_box_pack_start (GTK_BOX (ret), width_label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (ret), al, TRUE, TRUE, 0);
  
  return ret;    
}

static void
glide_inspector_shape_setup_ui (GlideInspectorShape *ins)
{
  GtkWidget *vbox = gtk_vbox_new (FALSE, 0);
  GtkWidget *color_box = glide_inspector_shape_make_color_box (ins);
  GtkWidget *border_color_box = glide_inspector_shape_make_border_color_box (ins);
  GtkWidget *border_width_box = glide_inspector_shape_make_border_width_box (ins);
  
  gtk_box_pack_start (GTK_BOX (vbox), color_box, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), border_color_box, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), border_width_box, FALSE, FALSE, 0);
  
  gtk_container_add (GTK_CONTAINER (ins), vbox);
}

static void
glide_inspector_shape_init (GlideInspectorShape *inspector)
{
  inspector->priv = GLIDE_INSPECTOR_SHAPE_GET_PRIVATE (inspector);
  
  gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
  
  glide_inspector_shape_setup_ui (inspector);
  

  gtk_alignment_set (GTK_ALIGNMENT (inspector), .5, 0, 0.8, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (inspector), 5, 0, 0, 0);
  
  inspector->priv->pool = glide_mutual_binding_pool_new ();
}

static void
glide_inspector_shape_class_init (GlideInspectorShapeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_inspector_shape_finalize;
  object_class->get_property = glide_inspector_shape_get_property;
  object_class->set_property = glide_inspector_shape_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_ACTOR,
				   g_param_spec_object ("actor",
							"Actor",
							"The actor we are inspecting",
							GLIDE_TYPE_ACTOR,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideInspectorShapePrivate));
}

GtkWidget *
glide_inspector_shape_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_INSPECTOR_SHAPE, NULL);
}


GlideActor *
glide_inspector_shape_get_actor (GlideInspectorShape *inspector)
{
  return inspector->priv->actor;
}


void
glide_inspector_shape_set_actor (GlideInspectorShape *inspector,
				 GlideActor *actor)
{
  glide_mutual_binding_pool_unbind (inspector->priv->pool);
  inspector->priv->actor = actor;
  
  if (!actor)
    return;
  
  if (!GLIDE_IS_SHAPE (actor))
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), TRUE);
      glide_inspector_shape_bind (inspector);
    }
  
  g_object_notify (G_OBJECT (inspector), "actor");
}
