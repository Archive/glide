/*
 * glide-actor.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * SECTION:glide-actor
 * @short_description: Base abstract class for all visual document elements.
 *
 */
 
#include <glib/gi18n.h>
#include "glide-actor.h"

#include <girepository.h>
#include <math.h>

#include "glide-actor-priv.h"
#include "glide-json-util.h"

#include "glide-image.h"
#include "glide-text.h"
#include "glide-shape.h"

G_DEFINE_ABSTRACT_TYPE(GlideActor, glide_actor, CLUTTER_TYPE_ACTOR)

#define GLIDE_ACTOR_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_ACTOR, GlideActorPrivate))

enum {
  PROP_0,
  PROP_STAGE_MANAGER,
  PROP_SELECTED
  };

enum {
  SELECTED,
  DESELECTED,
  LAST_SIGNAL
};

static guint actor_signals[LAST_SIGNAL] = { 0, };

static gchar *
glide_actor_generate_name ()
{
  static int i = 0;
  
  i++;
  return g_strdup_printf("actor%d", i);
}


static void
glide_actor_finalize (GObject *object)
{
  GlideActor *actor = (GlideActor *)object;

  G_OBJECT_CLASS (glide_actor_parent_class)->finalize (object);
}


static void
glide_actor_get_property (GObject *object,
				guint prop_id,
				GValue *value,
				GParamSpec *pspec)
{
  GlideActor *actor = GLIDE_ACTOR (object);
  
  switch (prop_id)
    {
    case PROP_STAGE_MANAGER:
      g_value_set_object (value, actor->priv->manager);
      break;
    case PROP_SELECTED:
      g_value_set_boolean (value, actor->priv->selected);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_actor_selection_changed_callback (GlideStageManager *manager,
					GObject *old_selection,
					gpointer data)
{
  GlideActor *this = (GlideActor *)data;
  
  if (((GlideActor *)glide_stage_manager_get_selection (manager) != this) &&
      (this->priv->selected == TRUE))
    {
      this->priv->selected = FALSE;
      
      g_object_notify (G_OBJECT (this), "selected");
      g_signal_emit (this, actor_signals[DESELECTED], 0);
    }
  if ((GlideActor *)glide_stage_manager_get_selection (manager) == this)
    {
      this->priv->selected = TRUE;

      g_object_notify (G_OBJECT (this), "selected");
      g_signal_emit (this, actor_signals[SELECTED], 0);
    }
}

static void
glide_actor_set_stage_manager_real (GlideActor *actor,
				    GlideStageManager *manager)
{
  g_return_if_fail (actor->priv->manager == NULL || actor->priv->manager == manager);
  actor->priv->manager = manager;

  g_signal_connect (actor->priv->manager, "selection-changed",
							G_CALLBACK (glide_actor_selection_changed_callback),
							actor);
  g_object_notify (G_OBJECT (actor), "stage-manager");
}

static void
glide_actor_set_property (GObject *object,
			  guint prop_id,
			  const GValue *value,
			  GParamSpec *pspec)
{
  GlideActor *actor = GLIDE_ACTOR (object);
  
  switch (prop_id)
    {
    case PROP_STAGE_MANAGER:
      glide_actor_set_stage_manager_real (actor, g_value_get_object (value));
      break;
    default: 
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_actor_class_init (GlideActorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = glide_actor_finalize;
  object_class->set_property = glide_actor_set_property;
  object_class->get_property = glide_actor_get_property;
  
  g_object_class_install_property (object_class,
				   PROP_STAGE_MANAGER,
				   g_param_spec_object ("stage-manager",
							"Stage manager",
							"The stage manager responsible for the actor.",
							GLIDE_TYPE_STAGE_MANAGER,
							G_PARAM_READWRITE |
							G_PARAM_STATIC_STRINGS));
  
  g_object_class_install_property (object_class,
				   PROP_SELECTED,
				   g_param_spec_boolean("selected",
							"Selected",
							"Whether the glide actor is selected",
							FALSE,
							G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  
  actor_signals[SELECTED] = 
    g_signal_new ("selected",
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GlideActorClass, selected),
		  NULL, NULL,
		  gi_cclosure_marshal_generic,
		  G_TYPE_NONE, 0);
  actor_signals[DESELECTED] = 
    g_signal_new ("deselected",
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GlideActorClass, deselected),
		  NULL, NULL,
		  gi_cclosure_marshal_generic,
		  G_TYPE_NONE, 0);
		  

  g_type_class_add_private (object_class, sizeof(GlideActorPrivate));
}


static void
glide_actor_init (GlideActor *actor)
{
  gchar *name;
  actor->priv = GLIDE_ACTOR_GET_PRIVATE (actor);
  
  name = glide_actor_generate_name ();
  
  clutter_actor_set_reactive (CLUTTER_ACTOR (actor), TRUE);
  clutter_actor_set_name (CLUTTER_ACTOR (actor), name);
  
  g_free (name);
}

/**
 * glide_actor_get_stage_manager:
 * @actor: A #GlideActor
 *
 * Returns the stage manager responsible for @actor.
 *
 * Return value: The #GlideStageManager responsible for @actor.
 */
GlideStageManager *
glide_actor_get_stage_manager (GlideActor *actor)
{
  g_return_if_fail (GLIDE_IS_ACTOR (actor));

  return actor->priv->manager;
}

/**
 * glide_actor_get_selected:
 * @actor: #GlideActor
 *
 * Returns whether an actor is the current selection.
 *
 * Return value: Whether @actor is the current selection.
 */
gboolean
glide_actor_get_selected (GlideActor *actor)
{
  g_return_if_fail (GLIDE_IS_ACTOR (actor));

  return actor->priv->selected;
}

/**
 * glide_actor_set_stage_manager:
 * @actor: A #GlideActor
 * @manager: The #GlideStageManager to set.
 *
 * Sets @manager to be responsible for @actor. BUG: 
 * calling this more than once for a given actor
 * is not permitted and it should probably be
 * a construct only property
 */
void
glide_actor_set_stage_manager (GlideActor *actor, GlideStageManager *manager)
{
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  g_return_if_fail (GLIDE_IS_STAGE_MANAGER (manager));

  glide_actor_set_stage_manager_real (actor, manager);
}

/**
 * glide_actor_serialize:
 * @actor: A #GlideActor to serialize.
 *
 * Serializes @actor to a #JsonNode, which can later
 * be used by glide_actor_deserialize.
 *
 * Return value: A #JsonNode containing the serialized actor
 */
JsonNode *
glide_actor_serialize (GlideActor *actor)
{
  GlideActorClass *klass;
  
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  
  klass = GLIDE_ACTOR_GET_CLASS (actor);
  if (klass->serialize)
    {
      JsonNode *n = klass->serialize (actor);
      JsonObject *o = json_node_get_object (n);
      glide_json_object_set_string (o, "name", clutter_actor_get_name (CLUTTER_ACTOR (actor)));
      
      return n;
    }
  
  return NULL;
}

/**
 * glide_actor_deserialize:
 * @actor: A #GlideActor
 * @actor_obj: A #JsonObject containing the serialized actor data.
 *
 * Sets the properties of @actor based on the serialized data in @actor_obj.
 */
void
glide_actor_deserialize (GlideActor *actor, JsonObject *actor_obj)
{
  GlideActorClass *klass;
  
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  
  klass = GLIDE_ACTOR_GET_CLASS (actor);
  if (klass->deserialize)
    klass->deserialize (actor, actor_obj);
  
  glide_json_object_restore_actor_geometry (actor_obj, CLUTTER_ACTOR (actor));
  clutter_actor_set_name (CLUTTER_ACTOR (actor), glide_json_object_get_string (actor_obj, "name"));
}

/**
 * glide_actor_print:
 * @actor: A #GlideActor
 * @cr: A #cairo_t to print @actor too
 *
 * Draw a snapshot of @actor to @cr.
 *
 */
void 
glide_actor_print (GlideActor *actor, cairo_t *cr)
{
  GlideActorClass *klass;
  
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  
  klass = GLIDE_ACTOR_GET_CLASS (actor);
  if (klass->print)
    klass->print (actor, cr);
}


/**
 * glide_actor_construct_from_json:
 * @manager: The #GlideStageManager to construct actors for.
 * @actor_obj: The #JsonObject containing the actor data.
 *
 * Constructs a new #GlideActor from @actor_obj (of the type
 * specified in @actor_obj).
 *
 * BUG: This function is mostly used for choosing which "new"
 * function to call. and is pretty clearly a wart.
 *
 * Return value: The newly constructed #GlideActor, or %NULL on failure.
 */
GlideActor *
glide_actor_construct_from_json (GlideStageManager *manager, 
				 JsonObject *actor_obj)
{
  const gchar *a_type;
  GlideActor *ret;
  
  g_return_val_if_fail (GLIDE_IS_STAGE_MANAGER (manager), NULL);
  
  a_type = glide_json_object_get_string (actor_obj, "type");
  
  if (!strcmp(a_type, "image"))
    {
      ret = (GlideActor *)glide_image_new ();
    }
  else if (!strcmp (a_type, "text"))
    {
      ret = (GlideActor *)glide_text_new ();
    }
  else if (!strcmp (a_type, "shape"))
    {
      ret = (GlideActor *)glide_shape_new ();
    }
  else
    {
      return NULL;
    }
  glide_actor_set_stage_manager (ret, manager);
  glide_actor_deserialize (ret, actor_obj);

  return ret;
}

/**
 * glide_actor_get_undo_manager:
 * @actor: A #GlideActor
 *
 * Gets the undo manager responsible for actions
 * performed on @actor.
 *
 * Return value: The #GlideUndoManager assosciated with
 * @actor.
 */
GlideUndoManager *
glide_actor_get_undo_manager (GlideActor *actor)
{
  g_return_val_if_fail (GLIDE_IS_ACTOR (actor), NULL);
  return glide_stage_manager_get_undo_manager (actor->priv->manager);
}

/**
 * glide_actor_add_resource:
 * @actor: A #GlideActor
 * @filename: The path of the resource to add.
 *
 * A convenience wrapper around glide_document_add_resource()
 *
 * Return value: The newly imported resource name.
 */
gchar *
glide_actor_add_resource (GlideActor *actor, const gchar *filename)
{
  g_return_val_if_fail (GLIDE_IS_ACTOR (actor), NULL);
  return glide_document_add_resource (glide_stage_manager_get_document (glide_actor_get_stage_manager (actor)), filename);
}

/**
 * glide_actor_get_resource_path:
 * @actor: A #GlideActor
 * @resource_name: The name of the resource.
 *
 * A convenience wrapper around glide_document_get_resource_path()
 *
 * Return value: A path which can be used to open the file assosciated 
 * with @resource_name
 */
gchar *
glide_actor_get_resource_path (GlideActor *actor, const gchar *resource_name)
{
  g_return_val_if_fail (GLIDE_IS_ACTOR (actor), NULL);
  return glide_document_get_resource_path (glide_stage_manager_get_document (glide_actor_get_stage_manager (actor)), resource_name);
}

/**
 * glide_actor_start_undo:
 * @actor: A #GlideActor
 * @label: The undo action label.
 *
 * A convenience wrapper around glide_undo_manager_start_actor_action()
 *
 */
void
glide_actor_start_undo (GlideActor *actor, const gchar *label)
{
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  glide_undo_manager_start_actor_action (glide_actor_get_undo_manager (actor), actor, label);
}

/**
 * glide_actor_end_undo:
 * @actor: A #GlideActor
 *
 * A convenience wrapper around glide_undo_manager_end_actor_action().
 *
 */
void
glide_actor_end_undo (GlideActor *actor)
{
  g_return_if_fail (GLIDE_IS_ACTOR (actor));
  glide_undo_manager_end_actor_action (glide_actor_get_undo_manager (actor), actor);
}
