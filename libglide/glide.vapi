[CCode (cprefix = "Glide", lower_case_cprefix = "glide_")]
namespace Glide {
	[CCode (cheader_filename = "glide-animation-manager.h")]
	public delegate Clutter.Timeline AnimationCallback (AnimationInfo info, Clutter.Actor a, Clutter.Actor b);
	[CCode (cheader_filename = "glide-animation-manager.h")]
	public delegate GLib.List AnimationListOptionsCallback ();
	  
		[CCode (type_id = "GLIDE_TYPE_ANIMATION_INFO", cheader_filename = "glide-animation-manager.h")]
		public struct AnimationInfo {
			Animation animation;
			uint duration;
			
			string option;
		}	  
		[CCode (cheader_filename = "glide-animation-manager.h")]
		public struct Animation {
			string name;
			AnimationCallback animate;
			AnimationListOptionsCallback list_options;
		}
		}