/*
 * glide-slide-box.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_SLIDE_BOX_H__
#define __GLIDE_SLIDE_BOX_H__

#include <gtk/gtk.h>
#include "glide-stage-manager.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_SLIDE_BOX              (glide_slide_box_get_type())
#define GLIDE_SLIDE_BOX(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_SLIDE_BOX, GlideSlideBox))
#define GLIDE_SLIDE_BOX_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_SLIDE_BOX, GlideSlideBoxClass))
#define GLIDE_IS_SLIDE_BOX(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_SLIDE_BOX))
#define GLIDE_IS_SLIDE_BOX_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_SLIDE_BOX))
#define GLIDE_SLIDE_BOX_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_SLIDE_BOX, GlideSlideBoxClass))

typedef struct _GlideSlideBoxPrivate GlideSlideBoxPrivate;


typedef struct _GlideSlideBox GlideSlideBox;

struct _GlideSlideBox
{
  GtkVBox box;
  
  GlideSlideBoxPrivate *priv;
};

typedef struct _GlideSlideBoxClass GlideSlideBoxClass;

struct _GlideSlideBoxClass
{
  GtkVBoxClass parent_class;
};

GType glide_slide_box_get_type (void) G_GNUC_CONST;
GtkWidget *glide_slide_box_new (void);

GlideStageManager *glide_slide_box_get_stage_manager (GlideSlideBox *box);
void glide_slide_box_set_stage_manager (GlideSlideBox *box, GlideStageManager *manager);

G_END_DECLS

#endif
