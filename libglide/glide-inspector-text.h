/*
 * glide-inspector-text.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_INSPECTOR_TEXT_H__
#define __GLIDE_INSPECTOR_TEXT_H__

#include <gtk/gtk.h>
#include "glide-actor.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_INSPECTOR_TEXT              (glide_inspector_text_get_type())
#define GLIDE_INSPECTOR_TEXT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_INSPECTOR_TEXT, GlideInspectorText))
#define GLIDE_INSPECTOR_TEXT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_INSPECTOR_TEXT, GlideInspectorTextClass))
#define GLIDE_IS_INSPECTOR_TEXT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_INSPECTOR_TEXT))
#define GLIDE_IS_INSPECTOR_TEXT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_INSPECTOR_TEXT))
#define GLIDE_INSPECTOR_TEXT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_INSPECTOR_TEXT, GlideInspectorTextClass))

typedef struct _GlideInspectorTextPrivate GlideInspectorTextPrivate;


typedef struct _GlideInspectorText GlideInspectorText;

struct _GlideInspectorText
{
  GtkAlignment alignment;
  
  GlideInspectorTextPrivate *priv;
};

typedef struct _GlideInspectorTextClass GlideInspectorTextClass;

struct _GlideInspectorTextClass
{
  GtkAlignmentClass parent_class;
};

GType glide_inspector_text_get_type (void) G_GNUC_CONST;
GtkWidget *glide_inspector_text_new (void);

GlideActor *glide_inspector_text_get_actor (GlideInspectorText *inspector);
void glide_inspector_text_set_actor (GlideInspectorText *inspector, GlideActor *actor);

const gchar *glide_inspector_text_get_font_name (GlideInspectorText *ins);
void glide_inspector_text_get_color (GlideInspectorText *ins, ClutterColor *cc);

G_END_DECLS

#endif
