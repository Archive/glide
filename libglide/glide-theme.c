/*
 * glide-theme.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * SECTION:glide-theme
 * @short_description: A representation of a loaded theme file.
 *
 */

#include <stdlib.h>
 
#include "glide-theme.h"
#include "glide-theme-priv.h"
#include "glide-cairo-util.h"

#include <gio/gio.h>

#include "glide-gtk-util.h"
#include "glide-json-util.h"
#include "glide-debug.h"

#include "glide-dirs.h"


G_DEFINE_TYPE(GlideTheme, glide_theme, G_TYPE_OBJECT)

#define GLIDE_THEME_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_THEME, GlideThemePrivate))

enum {
  PROP_0,
  PROP_NAME,
  PROP_PATH,
  PROP_BACKGROUND,
  PROP_DEFAULT_FONTNAME,
  PROP_DEFAULT_COLOR
};

/*enum {
  LAST_SIGNAL
};

static guint theme_signals[LAST_SIGNAL] = { 0, };*/

static JsonParser *
glide_theme_parse_theme (GlideTheme *theme)
{
  JsonParser *p = json_parser_new ();
  GError *e = NULL;
  gchar *json_file = g_strconcat (theme->priv->working_path, "/", "theme.json", NULL);

  json_parser_load_from_file (p, json_file, &e);
  if (e)
    {
      gchar *sec = g_strdup_printf ("Failed to load theme: %s", theme->priv->path);
      g_warning ("Error loading theme: %s", e->message);
      
      glide_gtk_util_show_error_dialog ("Failed to load theme", sec);
      
      g_error_free (e);
      g_object_unref (G_OBJECT (p));
      g_free (sec);
      g_free (json_file);
      
      return NULL;
    }
  g_free (json_file);
  
  return p;
}

static void
glide_theme_extract_archive (GlideTheme *theme)
{
  gchar *command = g_strdup_printf ("tar -xjf %s -C %s",
				    theme->priv->path,
				    theme->priv->working_path);
  system(command);
  g_free(command);
}

static void
glide_theme_load_file (GlideTheme *theme)
{
  JsonParser *p;
  JsonNode *root;
  JsonObject *root_object;
  
  glide_theme_extract_archive (theme);

  p = glide_theme_parse_theme (theme);
  
  root = json_parser_get_root (p);
  root_object = json_node_get_object (root);
  
  theme->priv->name = g_strdup (glide_json_object_get_string (root_object, "name"));
  theme->priv->default_background = g_strconcat (theme->priv->working_path, "/", glide_json_object_get_string (root_object, "default-background"), NULL);
  theme->priv->default_fontname = g_strdup (glide_json_object_get_string (root_object, "default-fontname"));
  clutter_color_from_string(&theme->priv->default_color, glide_json_object_get_string (root_object, "default-color"));
  
  g_object_unref (G_OBJECT (p));
}

static void
glide_theme_set_path (GlideTheme *theme, gchar *path)
{
  g_return_if_fail (theme->priv->path == NULL);
  theme->priv->path = path;
  
  glide_theme_load_file (theme);
}

static void
glide_theme_finalize (GObject *object)
{
  GlideTheme *theme = GLIDE_THEME (object);

  g_free (theme->priv->name);
  g_free (theme->priv->path);
  g_free (theme->priv->default_background);
  g_free (theme->priv->working_path);
  g_free (theme->priv->default_fontname);
  
  G_OBJECT_CLASS (glide_theme_parent_class)->finalize (object);
}


static void
glide_theme_get_property (GObject *object,
				  guint prop_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  GlideTheme *theme = GLIDE_THEME (object);
  
  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, theme->priv->name);
      break;
    case PROP_PATH:
      g_value_set_string (value, theme->priv->path);
      break;
    case PROP_BACKGROUND:
      g_value_set_string (value, theme->priv->default_background);
      break;
    case PROP_DEFAULT_FONTNAME:
      g_value_set_string (value, theme->priv->default_fontname);
      break;
    case PROP_DEFAULT_COLOR:
      clutter_value_set_color (value, &theme->priv->default_color);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_theme_set_property (GObject *object,
			     guint prop_id,
			     const GValue *value,
			     GParamSpec *pspec)
{
  GlideTheme *theme = GLIDE_THEME (object);
  
  switch (prop_id)
    {
    case PROP_PATH:
      glide_theme_set_path (theme, g_value_dup_string (value));
      break;
    default: 
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_theme_class_init (GlideThemeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = glide_theme_finalize;
  object_class->set_property = glide_theme_set_property;
  object_class->get_property = glide_theme_get_property;

  g_object_class_install_property (object_class, PROP_NAME,
				   g_param_spec_string ("name",
							"Name",
							"Name of the theme",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_PATH,
				   g_param_spec_string ("path",
							"Path",
							"Path of the theme",
							NULL,
							G_PARAM_READWRITE |
							G_PARAM_STATIC_STRINGS |
							G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, PROP_BACKGROUND,
				   g_param_spec_string ("default-background",
							"Default background",
							"The default background image for the theme",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, PROP_DEFAULT_FONTNAME,
				   g_param_spec_string ("default-fontname",
							"Default fontname",
							"The default fontname for the theme",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_DEFAULT_COLOR,
				   clutter_param_spec_color ("default-color",
							     "Default Color",
							     "The default fill color for the theme",
							     NULL,
							     G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));


  g_type_class_add_private (object_class, sizeof(GlideThemePrivate));
}

static void
glide_theme_make_working_dir (GlideTheme *d)
{
  gchar *wdir;
  wdir = glide_dirs_make_temp_dir ();
 
  if (d->priv->working_path)
    g_free (d->priv->working_path);

  d->priv->working_path = wdir;
}

static void
glide_theme_init (GlideTheme *d)
{
  d->priv = GLIDE_THEME_GET_PRIVATE (d);
  
  glide_theme_make_working_dir (d);
}

/**
 * glide_theme_new:
 * @path: The path containing the theme definition file.
 *
 * Constructs a new #GlideTheme from the definition file
 * at @path.
 *
 * Return value: The newly constructed #GlideTheme.
 */
GlideTheme *
glide_theme_new (const gchar *path)
{
  return g_object_new (GLIDE_TYPE_THEME,
		       "path", path,
		       NULL);
}

/**
 * glide_theme_get_default_background:
 * @theme: A #GlideTheme.
 *
 * Returns the default background for @theme.
 *
 * Return value: The default background of @theme.
 */
const gchar *
glide_theme_get_default_background (GlideTheme *theme)
{
  g_return_val_if_fail (GLIDE_IS_THEME (theme), NULL);
  return theme->priv->default_background;
}

/**
 * glide_theme_get_name:
 * @theme: A #GlideTheme.
 *
 * Returns the name for @theme.
 *
 * Return value: The name of @theme.
 */
const gchar *
glide_theme_get_name (GlideTheme *theme)
{
  g_return_val_if_fail (GLIDE_IS_THEME (theme), NULL);
  return theme->priv->name;
}

/**
 * glide_theme_get_path:
 * @theme: A #GlideTheme.
 *
 * Returns the path of the definition file for @theme.
 *
 * BUG: Themes should probably be resources of some sort, imported in to the file.
 *
 * Return value: The path of @theme.
 */
const gchar *
glide_theme_get_path (GlideTheme *theme)
{
  g_return_val_if_fail (GLIDE_IS_THEME (theme), NULL);
  return theme->priv->path;
}

/**
 * glide_theme_get_default_fontname:
 * @theme: A #GlideTheme
 *
 * Returns the default fontname for @theme.
 *
 * Return value: The default fontname of @theme.
 */
const gchar *
glide_theme_get_default_fontname (GlideTheme *theme)
{
  g_return_val_if_fail (GLIDE_IS_THEME (theme), NULL);
  return theme->priv->default_fontname;
}

/**
 * glide_theme_get_default_color:
 * @theme: A #GlideTheme
 * @color: A #ClutterColor in which the return value will be stored.
 *
 * Returns the default color for @theme through @color.
 *
 */
void
glide_theme_get_default_color (GlideTheme *theme, ClutterColor *color)
{
  g_return_if_fail (GLIDE_IS_THEME (theme));
  *color = theme->priv->default_color;
}

static void
glide_theme_preview_text (GlideTheme *theme, cairo_t *cr,
			  gint width, gint height)
{
  PangoLayout *p = pango_cairo_create_layout (cr);
  PangoFontDescription *d = pango_font_description_from_string (theme->priv->default_fontname);
  gint lwidth, lheight;
  
  pango_layout_set_font_description (p, d);
  pango_layout_set_text (p, "Double click to edit.", -1);
  
  pango_layout_get_pixel_size (p, &lwidth, &lheight);
  
  cairo_save (cr);
  
  cairo_move_to (cr, width/2.0-(.6*width)/2.0, (height/2.0)-(lheight/2.0)*(.6*width)/lwidth);
  cairo_scale (cr, (.6*width)/lwidth, (.6*width)/lwidth);
  
  glide_cairo_set_clutter_color (cr, &theme->priv->default_color);
  pango_cairo_show_layout (cr, p);
  
  cairo_restore (cr);

  
  cairo_fill (cr);
  
  g_object_unref (G_OBJECT (p));
}

static void
glide_theme_preview_background (GlideTheme *theme, cairo_t *cr,
				gint width, gint height)
{
  GdkPixbuf *p, *s;
  
  // TODO: Error
  p = gdk_pixbuf_new_from_file (theme->priv->default_background, NULL);
  s = gdk_pixbuf_scale_simple (p, width, height, GDK_INTERP_HYPER);
  
  gdk_cairo_set_source_pixbuf (cr, s, 0, 0);
  
  cairo_rectangle (cr, 0, 0, width, height);
  cairo_fill (cr);
  
  g_object_unref (G_OBJECT(p));
  g_object_unref (G_OBJECT(s));
}

void
glide_theme_preview (GlideTheme *theme, cairo_t *cr, gint width, gint height)
{
  glide_theme_preview_background (theme, cr, width, height);
  glide_theme_preview_text (theme, cr, width, height);
}

