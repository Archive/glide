/*
 * glide-inspector-actor-priv.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __GLIDE_INSPECTOR_ACTOR_PRIVATE_H__
#define __GLIDE_INSPECTOR_ACTOR_PRIVATE_H__

#include "glide-inspector-actor.h"

G_BEGIN_DECLS

struct _GlideInspectorActorPrivate
{
  GlideActor *actor;
  
  GtkWidget *name_entry;
  
  GtkWidget *x_spin;
  GtkWidget *y_spin;
  GtkWidget *width_spin;
  GtkWidget *height_spin;
  
  GtkWidget *centerh_button;
  GtkWidget *centerv_button;
  
  gulong allocation_changed_id;
  
  gboolean ignore_change;
};

G_END_DECLS

#endif
