/*
 * glide-inspector-text.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-inspector-text
 * @short_description: An inspector page for text actors.
 *
 */

#include "glide-inspector-text.h"
#include "glide-inspector-text-priv.h"

#include "glide-text.h"

#include "glide-undo-manager.h"

#include "glide-gtk-util.h"

#include <string.h>
#include <math.h>

#define GLIDE_INSPECTOR_TEXT_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_INSPECTOR_TEXT, GlideInspectorTextPrivate))

G_DEFINE_TYPE(GlideInspectorText, glide_inspector_text, GTK_TYPE_ALIGNMENT);

enum {
  PROP_0,
  PROP_ACTOR
};

static void
glide_inspector_text_align_text (GlideInspectorText *ins, PangoAlignment alignment)
{
  glide_actor_start_undo (ins->priv->actor, "Set text alignment");
  glide_text_set_line_alignment (GLIDE_TEXT (ins->priv->actor), alignment);
  glide_actor_end_undo (ins->priv->actor);
}

static void
glide_inspector_text_align_left_clicked (GtkWidget *widg,
					 gpointer user_data)
{
  GlideInspectorText *ins = (GlideInspectorText *)user_data;
  glide_inspector_text_align_text (ins, PANGO_ALIGN_LEFT);
}

static void
glide_inspector_text_align_center_clicked (GtkWidget *widg,
					 gpointer user_data)
{
  GlideInspectorText *ins = (GlideInspectorText *)user_data;
  glide_inspector_text_align_text (ins, PANGO_ALIGN_CENTER);
}

static void
glide_inspector_text_align_right_clicked (GtkWidget *widg,
					 gpointer user_data)
{
  GlideInspectorText *ins = (GlideInspectorText *)user_data;
  glide_inspector_text_align_text (ins, PANGO_ALIGN_RIGHT);
}

static void
glide_inspector_text_update_color (GlideInspectorText *ins)
{
  ClutterColor cc;
  GdkColor c;
  
  glide_text_get_color (GLIDE_TEXT (ins->priv->actor), &cc);
  glide_gdk_color_from_clutter_color (&cc, &c);
  
  ins->priv->ignore_set = TRUE;
  gtk_color_button_set_color (GTK_COLOR_BUTTON (ins->priv->color_button), &c);
  ins->priv->ignore_set = FALSE;
}

static void
glide_inspector_text_color_changed (GObject *object,
				    GParamSpec *pspec,
				    gpointer user_data)
{
  GlideInspectorText *ins = (GlideInspectorText *)user_data;
  glide_inspector_text_update_color (ins);
}

static void
glide_inspector_text_color_set (GtkColorButton *widg,
				gpointer user_data)
{
  GlideInspectorText *ins = (GlideInspectorText *)user_data;
  GdkColor c;
  ClutterColor cc;
  
  if (ins->priv->ignore_set)
    return;
  
  gtk_color_button_get_color (widg, &c);
  glide_clutter_color_from_gdk_color (&c, &cc);
  
  glide_actor_start_undo (ins->priv->actor, "Modify text color");
  glide_text_set_color (GLIDE_TEXT (ins->priv->actor), &cc);
  glide_actor_end_undo (ins->priv->actor);
}

static void
glide_inspector_text_update_fontname (GlideInspectorText *ins)
{
  const gchar *fontname = glide_text_get_font_name (GLIDE_TEXT (ins->priv->actor));

  ins->priv->ignore_set = TRUE;
  gtk_font_button_set_font_name (GTK_FONT_BUTTON (ins->priv->font_button), fontname);
  ins->priv->ignore_set = FALSE;
}

static void
glide_inspector_text_fontname_changed (GObject *object,
				       GParamSpec *pspec,
				       gpointer user_data)
{
  GlideInspectorText *ins = (GlideInspectorText *)user_data;
  glide_inspector_text_update_fontname (ins);
}

static void
glide_inspector_text_font_set (GtkFontButton *widg,
			       gpointer user_data)
{
  GlideInspectorText *ins = (GlideInspectorText *)user_data;
  
  if (ins->priv->ignore_set)
    return;
  
  glide_actor_start_undo (ins->priv->actor, "Modify actor font.");
  glide_text_set_font_name (GLIDE_TEXT (ins->priv->actor),
			    gtk_font_button_get_font_name (widg));
  glide_actor_end_undo (ins->priv->actor);
}


static void
glide_inspector_text_finalize (GObject *object)
{
  
}

static void
glide_inspector_text_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorText *inspector = GLIDE_INSPECTOR_TEXT (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      g_value_set_object (value, inspector->priv->actor);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_text_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorText *inspector = GLIDE_INSPECTOR_TEXT (object);
  
  switch (prop_id)
    {
    case PROP_ACTOR:
      glide_inspector_text_set_actor (inspector, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static GtkWidget *
glide_inspector_text_make_alignment_button (const gchar *stock)
{
  GtkWidget *image = gtk_image_new_from_stock (stock, GTK_ICON_SIZE_MENU);
  GtkWidget *button = gtk_button_new ();
  
  gtk_container_add (GTK_CONTAINER (button), image);
  
  return button;
}
 
static GtkWidget *
glide_inspector_text_make_alignment_button_box (GlideInspectorText *ins)
{
  GtkWidget *bbox = gtk_hbox_new (FALSE, 0);
  GtkWidget *lbutton, *cbutton, *rbutton;
  
  lbutton = glide_inspector_text_make_alignment_button (GTK_STOCK_JUSTIFY_LEFT);
  cbutton = glide_inspector_text_make_alignment_button (GTK_STOCK_JUSTIFY_CENTER);
  rbutton = glide_inspector_text_make_alignment_button (GTK_STOCK_JUSTIFY_RIGHT);
  
  g_signal_connect (lbutton, "clicked", G_CALLBACK (glide_inspector_text_align_left_clicked), ins);
  g_signal_connect (cbutton, "clicked", G_CALLBACK (glide_inspector_text_align_center_clicked), ins);
  g_signal_connect (rbutton, "clicked", G_CALLBACK (glide_inspector_text_align_right_clicked), ins);
  
  gtk_box_pack_start (GTK_BOX (bbox), lbutton, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (bbox), cbutton, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (bbox), rbutton, FALSE, FALSE, 0);
  return bbox;
}

static GtkWidget *
glide_inspector_text_make_alignment_box (GlideInspectorText *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0, 0.0, 1);
  GtkWidget *bbox = glide_inspector_text_make_alignment_button_box (ins);
  
  gtk_label_set_markup (GTK_LABEL (label), "<b>Alignment:</b>");
  
  gtk_container_add (GTK_CONTAINER (align), bbox);
  gtk_box_pack_start (GTK_BOX (ret), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(ret), align, TRUE, TRUE, 0);
  
  return ret;
}

static GtkWidget*
glide_inspector_text_make_color_box (GlideInspectorText *ins)
{
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0.5, 0.4, 0.25);
  GtkWidget *button = gtk_color_button_new ();
  
  gtk_label_set_markup (GTK_LABEL (label), "<b>Color:</b>");
  
  gtk_container_add (GTK_CONTAINER (align), button);
  
  gtk_box_pack_start (GTK_BOX(ret), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(ret), align, TRUE, TRUE, 0);
  
  ins->priv->color_button = button;
  g_signal_connect (button, "color-set", G_CALLBACK (glide_inspector_text_color_set), ins);

  return ret;
}

static GtkWidget *
glide_inspector_text_make_font_box (GlideInspectorText *ins)
{ 
  GtkWidget *ret = gtk_hbox_new (FALSE, 0);
  GtkWidget *label = gtk_label_new (NULL);
  GtkWidget *align = gtk_alignment_new (1, 0, 0.25, 1);
  GtkWidget *button = gtk_font_button_new ();
  
  gtk_label_set_markup (GTK_LABEL (label), "<b>Font:</b>");
  
  gtk_container_add (GTK_CONTAINER (align), button);
  
  gtk_box_pack_start (GTK_BOX(ret), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(ret), align, TRUE, TRUE, 0);
  
  glide_gtk_util_set_widget_font_small (button);
  
  ins->priv->font_button = button;
  g_signal_connect (button, "font-set", G_CALLBACK (glide_inspector_text_font_set), ins);
  return ret;
}

static void
glide_inspector_text_setup_ui (GlideInspectorText *ins)
{  
  GtkWidget *vbox = gtk_vbox_new (FALSE, 0);
  GtkWidget *font_box = glide_inspector_text_make_font_box (ins);
  GtkWidget *color_box = glide_inspector_text_make_color_box (ins);
  GtkWidget *alignment_box = glide_inspector_text_make_alignment_box (ins);
  
  gtk_box_pack_start (GTK_BOX (vbox), font_box, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), color_box, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), alignment_box, FALSE, FALSE, 0);
  
  gtk_container_add (GTK_CONTAINER (ins), vbox);
}

static void
glide_inspector_text_init (GlideInspectorText *inspector)
{
  inspector->priv = GLIDE_INSPECTOR_TEXT_GET_PRIVATE (inspector);
  
  gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);

  glide_inspector_text_setup_ui (inspector);

  gtk_alignment_set (GTK_ALIGNMENT (inspector), .5, 0, 0.8, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (inspector), 5, 0, 0, 0);
}

static void
glide_inspector_text_class_init (GlideInspectorTextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_inspector_text_finalize;
  object_class->get_property = glide_inspector_text_get_property;
  object_class->set_property = glide_inspector_text_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_ACTOR,
				   g_param_spec_object ("actor",
							"Actor",
							"The actor we are inspecting",
							GLIDE_TYPE_ACTOR,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideInspectorTextPrivate));
}

GtkWidget *
glide_inspector_text_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_INSPECTOR_TEXT, NULL);
}


GlideActor *
glide_inspector_text_get_actor (GlideInspectorText *inspector)
{
  return inspector->priv->actor;
}

void
glide_inspector_text_set_actor (GlideInspectorText *inspector,
				 GlideActor *actor)
{
  if (inspector->priv->font_notify_id)
    {
      g_signal_handler_disconnect (inspector->priv->actor,
				   inspector->priv->font_notify_id);
      inspector->priv->font_notify_id = 0;
    }
  inspector->priv->actor = actor;
  if (!actor)
    return;
  
  if (!GLIDE_IS_TEXT (actor))
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (GTK_WIDGET (inspector), TRUE);
      glide_inspector_text_update_fontname (inspector);
      glide_inspector_text_update_color (inspector);
      
      inspector->priv->font_notify_id = g_signal_connect (actor, "notify::font-name",
							   G_CALLBACK (glide_inspector_text_fontname_changed),
							   inspector);
      inspector->priv->color_notify_id = g_signal_connect (actor, "notify::color",
							   G_CALLBACK (glide_inspector_text_color_changed),
							   inspector);
    }
  
  g_object_notify (G_OBJECT (inspector), "actor");
}

const gchar *
glide_inspector_text_get_font_name (GlideInspectorText *ins)
{
  return gtk_font_button_get_font_name (GTK_FONT_BUTTON (ins->priv->font_button));
}

void
glide_inspector_text_get_color (GlideInspectorText *ins, ClutterColor *cc)
{
  GdkColor c;
  
  gtk_color_button_get_color (GTK_COLOR_BUTTON (ins->priv->color_button), &c);
  glide_clutter_color_from_gdk_color (&c, cc);
}
