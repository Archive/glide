/*
 * glide-shape.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __GLIDE_SHAPE_H__
#define __GLIDE_SHAPE_H__

#include <glib-object.h>
#include <clutter/clutter.h>
#include "glide-actor.h"


G_BEGIN_DECLS

#define GLIDE_TYPE_SHAPE                  (glide_shape_get_type())
#define GLIDE_SHAPE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GLIDE_TYPE_SHAPE, GlideShape))
#define GLIDE_SHAPE_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GLIDE_TYPE_SHAPE, GlideShapeClass))
#define GLIDE_IS_SHAPE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GLIDE_TYPE_SHAPE))
#define GLIDE_IS_SHAPE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_SHAPE))
#define GLIDE_SHAPE_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GLIDE_TYPE_SHAPE, GlideShapeClass))

typedef struct _GlideShape        GlideShape;
typedef struct _GlideShapePrivate GlideShapePrivate;
typedef struct _GlideShapeClass   GlideShapeClass;

typedef enum {
  GLIDE_SHAPE_TYPE_RECTANGLE
} GlideShapeType;

struct _GlideShape
{
  GlideActor           parent;
  
  GlideShapePrivate *priv;
}; 

struct _GlideShapeClass 
{
  /*< private >*/
  GlideActorClass parent_class;
};

GType glide_shape_get_type (void) G_GNUC_CONST;
ClutterActor *glide_shape_new              ();

void          glide_shape_get_color        (GlideShape   *shape,
                                                  ClutterColor       *color);
void          glide_shape_set_color        (GlideShape   *shape,
						  const ClutterColor *color);
guint         glide_shape_get_border_width (GlideShape   *shape);
void          glide_shape_set_border_width (GlideShape   *shape,
                                                  guint               width);
void          glide_shape_get_border_color (GlideShape   *shape,
                                                  ClutterColor       *color);
void          glide_shape_set_border_color (GlideShape   *shape,
                                                  const ClutterColor *color);

gboolean glide_shape_get_has_border (GlideShape *shape);
void glide_shape_set_has_border (GlideShape *shape, gboolean has_border);

G_END_DECLS

#endif /* __CLUTTER_SHAPE_H__ */
