/*
 * glide-json-util.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __GLIDE_JSON_UTIL_H__
#define __GLIDE_JSON_UTIL_H__

#include "glide-actor.h"
#include "glide-animation-manager.h"

void glide_json_object_set_string (JsonObject *obj, const gchar *prop, const gchar *value);
const gchar *glide_json_object_get_string (JsonObject *obj, const gchar *prop);

void glide_json_object_set_boolean (JsonObject *obj, const gchar *prop, gboolean value);
gboolean glide_json_object_get_boolean (JsonObject *obj, const gchar *prop);

void glide_json_object_set_double (JsonObject *obj, const gchar *prop, gdouble value);
gdouble glide_json_object_get_double (JsonObject *obj, const gchar *prop);

void glide_json_object_add_actor_geometry (JsonObject *obj, ClutterActor *actor);
void glide_json_object_restore_actor_geometry (JsonObject *obj, ClutterActor *actor);
void glide_json_object_set_animation (JsonObject *obj,
				      const gchar *prop,
				      const GlideAnimationInfo *info);
void glide_json_object_get_animation (JsonObject *obj,
				      const gchar *prop,
				      GlideAnimationInfo *info);

#endif
