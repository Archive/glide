/*
 * glide-inspector-notebook.c
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:glide-inspector-notebook
 * @short_description: The Glide inspector widget.
 *
 */

#include "glide-inspector-notebook.h"
#include "glide-inspector-notebook-priv.h"

#include "glide-inspector-animation.h"
#include "glide-inspector-actor.h"
#include "glide-inspector-slide.h"
#include "glide-inspector-image.h"
#include "glide-inspector-text.h"
#include "glide-inspector-shape.h"

#include "glide-text.h"
#include "glide-image.h"
#include "glide-slide.h"
#include "glide-shape.h"

#define GLIDE_INSPECTOR_NOTEBOOK_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GLIDE_TYPE_INSPECTOR_NOTEBOOK, GlideInspectorNotebookPrivate))

G_DEFINE_TYPE(GlideInspectorNotebook, glide_inspector_notebook, GTK_TYPE_NOTEBOOK);

enum {
  PROP_0,
  PROP_STAGE_MANAGER
};

enum {
  PAGE_ANIMATIONS,
  PAGE_GENERAL,
  PAGE_SLIDE,
  PAGE_TEXT,
  PAGE_IMAGE,
  PAGE_SHAPE
};

static void
glide_inspector_notebook_selection_changed (GlideStageManager *manager,
					    GObject *old_selection,
					    gpointer user_data)
{
  GlideInspectorNotebook *inspector = (GlideInspectorNotebook *)user_data;
  GtkNotebook *notebook = (GtkNotebook *)inspector;
  GlideActor *selection = glide_stage_manager_get_selection (manager);
  int i;
  
  for (i = 0; i < gtk_notebook_get_n_pages (notebook); i++)
    {
      GtkWidget *page = gtk_notebook_get_nth_page (notebook, i);
      g_object_set (page, "actor", selection, NULL);
    }
  
  if (GLIDE_IS_SLIDE (selection))
    {
      gtk_notebook_set_current_page (notebook, PAGE_ANIMATIONS);
    }
  else if (GLIDE_IS_TEXT (selection))
    {
      gtk_notebook_set_current_page (notebook, PAGE_TEXT);
    }
  else if (GLIDE_IS_IMAGE (selection))
    {
      gtk_notebook_set_current_page (notebook, PAGE_IMAGE);
    }
  else if (GLIDE_IS_SHAPE (selection))
    {
      gtk_notebook_set_current_page (notebook, PAGE_SHAPE);
    }
}

static void
glide_inspector_notebook_finalize (GObject *object)
{
  GlideInspectorNotebook *inspector = GLIDE_INSPECTOR_NOTEBOOK (object);
  
  g_object_unref (G_OBJECT (inspector->priv->manager));
}

static void
glide_inspector_notebook_get_property (GObject *object,
			      guint prop_id,
			      GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorNotebook *inspector = GLIDE_INSPECTOR_NOTEBOOK (object);
  
  switch (prop_id)
    {
    case PROP_STAGE_MANAGER:
      g_value_set_object (value, inspector->priv->manager);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_notebook_set_property (GObject *object,
			      guint prop_id,
			      const GValue *value,
			      GParamSpec *pspec)
{
  GlideInspectorNotebook *inspector = GLIDE_INSPECTOR_NOTEBOOK (object);
  
  switch (prop_id)
    {
    case PROP_STAGE_MANAGER:
      glide_inspector_notebook_set_stage_manager (inspector, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
glide_inspector_notebook_add_pages (GlideInspectorNotebook *inspector)
{
  GtkNotebook *notebook = GTK_NOTEBOOK (inspector);
  GtkWidget *animation_image = gtk_image_new_from_stock (GTK_STOCK_MEDIA_NEXT, GTK_ICON_SIZE_MENU);
  GtkWidget *actor_image = gtk_image_new_from_stock (GTK_STOCK_PROPERTIES, GTK_ICON_SIZE_MENU);
  GtkWidget *slide_image = gtk_image_new_from_stock (GTK_STOCK_MISSING_IMAGE, GTK_ICON_SIZE_MENU);
  GtkWidget *image_image = gtk_image_new_from_icon_name ("image-x-generic", GTK_ICON_SIZE_MENU);
  GtkWidget *text_image = gtk_image_new_from_icon_name ("format-text-italic", GTK_ICON_SIZE_MENU);
  GtkWidget *shape_image = gtk_image_new_from_icon_name ("gtk-select-color", GTK_ICON_SIZE_MENU);

  inspector->priv->inspector_animation= glide_inspector_animation_new ();
  inspector->priv->inspector_actor = glide_inspector_actor_new ();
  inspector->priv->inspector_slide = glide_inspector_slide_new ();
  inspector->priv->inspector_image = glide_inspector_image_new ();
  inspector->priv->inspector_text = glide_inspector_text_new ();
  inspector->priv->inspector_shape = glide_inspector_shape_new ();
  
  gtk_notebook_append_page (notebook, inspector->priv->inspector_animation, animation_image);
  gtk_notebook_append_page (notebook, inspector->priv->inspector_actor, actor_image);
  gtk_notebook_append_page (notebook, inspector->priv->inspector_slide, slide_image);
  gtk_notebook_append_page (notebook, inspector->priv->inspector_text, text_image);
  gtk_notebook_append_page (notebook, inspector->priv->inspector_image, image_image);
  gtk_notebook_append_page (notebook, inspector->priv->inspector_shape, shape_image);
}

static void
glide_inspector_notebook_init (GlideInspectorNotebook *inspector)
{
  inspector->priv = GLIDE_INSPECTOR_NOTEBOOK_GET_PRIVATE (inspector);
  
  glide_inspector_notebook_add_pages (inspector);
  
  gtk_widget_set_size_request (GTK_WIDGET (inspector), 225, 400);
}

static void
glide_inspector_notebook_class_init (GlideInspectorNotebookClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = glide_inspector_notebook_finalize;
  object_class->get_property = glide_inspector_notebook_get_property;
  object_class->set_property = glide_inspector_notebook_set_property;
  
  g_object_class_install_property (object_class,
				   PROP_STAGE_MANAGER,
				   g_param_spec_object ("stage-manager",
							"Stage manager",
							"The stage manager this inspector is responsible for",
							GLIDE_TYPE_STAGE_MANAGER,
							G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (object_class, sizeof(GlideInspectorNotebookPrivate));
}

GtkWidget *
glide_inspector_notebook_new ()
{
  return (GtkWidget *)g_object_new (GLIDE_TYPE_INSPECTOR_NOTEBOOK, NULL);
}


GlideStageManager *
glide_inspector_notebook_get_stage_manager (GlideInspectorNotebook *inspector)
{
  return inspector->priv->manager;
}

void
glide_inspector_notebook_set_stage_manager (GlideInspectorNotebook *inspector,
					    GlideStageManager *manager)
{
  int i = 0;
  GtkNotebook *notebook = GTK_NOTEBOOK (inspector);

  for (i = 0; i < gtk_notebook_get_n_pages (notebook); i++)
    {
      GtkWidget *page = gtk_notebook_get_nth_page (notebook, i);
      g_object_set (page, "actor", NULL, NULL);
    }

  if (manager)
    {
      inspector->priv->manager = manager;
      inspector->priv->selection_changed_id = g_signal_connect (inspector->priv->manager,
								"selection-changed",
								G_CALLBACK (glide_inspector_notebook_selection_changed),
								inspector);
    }
  else
    {
      inspector->priv->manager = NULL;
    }
  
  g_object_notify (G_OBJECT (inspector), "stage-manager");
}

void
glide_inspector_notebook_get_text_color (GlideInspectorNotebook *ins,
					 ClutterColor *cc)
{
  glide_inspector_text_get_color (GLIDE_INSPECTOR_TEXT (ins->priv->inspector_text), cc);
}

const gchar *
glide_inspector_notebook_get_font_name (GlideInspectorNotebook *ins)
{
  return glide_inspector_text_get_font_name (GLIDE_INSPECTOR_TEXT (ins->priv->inspector_text));
}
