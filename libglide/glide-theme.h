/*
 * glide-theme.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __GLIDE_THEME_H__
#define __GLIDE_THEME_H__

#include <clutter/clutter.h>

#include <json-glib/json-glib.h>
#include <cairo.h>

#include "glide-types.h"

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define GLIDE_TYPE_THEME              (glide_theme_get_type())
#define GLIDE_THEME(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_THEME, GlideTheme))
#define GLIDE_THEME_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_THEME, GlideThemeClass))
#define GLIDE_IS_THEME(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_THEME))
#define GLIDE_IS_THEME_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_THEME))
#define GLIDE_THEME_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_THEME, GlideThemeClass))

/* Private structure type */
typedef struct _GlideThemePrivate GlideThemePrivate;

/*
 * Main object structure
 */
typedef struct _GlideTheme GlideTheme;

struct _GlideTheme 
{
  GObject object;
  
  GlideThemePrivate *priv;
};

/*
 * Class definition
 */
typedef struct _GlideThemeClass GlideThemeClass;

struct _GlideThemeClass 
{
  GObjectClass parent_class;
};

/*
 * Public methods
 */
GType 		 glide_theme_get_type 			(void) G_GNUC_CONST;

GlideTheme   *glide_theme_new (const gchar *path);

const gchar     *glide_theme_get_path (GlideTheme *theme);
const gchar     *glide_theme_get_name (GlideTheme *theme);
const gchar     *glide_theme_get_default_background (GlideTheme *theme);
const gchar     *glide_theme_get_default_fontname (GlideTheme *theme);
void             glide_theme_get_default_color (GlideTheme *theme, ClutterColor *c);

void glide_theme_preview (GlideTheme *theme, cairo_t *cr, gint width, gint height);

G_END_DECLS

#endif  /* __GLIDE_THEME_H__  */
