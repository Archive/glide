/*
 * glide-inspector-slide.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_INSPECTOR_SLIDE_H__
#define __GLIDE_INSPECTOR_SLIDE_H__

#include <gtk/gtk.h>
#include "glide-actor.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_INSPECTOR_SLIDE              (glide_inspector_slide_get_type())
#define GLIDE_INSPECTOR_SLIDE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_INSPECTOR_SLIDE, GlideInspectorSlide))
#define GLIDE_INSPECTOR_SLIDE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_INSPECTOR_SLIDE, GlideInspectorSlideClass))
#define GLIDE_IS_INSPECTOR_SLIDE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_INSPECTOR_SLIDE))
#define GLIDE_IS_INSPECTOR_SLIDE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_INSPECTOR_SLIDE))
#define GLIDE_INSPECTOR_SLIDE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_INSPECTOR_SLIDE, GlideInspectorSlideClass))

typedef struct _GlideInspectorSlidePrivate GlideInspectorSlidePrivate;


typedef struct _GlideInspectorSlide GlideInspectorSlide;

struct _GlideInspectorSlide
{
  GtkAlignment alignment;
  
  GlideInspectorSlidePrivate *priv;
};

typedef struct _GlideInspectorSlideClass GlideInspectorSlideClass;

struct _GlideInspectorSlideClass
{
  GtkAlignmentClass parent_class;
};

GType glide_inspector_slide_get_type (void) G_GNUC_CONST;
GtkWidget *glide_inspector_slide_new (void);

GlideActor *glide_inspector_slide_get_actor (GlideInspectorSlide *inspector);
void glide_inspector_slide_set_actor (GlideInspectorSlide *inspector, GlideActor *actor);

G_END_DECLS

#endif
