/*
 * glide-inspector-animation.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_INSPECTOR_ANIMATION_H__
#define __GLIDE_INSPECTOR_ANIMATION_H__

#include <gtk/gtk.h>
#include "glide-actor.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_INSPECTOR_ANIMATION              (glide_inspector_animation_get_type())
#define GLIDE_INSPECTOR_ANIMATION(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_INSPECTOR_ANIMATION, GlideInspectorAnimation))
#define GLIDE_INSPECTOR_ANIMATION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_INSPECTOR_ANIMATION, GlideInspectorAnimationClass))
#define GLIDE_IS_INSPECTOR_ANIMATION(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_INSPECTOR_ANIMATION))
#define GLIDE_IS_INSPECTOR_ANIMATION_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_INSPECTOR_ANIMATION))
#define GLIDE_INSPECTOR_ANIMATION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_INSPECTOR_ANIMATION, GlideInspectorAnimationClass))

typedef struct _GlideInspectorAnimationPrivate GlideInspectorAnimationPrivate;


typedef struct _GlideInspectorAnimation GlideInspectorAnimation;

struct _GlideInspectorAnimation
{
  GtkAlignment alignment;
  
  GlideInspectorAnimationPrivate *priv;
};

typedef struct _GlideInspectorAnimationClass GlideInspectorAnimationClass;

struct _GlideInspectorAnimationClass
{
  GtkAlignmentClass parent_class;
};

GType glide_inspector_animation_get_type (void) G_GNUC_CONST;
GtkWidget *glide_inspector_animation_new (void);

GlideActor *glide_inspector_animation_get_actor (GlideInspectorAnimation *inspector);
void glide_inspector_animation_set_actor (GlideInspectorAnimation *inspector, GlideActor *actor);

G_END_DECLS

#endif
