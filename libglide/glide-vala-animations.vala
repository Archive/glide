namespace Glide {

    public static 
	Clutter.Timeline animate_fade (AnimationInfo info, 
								   Clutter.Actor a,  
								   Clutter.Actor b){
		var timeline = new Clutter.Timeline (info.duration);
		
		b.opacity = 0x00;
		b.show_all ();
		
		b.raise (a);
		b.animate_with_timeline (Clutter.AnimationMode.LINEAR, 
								 timeline, "opacity", 255);
				
		timeline.completed.connect((t) => {
				a.opacity = 0xff;
				a.hide_all();
			});	
		
		return timeline;
    }	

	public static
	GLib.List animate_slide_list_options ()
	{
		var list = new GLib.List<string> ();
		list.append("Up");
		list.append("Down");
		list.append("Left");
		list.append("Right");
		
		return list;
	}
	
	public static
	Clutter.Timeline animate_slide (AnimationInfo info,
									Clutter.Actor a,
									Clutter.Actor b)
	{
		var timeline = new Clutter.Timeline (info.duration);
		b.show_all ();
		
		switch (info.option)
		{
		case "Up":
			b.y = a.height;
			b.animate_with_timeline (Clutter.AnimationMode.EASE_IN_OUT_SINE, timeline, "y", 0.0);
			a.animate_with_timeline(Clutter.AnimationMode.EASE_IN_OUT_SINE, timeline, "y", -a.height);
			break;
		case "Down":
			b.y = -a.height;
			b.animate_with_timeline (Clutter.AnimationMode.EASE_IN_OUT_SINE, timeline, "y", 0.0);
			a.animate_with_timeline(Clutter.AnimationMode.EASE_IN_OUT_SINE, timeline, "y", a.height);
			break;
		case "Left":
			b.x = a.width;
			b.animate_with_timeline (Clutter.AnimationMode.EASE_IN_OUT_SINE, timeline, "x", 0.0);
			a.animate_with_timeline(Clutter.AnimationMode.EASE_IN_OUT_SINE, timeline, "x", -a.width);
			break;
		case "Right":
			b.x = -a.x;
			b.animate_with_timeline (Clutter.AnimationMode.EASE_IN_OUT_SINE, timeline, "x", 0.0);
			a.animate_with_timeline(Clutter.AnimationMode.EASE_IN_OUT_SINE, timeline, "x", a.width);
			break;
		default:
			break;
		}
		
		timeline.completed.connect ((t) => {
				a.hide_all();
				a.x = a.y = 0;
			});
		
		return timeline;

	}
	
	public static
	Clutter.Timeline animate_drop (AnimationInfo info,
								   Clutter.Actor a,
								   Clutter.Actor b){
		var timeline = new Clutter.Timeline(info.duration);

		b.show_all();
		b.raise(a);

		b.y = -a.height;
		b.animate_with_timeline(Clutter.AnimationMode.EASE_OUT_BOUNCE, timeline, "y", 0.0);
		
		timeline.completed.connect ((t) => {
				a.hide_all();
			});
		
		return timeline;
	}
	
	public static
	GLib.List animate_pivot_list_options (){
		var ret = new GLib.List<string> ();
		
		ret.append("Top Right");
		ret.append("Bottom Left");
		ret.append("Bottom Right");
		
		return ret;
	}
	
	public static
	Clutter.Timeline animate_pivot (AnimationInfo info,
									Clutter.Actor a,
									Clutter.Actor b)
	{
		Clutter.Alpha animation_alpha;
		var timeline = new Clutter.Timeline (info.duration);
		float xpos = 0, ypos = 0, angle = 90;
		
		b.show_all();
		b.raise(a);
		
		switch (info.option)
		{
		case "Top Right":
			xpos = a.width;
			angle = -90;
			break;
		case "Bottom Left":
			ypos = a.height;
			angle = -90;
			break;
		case "Bottom Right":
			xpos = a.width;
			ypos = a.height;
			break;
		}
		b.set_rotation (Clutter.RotateAxis.Z_AXIS, angle, xpos, ypos, 0);
		
		animation_alpha = new Clutter.Alpha.full (timeline, Clutter.AnimationMode.EASE_OUT_SINE);
		timeline.new_frame.connect((m) => {
				b.set_rotation (Clutter.RotateAxis.Z_AXIS,
								angle * (1-animation_alpha.alpha),
								xpos, ypos, 0);
			});
		timeline.completed.connect((m) => {
				a.hide_all();
			});
		
		timeline.start ();
		
		return timeline;
		
	}
	
	public static
	Clutter.Timeline animate_fall (AnimationInfo info,
								   Clutter.Actor a,
								   Clutter.Actor b)
	{
		Clutter.Alpha animation_alpha;
		var timeline = new Clutter.Timeline(info.duration);
		b.show_all();
		a.raise (b);
		
		animation_alpha = new Clutter.Alpha.full(timeline, Clutter.AnimationMode.EASE_IN_QUART);
		timeline.new_frame.connect((m) => {
				a.set_rotation(Clutter.RotateAxis.X_AXIS, -90.0 * animation_alpha.alpha, 0, a.height, 0);
			});
		timeline.completed.connect((m) => {
				a.set_rotation (Clutter.RotateAxis.X_AXIS, 0, 0, a.height, 0);
				a.hide_all ();
			});

		timeline.start ();

		return timeline;
	}
	
	public static
	GLib.List animate_zoom_list_options (){
		var ret = new GLib.List<string> ();
		
		ret.append ("Center");
		ret.append ("Top Left");
		ret.append ("Top Right");
		ret.append ("Bottom Left");
		ret.append ("Bottom Right");
		
		return ret;
	}
	
	public static
	Clutter.Timeline animate_zoom (AnimationInfo info,
								   Clutter.Actor a,
								   Clutter.Actor b)
	{
		var timeline = new Clutter.Timeline (info.duration);
		
		b.show_all();
		b.raise(a);
		
		switch (info.option)
		{
		case "Center":
			b.set_scale_full(0, 0, a.width / 2, a.height / 2);
			break;
		case "Top Left":
			b.set_scale_full(0, 0, 0, 0);
			break;
		case "Top Right":
			b.set_scale_full(0, 0, a.width, 0);
			break;
		case "Bottom Left":
			b.set_scale_full(0, 0, 0, a.height);
			break;
		case "Bottom Right":
			b.set_scale_full(0, 0, a.width, a.height);
			break;
		}
		b.animate_with_timeline (Clutter.AnimationMode.EASE_OUT_SINE,
								 timeline, "scale_x", 1.0,
								 "scale_y", 1.0);
		
		timeline.completed.connect ((m) => {
				a.hide_all ();
			});
		
		return timeline;
		
	}
	
	public static
	GLib.List animate_panel_list_options ()
	{
		var ret = new GLib.List<string> ();
		
		ret.append("Up");
		ret.append("Down");
		ret.append("Left");
		ret.append("Right");
		
		return ret;
	}
	
	public static
	Clutter.Timeline animate_panel (AnimationInfo info,
									Clutter.Actor a,
									Clutter.Actor b)
	{
		var timeline = new Clutter.Timeline (info.duration);
		var animator = new Clutter.Animator ();
		string property;
		float pos;

		
		animator.timeline = timeline;
		b.show_all ();
		
		switch (info.option)
		{
		case "Up":
			pos = a.height;
			property = "y";
			break;
		case "Down":
			pos =-a.height;
			property = "y";
			break;
		case "Left":
			pos = a.width;
			property = "x";
			break;
		case "Right":
			pos = -a.width;
			property = "x";
			break;
		default:
			pos = a.width;
			property = "x";
			break;
		}
		
		b.set_property (property, pos);
		b.set_scale_full (0.8, 0.8, a.width/2, a.height/2);
		a.set_property ("scale_center_x", a.width/2);
		a.set_property ("scale_center_y", a.height/2);
		animator.set (a, "scale_x", Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.25, 0.8f, a,
					  "scale_y", Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.25, 0.8f);
		animator.set (a, property, Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.25f, 0.0f);
		animator.set (b, property, Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.25f, pos);
		animator.set (a, property, Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.75, -pos);
		animator.set (b, property, Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.75, 0.0f);
		animator.set (b, "scale_x", Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.75, 0.8f, b,
					  "scale_y", Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.75, 0.8f);
		animator.set (a, "scale_x", Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.75, 0.8f, a,
					  "scale_y", Clutter.AnimationMode.EASE_IN_OUT_SINE, 0.75, 0.8f);
		animator.set (b, "scale_x", Clutter.AnimationMode.EASE_IN_OUT_SINE, 1.0, 1.0f, b,
					  "scale_y", Clutter.AnimationMode.EASE_IN_OUT_SINE, 1.0, 1.0f);
		animator.set (a, "scale_x", Clutter.AnimationMode.EASE_IN_OUT_SINE, 1.0, 1.0f, a,
					  "scale_y", Clutter.AnimationMode.EASE_IN_OUT_SINE, 1.0, 1.0f);

		
		timeline.completed.connect ((m) => {
				a.hide_all();
				a.x = a.y = 0;
				// BUG: Otherwise vala leaks us.
				animator.timeline = null;
			});
		timeline.start();
		
		return timeline;
	}
	
	
}