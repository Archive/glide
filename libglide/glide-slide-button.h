/*
 * glide-slide-button.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIDE_SLIDE_BUTTON_H__
#define __GLIDE_SLIDE_BUTTON_H__

#include <gtk/gtk.h>
#include "glide-slide.h"

G_BEGIN_DECLS

#define GLIDE_TYPE_SLIDE_BUTTON              (glide_slide_button_get_type())
#define GLIDE_SLIDE_BUTTON(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), GLIDE_TYPE_SLIDE_BUTTON, GlideSlideButton))
#define GLIDE_SLIDE_BUTTON_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), GLIDE_TYPE_SLIDE_BUTTON, GlideSlideButtonClass))
#define GLIDE_IS_SLIDE_BUTTON(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), GLIDE_TYPE_SLIDE_BUTTON))
#define GLIDE_IS_SLIDE_BUTTON_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLIDE_TYPE_SLIDE_BUTTON))
#define GLIDE_SLIDE_BUTTON_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), GLIDE_TYPE_SLIDE_BUTTON, GlideSlideButtonClass))

typedef struct _GlideSlideButtonPrivate GlideSlideButtonPrivate;


typedef struct _GlideSlideButton GlideSlideButton;

struct _GlideSlideButton
{
  GtkButton button;
  
  GlideSlideButtonPrivate *priv;
};

typedef struct _GlideSlideButtonClass GlideSlideButtonClass;

struct _GlideSlideButtonClass
{
  GtkButtonClass parent_class;
};

GType glide_slide_button_get_type (void) G_GNUC_CONST;
GtkWidget *glide_slide_button_new (void);

GlideSlide *glide_slide_button_get_slide (GlideSlideButton *button);
void glide_slide_button_set_slide (GlideSlideButton *button, GlideSlide *slide);

G_END_DECLS

#endif
