/*
 * glide-animations.h
 * Copyright (C) Robert Carr 2010 <racarr@gnome.org>
 * 
 * Glide is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Glide is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __GLIDE_ANIMATIONS_H__
#define __GLIDE_ANIMATIONS_H__

#include <clutter/clutter.h>
#include "glide-animation-manager.h"

ClutterTimeline *glide_animations_animate_fade (const GlideAnimationInfo *info, ClutterActor *a, ClutterActor *b);
ClutterTimeline *glide_animations_animate_drop (const GlideAnimationInfo *info, ClutterActor *a, ClutterActor *b);
ClutterTimeline *glide_animations_animate_zoom (const GlideAnimationInfo *info, ClutterActor *a, ClutterActor *b);
ClutterTimeline *glide_animations_animate_zoom_contents (const GlideAnimationInfo *info, ClutterActor *a, ClutterActor *b);
ClutterTimeline *glide_animations_animate_pivot (const GlideAnimationInfo *info, ClutterActor *a, ClutterActor *b);
ClutterTimeline *glide_animations_animate_slide (const GlideAnimationInfo *info, ClutterActor *a, ClutterActor *b);
ClutterTimeline *glide_animations_animate_doorway (const GlideAnimationInfo *info, ClutterActor *a, ClutterActor *b);

#endif
